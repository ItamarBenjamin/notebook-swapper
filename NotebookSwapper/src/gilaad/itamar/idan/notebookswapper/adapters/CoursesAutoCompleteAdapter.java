package gilaad.itamar.idan.notebookswapper.adapters;

import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class CoursesAutoCompleteAdapter extends ArrayAdapter<ICourse>
{
	private CoursesFilter m_Filter;
	private ArrayList<ICourse> m_FilteredItems;
	private final ArrayList<ICourse> m_AllItems;
	private final LayoutInflater inflator;

	public CoursesAutoCompleteAdapter(Activity context, List<ICourse> list) {
        super(context, android.R.layout.simple_list_item_1, list);
        m_AllItems = new ArrayList<ICourse>();
        m_AllItems.addAll(list);
        m_FilteredItems = new ArrayList<ICourse>();
        m_FilteredItems.addAll(m_AllItems);
        inflator = context.getLayoutInflater();
        getFilter();
    }

	public static String getTitleFor(ICourse course)
	{
		return String.valueOf(course.getCourseNumber()) + " " + course.getCourseName();
	}

	@Override
    public Filter getFilter() {
        if (m_Filter == null){
          m_Filter  = new CoursesFilter();
        }
        return m_Filter;
      }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view;
		if (null == convertView)
		{
			view = inflator.inflate(android.R.layout.simple_list_item_1, null);
			ViewHolder vh = new ViewHolder();
			vh.text = ((TextView)view.findViewById(android.R.id.text1));
			view.setTag(vh);
		}
		else
		{
			view = convertView;
		}
		TextView tv = ((ViewHolder) view.getTag()).text;
		ICourse item = getItem(position);
		tv.setText(getTitleFor(item));
		return view;
	}

	private class ViewHolder
	{
		public TextView text;
	}

	private class CoursesFilter extends Filter
	{

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults result = new FilterResults();
            if(constraint != null && constraint.toString().length() > 0)
            {
            	constraint = constraint.toString().toLowerCase();
                ArrayList<ICourse> filteredItems = new ArrayList<ICourse>();

            	for(int i = 0, l = m_AllItems.size(); i < l; i++)
            	{
            		ICourse m = m_AllItems.get(i);
            		String sCourseNameLowerCase = m.getCourseName().toLowerCase();
            		String sCourseNumber = String.valueOf(m.getCourseNumber());
            		String[] words = constraint.toString().split(" +");
        			for (String word : words)
        			{
						if (word.matches("[0-9]+") && sCourseNumber.startsWith(word))
        				{
        					filteredItems.add(m);
        				} else if (word.matches("[a-zA-Zא-ת0-9]+") && sCourseNameLowerCase.startsWith(word))
						{
							filteredItems.add(m);
						}
        			}
            	}

                result.count = filteredItems.size();
                result.values = filteredItems;
            }
            else
            {
                result.values = m_AllItems;
                result.count = m_AllItems.size();
            }
            return result;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			m_FilteredItems = (ArrayList<ICourse>) results.values;
            notifyDataSetChanged();
            clear();
            for(int i = 0, l = m_FilteredItems.size(); i < l; i++) {
				add(m_FilteredItems.get(i));
			}
            notifyDataSetInvalidated();
		}

	}
}
