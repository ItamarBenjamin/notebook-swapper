package gilaad.itamar.idan.notebookswapper.adapters;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.basket.IOnOffersItemPickedListner;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;
import gilaad.itamar.idan.notebookswapper.edit.EditOfferActivity;
import gilaad.itamar.idan.notebookswapper.offer.OfferActivity;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class OffersListAdapter extends AbstractNotebookAdapter<IOfferItem> {

	private final Activity m_activity;
	private final ListView m_lstView;
	private List<IOfferItem> syncingOrUnsyncedOffers = new ArrayList<IOfferItem>();

	public OffersListAdapter(Activity activity, ListView lstView) {
		super(activity);
		m_lstView = lstView;
		m_activity = activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inf = LayoutInflater.from(m_context);
		View view;
		IOfferItem oOffer = getItem(position);
		boolean bUnsynced = syncingOrUnsyncedOffers.contains(oOffer);

		if (null == convertView)
		{
			view = inf.inflate(R.layout.offer_item, null);
			ViewHolder vh = new ViewHolder();
			vh.name = (TextView)view.findViewById(R.id.txtCourseName);
			vh.edition = (TextView)view.findViewById(R.id.txtCourseEdition);
			vh.number = (TextView)view.findViewById(R.id.txtCourseNumber);
			vh.msgsCount = (TextView)view.findViewById(R.id.txtNewMsgsCount);
			vh.price = (TextView)view.findViewById(R.id.txtOfferNotebookPrice);
			vh.condition = (TextView)view.findViewById(R.id.txtOfferNotebookCondition);
			vh.container = (RelativeLayout)view.findViewById(R.id.textContainer);
			vh.unsynced = (ImageView)view.findViewById(R.id.unsynced);
			vh.deleteButton = (ImageButton)view.findViewById(R.id.deleteImgButton);
			vh.editButton = (ImageButton)view.findViewById(R.id.editImgButton);
			view.setTag(vh);
		}
		else
		{
			view = convertView;
		}
		if (!bUnsynced)
		{
			view.setOnTouchListener(getListner(oOffer));
		}
		else
		{
			view.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					return false;
				}
			});
		}
		ViewHolder vh = (ViewHolder) view.getTag();
		CacheManager manager = CacheManager.getManager(m_activity);

		ImageButton deleteButton = ((ImageButton)view.findViewById(R.id.deleteImgButton));
		deleteButton.setOnClickListener(new OnClickListenerDelete(oOffer));

		ImageButton editButton = ((ImageButton)view.findViewById(R.id.editImgButton));
		editButton.setOnClickListener(new OnClickListenerEdit(oOffer, m_activity));
		vh.container.setTranslationX(0);
		vh.deleteButton.setVisibility(View.GONE);
		vh.deleteButton.setClickable(false);
		vh.editButton.setVisibility(View.GONE);
		vh.editButton.setClickable(false);
		ICatalogItem oCatalogItem = manager.getCatalogItem(oOffer.getCatalogItemID());
		ICourse course = manager.getCourse(oCatalogItem.getCourseNumber());
		Integer iCourseNumber = oCatalogItem.getCourseNumber();
//		String sNotebookType = oCatalogItem.getType().toString();
		String sCourseName = course.getCourseName();
		String free = getActivity().getString(R.string.Free);
		String coin = getActivity().getString(R.string.NewShekel);
		String sPrice = oOffer.getPrice() == 0 ? free : Integer.toString(oOffer.getPrice()) + " " + coin;

		vh.name.setText(sCourseName + " - " + oCatalogItem.getType().toName());
		vh.edition.setText(oCatalogItem.getEdition());
		vh.number.setText(Integer.toString(iCourseNumber));
		oOffer.getCatalogItemID();

		int iUnreadCounter = 0;
		List<Message> myOfferMessages = CacheManager.getManager(getActivity()).getMyOffersMessages(oOffer.getOfferID());
		for (Message message : myOfferMessages) {
			if (!message.isRead)
			{
				if (!message.fromOffer)
				{
					iUnreadCounter++;
				}
			}
		}
		vh.msgsCount.setText(Integer.toString(iUnreadCounter));
		vh.price.setText(sPrice);
		vh.condition.setText(oOffer.getCondition().toString());
		if (bUnsynced)
		{
			vh.unsynced.setVisibility(View.VISIBLE);
			vh.container.setBackgroundColor(m_activity.getResources().getColor(R.color.unsynced_item));
		}
		else
		{
			vh.unsynced.setVisibility(View.GONE);
			vh.container.setBackgroundColor(m_activity.getResources().getColor(R.color.background));
		}
		return view;
	}

	private class OnClickListenerDelete implements OnClickListener {
		IOfferItem m_oItem;
		public OnClickListenerDelete(IOfferItem item) {
			super();
			m_oItem = item;
		}
		@Override
		public void onClick(View v) {
			// set inactive in database
//			m_oItem.setStatus(ItemStatus.INACTIVE);
//			new UpdateOfferAndRefreshView().execute(m_oItem);
			// TODO: we'll probably want to change this back to update with inactive when the server will support this
			Intent i = new Intent(getContext(), RemoteDBService.class);
			i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.RemoveOffer);
			i.putExtra(RemoteDBService.PARAM_OBJECT, m_oItem);
			getContext().startService(i);
			// TODO: need to add list extension and move this item there. when it will be removed from there, it will really deleted.
//			m_adapter.notifyDataSetChanged();
			// TODO: later change updating the list to some cool animation
		}
	}

	private class OnClickListenerEdit implements OnClickListener {
		IOfferItem m_oItem;
		Context m_context;
		public OnClickListenerEdit(IOfferItem item, Context context) {
			super();
			m_oItem = item;
			m_context = context;
		}
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(m_context,EditOfferActivity.class);
    		intent.putExtra(OfferActivity.EXTRA_OFFERITEM, m_oItem);
    		m_context.startActivity(intent);
		}
	}
	private class ViewHolder
	{
		public ImageButton editButton;
		public ImageButton deleteButton;
		public ImageView unsynced;
		public RelativeLayout container;
		public TextView name,number,edition,condition,price,msgsCount;
	}

	@Override
	public void notifyDataSetChanged() {
		ArrayList<IOfferItem> myOffers = CacheManager.getManager(m_activity).getMyOffers();
		syncingOrUnsyncedOffers  = CacheManager.getManager(getActivity()).getSyncingOrUnsyncedOffers();
		myOffers.addAll(syncingOrUnsyncedOffers);
		IOfferItem[] array = myOffers.toArray(new IOfferItem[myOffers.size()]);
		setData(array);
		Log.v("OffersListAdapter","DataSetChanged");
		super.notifyDataSetChanged();
	}

	private OnTouchListener getListner(final IOfferItem offer) {
		return new OnTouchListnerWithOnClick() {
			IOfferItem m_item = offer;

			@Override
			protected void runOnClickFunction()
			{
				IOnOffersItemPickedListner listner = (IOnOffersItemPickedListner) getActivity();
				listner.OnOffersItemPicked(m_item);
			}

			@Override
			protected ListView getListView() {
				return m_lstView;
			}

			@Override
			protected Context getContext() {
				return getActivity();
			}
		};
	}

	private Activity getActivity()
	{
		return m_activity;
	}
}
