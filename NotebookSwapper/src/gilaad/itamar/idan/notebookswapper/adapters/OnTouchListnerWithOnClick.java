package gilaad.itamar.idan.notebookswapper.adapters;

import gilaad.itamar.idan.notebookswapper.R;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;

abstract public class OnTouchListnerWithOnClick implements OnTouchListener {
		float mDownX;
		private int mSwipeSlop = -1;
		private final Runnable m_run = null;
		private boolean mItemPressed;
		private boolean mSwiping;

		private static final int SWIPE_DURATION = 250;
		private static final int MOVE_DURATION = 150;

		@Override
		public boolean onTouch(final View v, MotionEvent event) {
			final RelativeLayout tmp = (RelativeLayout)v.findViewById(R.id.textContainer);
			if (mSwipeSlop < 0) {
				mSwipeSlop = ViewConfiguration.get(getContext()).
						getScaledTouchSlop();
			}
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				if (mItemPressed) {
					return false;
				}
				mItemPressed = true;
				mSwiping=false;
				mDownX = event.getX();
				break;
			case MotionEvent.ACTION_CANCEL:
				//tmp.setAlpha(1);
				tmp.setTranslationX(0);
				mItemPressed = false;
				break;
			case MotionEvent.ACTION_MOVE:
			{
				float x = event.getX() + v.getTranslationX();
				float deltaX = x - mDownX;
				float deltaXAbs = Math.abs(deltaX);
				if (!mSwiping) {
					if (deltaXAbs > mSwipeSlop) {
						mSwiping = true;
						getListView().requestDisallowInterceptTouchEvent(true);
						ImageButton imageButton = (ImageButton)v.findViewById(R.id.deleteImgButton);
						imageButton.setVisibility(View.VISIBLE);
						imageButton.setClickable(true);
						ImageButton imageButton2 = (ImageButton)v.findViewById(R.id.editImgButton);
						imageButton2.setVisibility(View.VISIBLE);
						imageButton2.setClickable(true);
					}
				}
				if (mSwiping) {
					if ((x-mDownX)>0) {
						return true;
					}
					tmp.setTranslationX((x - mDownX));
				}
			}
			break;
			case MotionEvent.ACTION_UP:
			{
				// User let go - figure out whether to animate the view out, or back into place
				if (mSwiping) {
					float x = event.getX() + v.getTranslationX();
					float deltaX = x - mDownX;
					float deltaXAbs = Math.abs(deltaX);
					float fractionCovered;
					float endX;
					final boolean remove;
					if (deltaX < -( v.getWidth() / 4)) {
						// Greater than a quarter of the width - animate it out
						fractionCovered = deltaXAbs / v.getWidth();
						endX = deltaX < 0 ? -v.getWidth()*3/4 : 0;
						remove = true;
					} else {
						// Not far enough - animate it back
						fractionCovered = 1 - (deltaXAbs / v.getWidth());
						endX = 0;
						remove = false;
					}
					// Animate position and alpha of swiped item
					// NOTE: This is a simplified version of swipe behavior, for the
					// purposes of this demo about animation. A real version should use
					// velocity (via the VelocityTracker class) to send the item off or
					// back at an appropriate speed.
					long duration = (int) ((1 - fractionCovered) * SWIPE_DURATION);
					getListView().setEnabled(false);
					tmp.animate().setDuration(duration).translationX(endX).
					withEndAction(new Runnable() {
						@Override
						public void run() {
							// Restore animated values

							if (remove) {
								//animateRemoval(mListView, v);

							} else {
								ImageButton imageButton = (ImageButton)v.findViewById(R.id.deleteImgButton);
								imageButton.setVisibility(View.GONE);
								imageButton.setClickable(false);
								ImageButton imageButton2 = (ImageButton)v.findViewById(R.id.editImgButton);
								imageButton2.setVisibility(View.GONE);
								imageButton2.setClickable(false);
								tmp.clearAnimation();
								mSwiping = false;
								getListView().setEnabled(true);
							}
						}
					});
				}
				else
				{
					runOnClickFunction();
				}
			}
			mItemPressed = false;
			break;
			default:
				return false;
			}
			return true;
		}

		abstract protected ListView getListView();
		abstract protected Context getContext();
		abstract protected void runOnClickFunction();
}
