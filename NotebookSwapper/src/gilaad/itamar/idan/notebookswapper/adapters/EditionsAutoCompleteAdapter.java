package gilaad.itamar.idan.notebookswapper.adapters;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class EditionsAutoCompleteAdapter extends ArrayAdapter<String>
{
	private final LayoutInflater inflator;

	public EditionsAutoCompleteAdapter(Activity context, List<String> list) {
        super(context, android.R.layout.simple_spinner_item, list);
        inflator = context.getLayoutInflater();
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view;
		if (null == convertView)
		{
			view = inflator.inflate(android.R.layout.simple_spinner_item, parent, false);
			ViewHolder vh = new ViewHolder();
			vh.text = ((TextView)view.findViewById(android.R.id.text1));
			view.setTag(vh);
		}
		else
		{
			view = convertView;
		}
		TextView tv = ((ViewHolder) view.getTag()).text;
		String item = getItem(position);
		tv.setText(item);
		return view;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		View view;
		if (null == convertView)
		{
			view = inflator.inflate(android.R.layout.simple_spinner_dropdown_item, null);
			ViewHolder vh = new ViewHolder();
			vh.text = ((TextView)view.findViewById(android.R.id.text1));
			view.setTag(vh);
		}
		else
		{
			view = convertView;
		}
		TextView tv = ((ViewHolder) view.getTag()).text;
		String item = getItem(position);
		tv.setText(item);
		return view;
	}

	private class ViewHolder
	{
		public TextView text;
	}
}
