package gilaad.itamar.idan.notebookswapper.adapters;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.accounts.AccountManager;
import gilaad.itamar.idan.notebookswapper.animation.DefaultAnimation;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.searchnotebook.IMatchCatalogListener;
import gilaad.itamar.idan.notebookswapper.searchnotebook.ISearchItem;
import gilaad.itamar.idan.notebookswapper.searchnotebook.NotebookItem;
import gilaad.itamar.idan.notebookswapper.searchnotebook.SectionItem;
import gilaad.itamar.idan.notebookswapper.wishactivity.AddWishActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SearchCatalogAdapter extends AbstractNotebookAdapter<ISearchItem> {

	public NotebookItem setQuery(String query)
	{
		List<ICourse> searchCoursesByNameOrNumber = CacheManager.getManager(getContext()).searchCoursesByNameOrNumber(query);
		Map<String,List<ICourse>> mapFacultyToCourse = new HashMap<String, List<ICourse>>();
		for (ICourse course : searchCoursesByNameOrNumber)
		{
			String faculty = course.getFaculty();
			List<ICourse> listCourses = mapFacultyToCourse.get(faculty);
			if (listCourses == null)
			{
				listCourses = new ArrayList<ICourse>();
				mapFacultyToCourse.put(faculty, listCourses);
			}
			listCourses.add(course);
		}
		ISearchItem[] array = makeSearchItemsList(mapFacultyToCourse);
		setData(array);
		if (array.length == 2)
		{
			return (NotebookItem) array[1];
		}
		return null;
	}

	public SearchCatalogAdapter(Context context){
		super(context);
		setData(new ISearchItem[0]);
	}

	private ISearchItem[] makeSearchItemsList(Map<String, List<ICourse>> mapFacoultyToCourse) {
		List<ISearchItem> lstSearchItems = new ArrayList<ISearchItem>();
		for (Entry<String, List<ICourse>> ent : mapFacoultyToCourse.entrySet()) {
			lstSearchItems.add(new SectionItem(ent.getKey()));
			for (ICourse course : ent.getValue()) {
				for (ICatalogItem $ : CacheManager.getManager(getContext()).getCatalogItemsForCourse(course)) {
					lstSearchItems.add(new NotebookItem($,course));
				}
			}
		}
		return lstSearchItems.toArray(new ISearchItem[lstSearchItems.size()]);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inf = LayoutInflater.from(m_context);
		View view;
		ISearchItem item = getItem(position);
		if (null == convertView)
		{
			if (item instanceof SectionItem)
			{
				view = inf.inflate(R.layout.seach_notebook_header, null);
				ViewHolderForSection vh = new ViewHolderForSection();
				vh.header = (TextView)view.findViewById(R.id.txtHeaderName);
				view.setTag(vh);
			}
			else
			{
				view = inf.inflate(R.layout.search_notebook_item, null);
				ViewHolderForItem vh = new ViewHolderForItem();
				vh.title = (TextView)view.findViewById(R.id.txtTitle);
				vh.number = (TextView)view.findViewById(R.id.txtCourseNumber);
				vh.edition =(TextView)view.findViewById(R.id.txtCourseEdition);
				vh.btnAdd = (ImageButton)view.findViewById(R.id.addImgButton);
				vh.itemFrame = (RelativeLayout)view.findViewById(R.id.frmCatalogItem);
				view.setTag(vh);
			}

		}
		else
		{
			Object tag = convertView.getTag();
			if (tag instanceof ViewHolderForSection)
			{
				if (item instanceof SectionItem)
				{
					view = convertView;
				}
				else
				{
					view = inf.inflate(R.layout.search_notebook_item, null);
					ViewHolderForItem vh = new ViewHolderForItem();
					vh.title = (TextView)view.findViewById(R.id.txtTitle);
					vh.number = (TextView)view.findViewById(R.id.txtCourseNumber);
					vh.edition =(TextView)view.findViewById(R.id.txtCourseEdition);
					vh.btnAdd = (ImageButton)view.findViewById(R.id.addImgButton);
					vh.itemFrame = (RelativeLayout)view.findViewById(R.id.frmCatalogItem);
					view.setTag(vh);
				}
			}
			else
			{
				if (item instanceof SectionItem)
				{
					view = inf.inflate(R.layout.seach_notebook_header, null);
					ViewHolderForSection vh = new ViewHolderForSection();
					vh.header = (TextView)view.findViewById(R.id.txtHeaderName);
					view.setTag(vh);
				}
				else
				{
					view = convertView;
				}
			}
		}

		if (item instanceof SectionItem)
		{
			ViewHolderForSection vh =(ViewHolderForSection) view.getTag();
			SectionItem secItem = (SectionItem) item;
			vh.header.setText(secItem.getHeader());
			view.setClickable(false);
		}
		else
		{
			ViewHolderForItem vh = (ViewHolderForItem) view.getTag();
			final NotebookItem notebookItem = (NotebookItem) item;
			ICatalogItem catItem = notebookItem.getCatalogItem();
			ICourse courseItem = notebookItem.getCourse();

			vh.title.setText(courseItem.getCourseName() + " - " + catItem.getType().toName());
			vh.number.setText(Integer.toString(catItem.getCourseNumber()));
			vh.edition.setText(catItem.getEdition());
			vh.itemFrame.setOnClickListener(new OnClickCourseListener(notebookItem));

			if (AccountManager.getCurrentAccount(m_context.getApplicationContext()).isGuest())
			{
				vh.btnAdd.setVisibility(View.GONE);
			} else
			{
				vh.btnAdd.setVisibility(View.VISIBLE);
				vh.btnAdd.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v) {
						final Animation animPress = DefaultAnimation.getDefaultButtonAnimation(getContext());
						v.startAnimation(animPress);
						Intent intent = new Intent(getContext(),AddWishActivity.class);
						intent.putExtra(AddWishActivity.NOTEBOOK, notebookItem);
						getContext().startActivity(intent);
					}
				});
			}
		}
		return view;
	}

	private class ViewHolderForItem
	{
		public RelativeLayout itemFrame;
		public ImageButton btnAdd;
		public TextView title,number,edition;
	}

	private class ViewHolderForSection
	{
		public TextView header;
	}

	class OnClickCourseListener implements OnClickListener{

		private final NotebookItem m_item;
		public OnClickCourseListener(NotebookItem item)
		{
			m_item = item;
		}
		@Override
		public void onClick(View v) {
			((IMatchCatalogListener)getContext()).onMatchCatalog(m_item);
		}

	}

}
