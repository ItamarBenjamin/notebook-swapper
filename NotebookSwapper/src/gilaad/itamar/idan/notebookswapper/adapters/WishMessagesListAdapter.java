package gilaad.itamar.idan.notebookswapper.adapters;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.asynctasks.DownloadImageTask;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IUserNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;
import gilaad.itamar.idan.notebookswapper.wishactivity.IOnWishMessagePickedListener;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class WishMessagesListAdapter extends AbstractNotebookAdapter<Message> {

	private final ListView m_lstView;
	private final Activity m_activity;
	private final Long m_myWishID;

	public WishMessagesListAdapter(Activity activity,ListView lstView,Long wishID) {
		super(activity);
		m_activity = activity;
		m_lstView = lstView;
		m_myWishID = wishID;
	}

	@Override
	public void notifyDataSetChanged() {
		List<Message> myWishMessages = CacheManager.getManager(m_activity).getMyWishMessages(m_myWishID);
		List<Message> myLastOfferMessages = new ArrayList<Message>();
		for (Message message : myWishMessages)
		{
			boolean bFound = false;
			for (Message lastMessage : myLastOfferMessages)
			{
				if (lastMessage.offerId.equals(message.offerId))
				{
					if (lastMessage.timestamp > message.timestamp)
					{
						bFound = true;
						break;
					}
					else
					{
						//Message is newer
						myLastOfferMessages.remove(lastMessage);
						break; //Don't remove or concurrent modification exception
					}
				}
			}

			if (!bFound)
			{
				myLastOfferMessages.add(message);
			}
		}
		Collections.sort(myLastOfferMessages,new Comparator<Message>() {

			@Override
			public int compare(Message lhs, Message rhs) {
				long l = rhs.timestamp-lhs.timestamp;
				if (l == 0)
				{
					return 0;
				}
				else
				{
					if (l<0) {
						return -1;
					} else {
						return 1;
					}
				}
			}

		});
		Message[] array = myLastOfferMessages.toArray(new Message[myLastOfferMessages.size()]);
		setData(array);
		Log.v("WishMessageListAdapter","DataSetChanged");
		super.notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inf = LayoutInflater.from(m_context);
		View view;
		if (null == convertView)
		{
			view = inf.inflate(R.layout.msg_small, null);
			ViewHolder vh = new ViewHolder();
			vh.message = ((TextView)view.findViewById(R.id.txtMessageBody));
			vh.fromUser = ((TextView)view.findViewById(R.id.txtFromUser));
			vh.userImg = (ImageView)view.findViewById(R.id.imgUserProfile);
			vh.time = (TextView)view.findViewById(R.id.txtMessageTime);
			view.setTag(vh);
		}
		else
		{
			view = convertView;
		}
		Message item = getItem(position);
		setOnClickListner(view,item);
		ViewHolder vh = (ViewHolder) view.getTag();
		vh.time.setText(getTimeSent(item));
		vh.message.setText(item.contents);
		vh.fromUser.setText(getOfferUser(item));
		setImageBitmap(item,vh.userImg);
		return view;
	}

	private void setImageBitmap(Message item, ImageView userImg) {
		String sUserID = item.fromOffer ? item.sender : item.receiver;
		IUserNotebook user = CacheManager.getManager(getContext()).getUserByID(sUserID);
		String sImageUrl = user.getImgUrl();
		Bitmap image = CacheManager.getManager(getContext()).getProfileImage(sImageUrl);
		if (image == null)
		{
			String[] urls = {sImageUrl};
			new DownloadImageTask(userImg, getContext(),false).execute(urls);
			image = BitmapFactory.decodeResource(getContext().getResources(),R.drawable.user);
		}
		userImg.setImageBitmap(image);
	}

	private String getTimeSent(Message item) {
		Date date = new Date(item.timestamp);
		Format format = new SimpleDateFormat("MM/dd HH:mm:ss");
		return format.format(date).toString();
	}


	private SpannableString getOfferUser(Message item)
	{
		String sUserID = item.fromOffer ? item.sender : item.receiver;
		String sName = CacheManager.getManager(getContext()).getUserByID(sUserID).getUsername();
		SpannableString spanString = new SpannableString(sName);
		spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
		return spanString;
	}

	private void setOnClickListner(View view, final Message msg) {
		view.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				IOnWishMessagePickedListener listner = (IOnWishMessagePickedListener) m_activity;
				listner.OnMessagePicked(msg);
			}
		});
	}

	private class ViewHolder
	{
		public TextView time;
		public ImageView userImg;
		public TextView fromUser,message;
	}

}
