package gilaad.itamar.idan.notebookswapper.adapters;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class FullMessagesListAdapter extends AbstractNotebookAdapter<Message> {

	private final Long m_wishID;
	private final Long m_offerID;
	private final boolean m_bByWish;
	private final Activity m_activity;
	private Bitmap m_BitmapMe;
	private Bitmap m_BitmapOther;
	private int m_MsgSize;

	public FullMessagesListAdapter(Activity activity,Long wishID,Long offerID,boolean bByWish, Bitmap m_BitmapMe, Bitmap m_BitmapOther) {
		super(activity);
		m_wishID = wishID;
		m_offerID = offerID;
		m_bByWish = bByWish;
		m_activity = activity;
		getDimensions(activity);
		this.m_BitmapMe = m_BitmapMe;
		this.m_BitmapOther = m_BitmapOther;
	}

	private void getDimensions(Activity activity) {
		WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		m_MsgSize = ((Double) (width * 0.7)).intValue();
	}

	@Override
	public void notifyDataSetChanged() {
		ArrayList<Message> myMessages = new ArrayList<Message>();
		ArrayList<Message> readMessages = new ArrayList<Message>();
		if (m_bByWish)
		{
			List<Message> allMyWishesMessages = CacheManager.getManager(m_activity).getMyWishMessages(m_wishID);
			for (Message message : allMyWishesMessages) {
				if (message.offerId.equals(m_offerID))
				{
					if (message.fromOffer)
					{
						readMessages.add(message);
					}
					myMessages.add(message);
				}
			}
		}
		else
		{
			List<Message> allMyOffersMessages = CacheManager.getManager(m_activity).getMyOffersMessages(m_offerID);
			for (Message message : allMyOffersMessages) {
				if (message.wishId.equals(m_wishID))
				{
					if (!message.fromOffer)
					{
						readMessages.add(message);
					}
					myMessages.add(message);
				}
			}
		}
		Collections.sort(myMessages,new Comparator<Message>() {

			@Override
			public int compare(Message lhs, Message rhs) {
				long l = lhs.timestamp-rhs.timestamp;
				if (l == 0)
				{
					return 0;
				}
				else
				{
					if (l<0) {
						return -1;
					} else {
						return 1;
					}
				}
			}

		});

		Message[] array = myMessages.toArray(new Message[myMessages.size()]);
		List<Message> newRead = new ArrayList<Message>();
		for (Message message : readMessages) {
			if (!message.isRead)
			{
				newRead.add(message);
			}
		}
		if (!newRead.isEmpty())
		{
			Intent i = new Intent(m_activity , RemoteDBService.class);
			i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.MessagesRead);
			i.putExtra(RemoteDBService.PARAM_OBJECT, readMessages);
			m_activity.startService(i);
		}
		setData(array);
		Log.v("FullMessagesListAdapter","DataSetChanged");
		super.notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inf = LayoutInflater.from(m_context);
		RelativeLayout view;
		if (null == convertView)
		{
			view = (RelativeLayout) inf.inflate(R.layout.msg_full, null);
			view.setLayoutParams(new AbsListView.LayoutParams(m_MsgSize,AbsListView.LayoutParams.WRAP_CONTENT));
			ViewHolder vh = new ViewHolder();
			vh.message = ((TextView)view.findViewById(R.id.txtMessageBody));
			vh.fromUser = ((TextView)view.findViewById(R.id.txtFromUser));
			vh.timeSent = ((TextView)view.findViewById(R.id.txtMessageTime));
			vh.userImg = (ImageView)view.findViewById(R.id.imgUserProfile);
			view.setTag(vh);
		}
		else
		{
			view = (RelativeLayout) convertView;
		}
		Message item = getItem(position);

		ViewHolder vh = (ViewHolder) view.getTag();

		vh.message.setText(item.contents);
		vh.fromUser.setText(getUser(item));
		vh.timeSent.setText(getTimeSent(item));
		setImageBitmap(item, vh.userImg);
		setUserAttributes(view,item);
		return view;
	}

	private void setUserAttributes(RelativeLayout view, Message item) {
		if ((m_bByWish && !item.fromOffer) || (!m_bByWish && item.fromOffer))
		{
			view.setBackground(m_activity.getResources().getDrawable(R.drawable.msg_drawable));
			view.setGravity(Gravity.LEFT);
		}
		else
		{
			view.setBackground(m_activity.getResources().getDrawable(R.drawable.msg_drawable_other_user));
			view.setGravity(Gravity.RIGHT);
		}
	}

	private String getTimeSent(Message item) {
		Date date = new Date(item.timestamp);
		Format format = new SimpleDateFormat("MM/dd HH:mm:ss");
		return format.format(date).toString();
	}

	private SpannableString getUser(Message item)
	{
		String sName;
		if (m_bByWish && !item.fromOffer || !m_bByWish && item.fromOffer)
		{
			sName= getContext().getString(R.string.me);
		}
		else
		{
			sName= CacheManager.getManager(getContext()).getUserByID(item.sender).getUsername();
		}

		SpannableString spanString = new SpannableString(sName);
		spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
		return spanString;

	}

	private void setImageBitmap(Message item, ImageView userImg) {
		Bitmap image =  null;
		if (m_bByWish && !item.fromOffer || !m_bByWish && item.fromOffer)
		{
			image= m_BitmapMe;
		}
		else
		{
			image= m_BitmapOther;
		}
		if (image == null)
		{
			image =BitmapFactory.decodeResource(getContext().getResources(),R.drawable.user);
		}
		userImg.setImageBitmap(image);
	}

	private class ViewHolder
	{
		public ImageView userImg;
		public TextView fromUser,message,timeSent;
	}

	public void setMyImg(Bitmap bitmap) {
		m_BitmapMe = bitmap;
	}

	public void setOtherImg(Bitmap bitmap) {
		m_BitmapOther = bitmap;

	}

}