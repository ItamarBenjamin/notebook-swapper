package gilaad.itamar.idan.notebookswapper.adapters;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.asynctasks.DownloadImageTask;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IUserNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;
import gilaad.itamar.idan.notebookswapper.offer.IOnOfferMessagePickedListner;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class OfferMessagesListAdapter extends AbstractNotebookAdapter<Message> {

	private final Activity m_activity;
	private final Long m_myOfferID;
	private Message[] m_Array;

	public OfferMessagesListAdapter(Activity activity,Long offerID) {
		super(activity);
		m_activity = activity;
		m_myOfferID = offerID;
	}

	@Override
	public void notifyDataSetChanged() {
		List<Message> myWishMessages = CacheManager.getManager(m_activity).getMyOffersMessages(m_myOfferID);
		List<Message> myLastWishMessages = new ArrayList<Message>();
		for (Message message : myWishMessages)
		{
			boolean bFound = false;
			for (Message lastMessage : myLastWishMessages)
			{
				if (lastMessage.wishId.equals(message.wishId))
				{
					if (lastMessage.timestamp > message.timestamp)
					{
						bFound = true;
						break;
					}
					else
					{
						//Message is newer
						myLastWishMessages.remove(lastMessage);
						break; //Don't remove or concurrent modification exception
					}
				}
			}

			if (!bFound)
			{
				myLastWishMessages.add(message);
			}
		}
		Collections.sort(myLastWishMessages,new Comparator<Message>() {

			@Override
			public int compare(Message lhs, Message rhs) {
				long l = rhs.timestamp-lhs.timestamp;
				if (l == 0)
				{
					return 0;
				}
				else
				{
					if (l<0) {
						return -1;
					} else {
						return 1;
					}
				}
			}

		});
		m_Array = myLastWishMessages.toArray(new Message[myLastWishMessages.size()]);
		setData(m_Array);
		Log.v("OfferMessagesListAdapter","DataSetChanged");
		super.notifyDataSetChanged();
	}

	@Override
	public boolean isEmpty()
	{
		return m_Array.length == 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inf = LayoutInflater.from(m_context);
		View view;
		if (null == convertView)
		{
			view = inf.inflate(R.layout.msg_small, null);
			ViewHolder vh = new ViewHolder();
			vh.message = ((TextView)view.findViewById(R.id.txtMessageBody));
			vh.fromUser = ((TextView)view.findViewById(R.id.txtFromUser));
			vh.time = (TextView)view.findViewById(R.id.txtMessageTime);
			vh.userImg = (ImageView)view.findViewById(R.id.imgUserProfile);
			view.setTag(vh);
		}
		else
		{
			view = convertView;
		}
		Message item = getItem(position);
		setOnClickListner(view,item);
		ViewHolder vh = (ViewHolder) view.getTag();

		vh.time.setText(getTimeSent(item));
		vh.message.setText(item.contents);
		vh.fromUser.setText(getWisherUser(item));
		setImageBitmap(item,vh.userImg);
		return view;
	}

	private SpannableString getWisherUser(Message item)
	{
		String sUser = item.fromOffer ? item.getReceiver() : item.getSender();
		String sName = CacheManager.getManager(getContext()).getUserByID(sUser).getUsername();
		SpannableString spanString = new SpannableString(sName);
		spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
		return spanString;
	}

	private String getTimeSent(Message item) {
		Date date = new Date(item.timestamp);
		Format format = new SimpleDateFormat("MM/dd HH:mm:ss");
		return format.format(date).toString();
	}

	private void setOnClickListner(View view, final Message msg) {
		view.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				IOnOfferMessagePickedListner listner = (IOnOfferMessagePickedListner) m_activity;
				listner.OnMessagePicked(msg);
			}
		});
	}


	private void setImageBitmap(Message item, ImageView userImg) {
		String sUserID = item.fromOffer ?  item.receiver : item.sender;
		IUserNotebook user = CacheManager.getManager(getContext()).getUserByID(sUserID);
		String sImageUrl = user.getImgUrl();
		Bitmap image = CacheManager.getManager(getContext()).getProfileImage(sImageUrl);
		if (image == null)
		{
			String[] urls = {sImageUrl};
			image = BitmapFactory.decodeResource(getContext().getResources(),R.drawable.user);
			userImg.setImageBitmap(image);
			new DownloadImageTask(userImg, getContext(),false).execute(urls);
		}
		else
		{
			userImg.setImageBitmap(image);
		}

	}

	private class ViewHolder
	{
		public ImageView userImg;
		public TextView time;
		public TextView fromUser,message;
	}
}
