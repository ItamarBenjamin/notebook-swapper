package gilaad.itamar.idan.notebookswapper.adapters;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.basket.IOnWishsItemPickedListener;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService;
import gilaad.itamar.idan.notebookswapper.wishactivity.EditWishActivity;
import gilaad.itamar.idan.notebookswapper.wishactivity.WishActivity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class WishlistAdapter extends AbstractNotebookAdapter<IWishlistItem> {

	private final ListView m_lstView;
	private final Activity m_activity;
	private List<IWishlistItem> syncingOrUnsyncedWishs;

	public WishlistAdapter(Activity activity,ListView lstView) {
		super(activity);
		m_activity = activity;
		m_lstView = lstView;
	}

	@Override
	public void notifyDataSetChanged() {
		ArrayList<IWishlistItem> myWishes = CacheManager.getManager(m_activity).getMyWishes();
		syncingOrUnsyncedWishs = CacheManager.getManager(getActivity()).getSyncingOrUnsyncedWishs();
		myWishes.addAll(syncingOrUnsyncedWishs);
		IWishlistItem[] array = myWishes.toArray(new IWishlistItem[myWishes.size()]);
		setData(array);
		Log.v("WishlistAdapter","DataSetChanged");
		super.notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inf = LayoutInflater.from(m_context);
		View view;
		IWishlistItem item = getItem(position);
		boolean bUnsynced = syncingOrUnsyncedWishs.contains(item);
		if (null == convertView)
		{
			view = inf.inflate(R.layout.wishlist_item, null);
			ViewHolder vh = new ViewHolder();
			vh.name = (TextView)view.findViewById(R.id.txtCourseName);
			vh.number = (TextView)view.findViewById(R.id.txtCourseNumber);
			vh.type = (TextView)view.findViewById(R.id.txtNotebookType);
			vh.delete = ((ImageButton)view.findViewById(R.id.deleteImgButton));
			vh.edit = ((ImageButton)view.findViewById(R.id.editImgButton));
			vh.msgCount = (TextView)view.findViewById(R.id.txtNewMsgsCount);
			vh.offersCount = (TextView)view.findViewById(R.id.txtNewOffersCount);
			vh.unsynced = (ImageView)view.findViewById(R.id.unsynced);
			vh.container = (RelativeLayout)view.findViewById(R.id.textContainer);
			vh.deleteButton = (ImageButton)view.findViewById(R.id.deleteImgButton);
			vh.editButton = (ImageButton)view.findViewById(R.id.editImgButton);
			view.setTag(vh);
		}
		else
		{
			view = convertView;
		}
		if (!bUnsynced)
		{
			view.setOnTouchListener(getListner(item));
		}
		else
		{
			view.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					return false;
				}
			});
		}
		ViewHolder vh = (ViewHolder) view.getTag();

		vh.container.setTranslationX(0);
		vh.delete.setOnClickListener(new OnClickListenerDelete(item));
		vh.edit.setOnClickListener(new OnClickListenerEdit(item,m_activity));

		ICourse course = CacheManager.getManager(m_activity).getCourse(item.getCourseNumber());
		Integer iCourseNumber = item.getCourseNumber();
		String sCourseName = course.getCourseName();

		vh.name.setText(sCourseName);
		vh.number.setText(iCourseNumber.toString());
		vh.type.setText(item.getType().toName());
		vh.offersCount.setText(getOffers(item.getWishID()));
		int iUnreadCounter = 0;
		vh.deleteButton.setVisibility(View.GONE);
		vh.deleteButton.setClickable(false);
		vh.editButton.setVisibility(View.GONE);
		vh.editButton.setClickable(false);
		List<Message> myWishMessages = CacheManager.getManager(getActivity()).getMyWishMessages(item.getWishID());
		for (Message message : myWishMessages) {
			if (!message.isRead)
			{
				if (message.fromOffer)
				{
					iUnreadCounter++;
				}
			}
		}
		vh.msgCount.setText(Integer.toString(iUnreadCounter));
		if (bUnsynced)
		{
			vh.unsynced.setVisibility(View.VISIBLE);
			vh.container.setBackgroundColor(m_activity.getResources().getColor(R.color.unsynced_item));
		}
		else
		{
			vh.unsynced.setVisibility(View.GONE);
			vh.container.setBackgroundColor(m_activity.getResources().getColor(R.color.background));
		}
		return view;
	}

	private String getOffers(Long wishID) {
		return Integer.toString(CacheManager.getManager(getActivity().getApplicationContext()).getOffersCount(wishID));
	}

	private OnTouchListener getListner(final IWishlistItem wish) {
		return new OnTouchListnerWithOnClick() {

			@Override
			protected void runOnClickFunction()
			{
				IOnWishsItemPickedListener listner = (IOnWishsItemPickedListener) getActivity();
				listner.OnWishItemPicked(wish);
			}

			@Override
			protected ListView getListView() {
				return m_lstView;
			}

			@Override
			protected Context getContext() {
				return getActivity();
			}
		};
	}

	private Activity getActivity()
	{
		return m_activity;
	}

	private class ViewHolder
	{
		public ImageButton editButton;
		public ImageButton deleteButton;
		public RelativeLayout container;
		public ImageView unsynced;
		public TextView offersCount;
		public TextView msgCount;
		public TextView name,number,type;
		public ImageButton delete, edit;
	}

	private class OnClickListenerDelete implements OnClickListener {
		IWishlistItem m_oItem;
		public OnClickListenerDelete(IWishlistItem item) {
			super();
			m_oItem = item;
		}
		@Override
		public void onClick(View v) {
			// set inactive in database
//			m_oItem.setStatus(ItemStatus.INACTIVE);
//			new UpdateOfferAndRefreshView().execute(m_oItem);
			// TODO: we'll probably want to change this back to update with inactive when the server will support this
			Intent i = new Intent(getContext(), RemoteDBService.class);
			i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.RemoveWish);
			i.putExtra(RemoteDBService.PARAM_OBJECT, m_oItem);
			getContext().startService(i);
//			new RemoveOfferAndRefreshView().execute(m_oItem);
			// TODO: need to add list extension and move this item there. when it will be removed from there, it will really deleted.
//			m_adapter.notifyDataSetChanged();
			// TODO: later change updating the list to some cool animation
		}
	}

	private class OnClickListenerEdit implements OnClickListener {
		IWishlistItem m_oItem;
		Context m_context;
		public OnClickListenerEdit(IWishlistItem item, Context context) {
			super();
			m_oItem = item;
			m_context = context;
		}
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(m_context,EditWishActivity.class);
    		intent.putExtra(WishActivity.EXTRA_WISHITEM, m_oItem);
    		m_context.startActivity(intent);
		}
	}
}
