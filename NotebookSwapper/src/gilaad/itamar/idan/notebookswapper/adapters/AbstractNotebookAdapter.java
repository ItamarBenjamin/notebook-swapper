package gilaad.itamar.idan.notebookswapper.adapters;

import android.content.Context;
import android.widget.BaseAdapter;

public abstract class AbstractNotebookAdapter<IType> extends BaseAdapter {

	IType[] m_Data;
	Object syncObject = new Object();
	Context m_context;

	public AbstractNotebookAdapter(Context context) {
		m_context = context;
	}

	@Override
	public int getCount() {
		synchronized (syncObject) {
			if (null == m_Data)
			{
				return 0;
			}
			return m_Data.length;
		}
	}

	@Override
	public IType getItem(int arg0) {
		synchronized (syncObject) {
			if (null == m_Data)
			{
				return null;
			}
			return m_Data[arg0];
		}
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	public void setData(IType... data) {
		synchronized (syncObject) {
			m_Data = data;
		}
	}

	public Context getContext()
	{
		return m_context;
	}
}
