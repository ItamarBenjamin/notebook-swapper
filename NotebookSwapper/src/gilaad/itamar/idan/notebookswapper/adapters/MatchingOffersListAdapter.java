package gilaad.itamar.idan.notebookswapper.adapters;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.searchnotebook.IMatchOfferItem;
import gilaad.itamar.idan.notebookswapper.searchnotebook.IMatchOfferListener;
import gilaad.itamar.idan.notebookswapper.searchnotebook.MatchOfferItem;
import gilaad.itamar.idan.notebookswapper.searchnotebook.MatchOfferSection;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MatchingOffersListAdapter extends AbstractNotebookAdapter<IMatchOfferItem> {

	private final Activity m_activity;
	private final Long m_wishID;
	private boolean m_bClickable = true;

	public MatchingOffersListAdapter(Activity activity,Long wishID) {
		super(activity);
		m_activity = activity;
		m_wishID = wishID;
	}

	public MatchingOffersListAdapter(Activity activity,Long wishID, boolean enableClick) {
		super(activity);
		m_activity = activity;
		m_wishID = wishID;
		m_bClickable = enableClick;
	}

	@Override
	public void notifyDataSetChanged() {
		if (null != m_wishID)
		{
			CacheManager.getManager(getContext().getApplicationContext()).resetOffersCounter(m_wishID);
		}
		super.notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inf = LayoutInflater.from(m_context);
		View view;
		IMatchOfferItem item = getItem(position);
		if (null == convertView)
		{
			if (item instanceof MatchOfferSection)
			{
				view = inf.inflate(R.layout.seach_notebook_header, null);
				ViewHolderForSection vh = new ViewHolderForSection();
				vh.header = (TextView)view.findViewById(R.id.txtHeaderName);
				view.setTag(vh);
			}
			else
			{
				view = inf.inflate(R.layout.offer_item, null);
				ViewHolderForItem vh = new ViewHolderForItem();
				vh.name = (TextView)view.findViewById(R.id.txtCourseName);
				vh.edition = (TextView)view.findViewById(R.id.txtCourseEdition);
				vh.number = (TextView)view.findViewById(R.id.txtCourseNumber);
				((TextView)view.findViewById(R.id.txtNewMsgsCount)).setVisibility(View.INVISIBLE);
				vh.price = (TextView)view.findViewById(R.id.txtOfferNotebookPrice);
				vh.condition = (TextView)view.findViewById(R.id.txtOfferNotebookCondition);
				vh.container = (RelativeLayout)view.findViewById(R.id.textContainer);
				view.setTag(vh);
			}

		}
		else
		{
			Object tag = convertView.getTag();
			if (tag instanceof ViewHolderForSection)
			{
				if (item instanceof MatchOfferSection)
				{
					view = convertView;
				}
				else
				{
					view = inf.inflate(R.layout.offer_item, null);
					ViewHolderForItem vh = new ViewHolderForItem();
					vh.name = (TextView)view.findViewById(R.id.txtCourseName);
					vh.edition = (TextView)view.findViewById(R.id.txtCourseEdition);
					vh.number = (TextView)view.findViewById(R.id.txtCourseNumber);
					((TextView)view.findViewById(R.id.txtNewMsgsCount)).setVisibility(View.INVISIBLE);
					vh.price = (TextView)view.findViewById(R.id.txtOfferNotebookPrice);
					vh.condition = (TextView)view.findViewById(R.id.txtOfferNotebookCondition);
					vh.container = (RelativeLayout)view.findViewById(R.id.textContainer);
					view.setTag(vh);
				}
			}
			else
			{
				if (item instanceof MatchOfferSection)
				{
					view = inf.inflate(R.layout.seach_notebook_header, null);
					ViewHolderForSection vh = new ViewHolderForSection();
					vh.header = (TextView)view.findViewById(R.id.txtHeaderName);
					view.setTag(vh);
				}
				else
				{
					view = convertView;
				}
			}
		}

		if (item instanceof MatchOfferSection)
		{
			ViewHolderForSection vh =(ViewHolderForSection) view.getTag();
			MatchOfferSection secItem = (MatchOfferSection) item;
			vh.header.setText(secItem.getTitle());
			view.setClickable(false);
		}
		else
		{
			ViewHolderForItem vh = (ViewHolderForItem) view.getTag();
			MatchOfferItem matchOfferItem = (MatchOfferItem)item;
			IOfferItem offerItem = matchOfferItem.getOfferItem();
			CacheManager manager = CacheManager.getManager(m_activity);
			ICatalogItem oCatalogItem = manager.getCatalogItem(offerItem.getCatalogItemID());
			ICourse course = manager.getCourse(oCatalogItem.getCourseNumber());
			String free = getActivity().getString(R.string.Free);
			String coin = getActivity().getString(R.string.NewShekel);
			String sPrice = offerItem.getPrice() == 0 ? free : Integer.toString(offerItem.getPrice()) + " " + coin;
			vh.price.setText(sPrice);
			vh.condition.setText(offerItem.getCondition().toString());
			String sCourseName = course.getCourseName();
			Integer iCourseNumber = oCatalogItem.getCourseNumber();
			vh.name.setText(sCourseName + " - " + oCatalogItem.getType().toName());
			vh.edition.setText(oCatalogItem.getEdition());
			vh.number.setText(Integer.toString(iCourseNumber));
			if (m_bClickable)
			{
				view.setOnClickListener(new OnClickCourseListener(offerItem));
			}
		}
		return view;
	}

	class OnClickCourseListener implements OnClickListener{

		private final IOfferItem m_item;
		public OnClickCourseListener(IOfferItem item)
		{
			m_item = item;
		}
		@Override
		public void onClick(View v) {
			((IMatchOfferListener)getContext()).onMatchOffer(m_item);
		}

	}

	private class ViewHolderForItem
	{

		public RelativeLayout container;
		public TextView condition;
		public TextView price;
		public TextView number;
		public TextView edition;
		public TextView name;

	}

	private class ViewHolderForSection
	{
		public TextView header;
	}

	private Activity getActivity()
	{
		return m_activity;
	}
}
