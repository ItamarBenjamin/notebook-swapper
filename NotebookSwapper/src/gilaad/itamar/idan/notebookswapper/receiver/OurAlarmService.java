package gilaad.itamar.idan.notebookswapper.receiver;

import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.common.NetworkStatusChecker;
import gilaad.itamar.idan.notebookswapper.common.SnapshotTaker;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import gilaad.itamar.idan.notebookswapper.server.blobstore.Upload;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService;

import java.util.ArrayList;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;

public class OurAlarmService extends BroadcastReceiver {

	private static String TASK =  "task";
	private static int UPLOAD_UNSYNCED = 1;
	private static int CHECK_FOR_UPDATES = 2;

	public static void setAlarmService(Context context) {
		// alarm clock for syncing unsynced offers, wishes and images
		long interval = 30000; // 30 secibds
		setAlarmServiceAux(context.getApplicationContext(), interval, UPLOAD_UNSYNCED);
		// alarm clock for checking updates in server's database
		interval = 360000; // 3 min
		setAlarmServiceAux(context.getApplicationContext(), interval, CHECK_FOR_UPDATES);
	}

	private static void setAlarmServiceAux(Context context, long interval,
			int task) {
		Intent testReceiverIntent = new Intent(context.getApplicationContext(), OurAlarmService.class);
		testReceiverIntent.putExtra(TASK, task);
		AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		PendingIntent p = PendingIntent.getBroadcast(context, task, testReceiverIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		manager.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(), interval, p);
	}

	public static void stopUpdates(Context context)
	{
		cacnelAlarmServiceAux(context.getApplicationContext(), CHECK_FOR_UPDATES);
		cacnelAlarmServiceAux(context.getApplicationContext(), UPLOAD_UNSYNCED);
	}

	private static void cacnelAlarmServiceAux(Context context, int task) {
		Intent testReceiverIntent = new Intent(context.getApplicationContext(), OurAlarmService.class);
		testReceiverIntent.putExtra(TASK, task);
		AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		PendingIntent p = PendingIntent.getBroadcast(context, task, testReceiverIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		manager.cancel(p);
	}

	@Override
	public void onReceive(final Context context, Intent intent) {
		if (!NetworkStatusChecker.isNetworkAvailable(context)) {
			return;
		}
		int task = intent.getExtras().getInt(TASK);
		if (task == UPLOAD_UNSYNCED) {
			// check if there is anything to upload
			if (CacheManager.getManager(context).unsyncedItemsExists()) {
				Log.v("OurAlarmService", "Some unsynced items were found");
				AsyncTask<Void, Void, Void> a = new AsyncTask<Void, Void, Void>() {

					@Override
					protected Void doInBackground(Void... params) {
						uploadEverything(context);
						return null;
					}
				};
				a.execute((Void[])null);
			} else {
				Log.v("OurAlarmService", "No unsynced items");
			}
		} else if (task == CHECK_FOR_UPDATES) {
			Log.v("OurAlarmService", "Updating cache");
			CacheManager.getManager(context).update();
		}
	}


	// will return true if managed to upload everything false otherwise
	public void uploadEverything(Context context) {
			uploadUnsyncedOffers(context);
			uploadUnsyncedWishes(context);
	}

	private void uploadUnsyncedOffers(Context context) {
		ArrayList<IOfferItem> unsyncedOffers = CacheManager.getManager(context).takeOutUnsyncedOffers();
		Upload u = new Upload(context);
		boolean failed = false;
		for(IOfferItem curr: unsyncedOffers) {
			if (failed == true) {
				// already failed, put offers back to database
				CacheManager.getManager(context).addUnsyncedOffer(curr);
				continue;
			}
			String imgPath = curr.getImageUrl();
			if (null != imgPath && imageIsLocal(imgPath)) {
				String url = u.uploadImage(SnapshotTaker.getFullScaledBitmapFromPath(curr.getImageUrl()));
				if (null == url) {
					// upload failed, no reason to continue
					Log.e("OurAlarmService", "failed to upload image - will try again later");
					failed = true;
					CacheManager.getManager(context).addUnsyncedOffer(curr);
					continue;
				}
				// upload successful
				curr.setImageUrl(url);
			}
			sendOffer(context, curr);
		}
	}


	private boolean imageIsLocal(String imageUri) {
		return !imageUri.startsWith("http");
	}

	private void sendOffer(Context context, IOfferItem curr) {
		Intent i = new Intent(context, RemoteDBService.class);
		if (null == curr.getOfferID()) {
			i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.AddOffer);
		} else {
			i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.UpdateOffer);
		}
		i.putExtra(RemoteDBService.PARAM_OBJECT, curr);
		context.startService(i);
	}

	private void uploadUnsyncedWishes(Context context) {
		ArrayList<IWishlistItem> unsyncedWishes = CacheManager.getManager(context).takeOutUnsyncedWishes();
		for(IWishlistItem curr: unsyncedWishes) {
			Intent i = new Intent(context, RemoteDBService.class);
			if (null == curr.getWishID()) {
				i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.AddWish);
			} else {
				i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.UpdateWish);
			}
			i.putExtra(RemoteDBService.PARAM_OBJECT, curr);
			context.startService(i);
		}
	}

}

