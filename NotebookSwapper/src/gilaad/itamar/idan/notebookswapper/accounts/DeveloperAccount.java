package gilaad.itamar.idan.notebookswapper.accounts;


public class DeveloperAccount extends Account {
	private static final long serialVersionUID = -544873626649260669L;
	public static final String id = "root";

	@Override
	public boolean isGuest() {
		return false;
	}
	@Override
	public String getUID() {
		return id;
	}
	@Override
	public void signOut() {
		// Do nothing
	}
	@Override
	public boolean hasImage() {
		return false;
	}
	@Override
	public String getImageURL() {
		return "http://b.thumbs.redditmedia.com/o9N-Ebf0x1gkdgLO.png";
	}
	@Override
	public String getName() {
		return "Itamar";
	}
	@Override
	public String getEmail() {
		return "em@il.com";
	}

}
