package gilaad.itamar.idan.notebookswapper.accounts;


public class GuestAccount extends Account {
	private static final long serialVersionUID = 2179525521136473075L;
	public static final String id = "GUEST";

	/*
	 * (non-Javadoc)
	 *
	 * @see gilaad.itamar.idan.notebookswapper.accounts.Account#isGuest()
	 */
	@Override
	public boolean isGuest() {
		return true;
	}

	@Override
	public String getUID() {
		return id;
	}

	@Override
	public void signOut() {
		// nothing to be done
	}

	@Override
	public boolean hasImage() {
		return false;
	}

	@Override
	public String getImageURL() {
		return null;
	}

	@Override
	public String getName() {
		return "אורח";
	}
	@Override
	public String getEmail() {
		return "em@il.com";
	}
}
