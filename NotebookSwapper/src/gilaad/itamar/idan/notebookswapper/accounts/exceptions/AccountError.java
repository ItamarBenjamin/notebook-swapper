package gilaad.itamar.idan.notebookswapper.accounts.exceptions;

public class AccountError extends Exception {
	private static final long serialVersionUID = -5265800048576740074L;

	public AccountError(String message) {
		super(message);
	}
}
