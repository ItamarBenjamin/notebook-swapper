package gilaad.itamar.idan.notebookswapper.accounts;

import java.io.Serializable;

public abstract class Account implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 3411583072833037750L;
	public abstract String getUID();
	public abstract boolean isGuest();
	public abstract void signOut();
	public abstract boolean hasImage();
	public abstract String getName();
	public abstract String getEmail();
	public abstract String getImageURL();
	public int getGender()
	{
		return 0;
	}
}
