package gilaad.itamar.idan.notebookswapper.accounts;

import gilaad.itamar.idan.notebookswapper.asynctasks.DownloadImageTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;



public class AccountManager {
	private static String storeCurrAccountName = "_NbS_currentAccount_.ser";
	private static Account mCurrentAccount = null;
	public static final int PROFILE_PIC_SIZE = 400;

	public static Account getCurrentAccount(Context context) {
		if (null == mCurrentAccount) {
			loadCurrentAccountNonVolatile(context);
		}
		return mCurrentAccount;
	}

	public static void setGoogleAccount(GoogleApiClient googleApiClient, Context context) {
		logOff(context);
		mCurrentAccount = new UserAccount(googleApiClient);
		new DownloadImageTask(null, context,false).execute(mCurrentAccount.getImageURL());
		storeCurrentAccountNonVolatile(context);
	}

	public static void setGuestAccount(Context context) {
		logOff(context);
		mCurrentAccount = new GuestAccount();
		storeCurrentAccountNonVolatile(context);
	}

	public static void setDeveloperAccount(Context context) {
		logOff(context);
		mCurrentAccount = new DeveloperAccount();
		storeCurrentAccountNonVolatile(context);
	}

	public static void logOff(Context context) {
		if (isAccountStored(context)) {
			File fileStreamPath = context.getFileStreamPath(storeCurrAccountName);
			fileStreamPath.delete();
		}
		if (null != mCurrentAccount) {
			mCurrentAccount.signOut();
		}
		mCurrentAccount = null;
	}

	private static boolean isAccountStored(Context context) {
		File fileStreamPath = context.getFileStreamPath(storeCurrAccountName);
		Log.d("AccountManager", "File is " + fileStreamPath);
		return fileStreamPath.exists();
	}

	private static void storeCurrentAccountNonVolatile(Context context) {
		try  {
	         File fileStreamPath = context.getFileStreamPath(storeCurrAccountName);
			 FileOutputStream fileOut = new FileOutputStream(fileStreamPath);
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(mCurrentAccount);
	         out.close();
	         fileOut.close();
	         Log.v("AccountManager", "Serialized data to " + fileStreamPath);
	      } catch(IOException i) {
	          i.printStackTrace();
	      }
	}

	private static void loadCurrentAccountNonVolatile(Context context) {
		if (!isAccountStored(context)) {
			Log.i("AccountManager", "No account is stored");
			return;
		}
	    try {
	    	File fileStreamPath = context.getFileStreamPath(storeCurrAccountName);
			FileInputStream fileIn = new FileInputStream(fileStreamPath);
	    	ObjectInputStream in = new ObjectInputStream(fileIn);
	    	mCurrentAccount = (Account) in.readObject();
	    	in.close();
    		fileIn.close();
    		Log.v("AccountManager", "Deserialized data from "+ fileStreamPath);
	      }catch(IOException i) {
	    	  Log.i("AccountManager", "exception 1");
	         i.printStackTrace();
	         return;
	      }catch(ClassNotFoundException c) {
	    	  Log.i("AccountManager", "exception 2");
	         c.printStackTrace();
	         return;
	      }
	}
}
