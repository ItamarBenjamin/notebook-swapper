package gilaad.itamar.idan.notebookswapper.accounts;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

public class UserAccount extends Account {
	private static final long serialVersionUID = -7882600780999712061L;
	//	private final Date joinDate;
	private final String m_sEmail;
	private final String m_sUid;
	private String m_sName;
	private final int m_iGender;
	private final String m_sImageURL;
	private static final int PROFILE_PIC_SIZE = 100;

	public UserAccount(GoogleApiClient googleApiClient) {
		// Note: we assume gooogleApiClient.connect was already called.
		// and googleApiClient.disconnect will be call later. so we don't handle it here.
		// (Both are currently being called in LoginActivity)
    	Person person = Plus.PeopleApi.getCurrentPerson(googleApiClient);
    	if (null == person) {
    		// TODO: handle this case! (throw something)
    		// this could happen when disconnected from the internet...
    	}
		m_sName = person.getDisplayName();
        m_sUid = person.getId();
        m_iGender = person.getGender();
        m_sEmail = Plus.AccountApi.getAccountName(googleApiClient);
        m_sImageURL = getImageURL(person);
    }

	private String getImageURL(Person person) {
	    String personPhotoUrl = person.getImage().getUrl();
	    // get the image in our preferred resolution:
	    personPhotoUrl = personPhotoUrl.substring(0, personPhotoUrl.length() - 2) + PROFILE_PIC_SIZE;
	    return personPhotoUrl;
	}

	@Override
	public int getGender() {
		return m_iGender;
	}

	@Override
	public boolean isGuest() {
		return false;
	}

	/**
	 * @return The account's E-mail
	 */
	@Override
	public String getEmail() {
		return m_sEmail;
	}

	/**
	 * Changes the account's name to another uniq name
	 *
	 * @param name
	 *
	 */
	public void setName(String name) {
		m_sName = name;
	}

	@Override
	public String getName() {
		return m_sName;
	}

//	public Date getJoinDate() {
//		return joinDate;
//	}

	@Override
	public void signOut() {
		// nothing to do ATM
	}

	@Override
	public String getUID() {
		return m_sUid;
	}

	@Override
	public boolean hasImage() {
		return (m_sImageURL != null);
	}

	@Override
	public String getImageURL() {
		return m_sImageURL;
	}
}
