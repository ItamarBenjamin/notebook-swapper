package gilaad.itamar.idan.notebookswapper.service;

import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.cache.LocalCacheSQLiteOpenHelper.CacheTables;
import gilaad.itamar.idan.notebookswapper.common.NetworkStatusChecker;
import gilaad.itamar.idan.notebookswapper.common.SnapshotTaker;
import gilaad.itamar.idan.notebookswapper.dbhandler.RemoteDBDataManager;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IUserNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;
import gilaad.itamar.idan.notebookswapper.server.blobstore.IUpload;
import gilaad.itamar.idan.notebookswapper.server.blobstore.Upload;
import gilaad.itamar.idan.notebookswapper.server.notebook.api.ReturnCodeNotebook;
import gilaad.itamar.idan.notebookswapper.validation.DBException;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;

public class RemoteDBService extends IntentService {

	public static final String PARAM_OPERATION = "operation";
	public static final String PARAM_OBJECT = "object";
	public static final String PARAM_STORE_UNSYNCED = "store_unsynced";
	public static final String PARAM_ERROR = "error";

	public enum RemoteDBServiceOperations
	{
		AddOffer, UpdateOffer,RemoveOffer,
		AddWish,UpdateWish,RemoveWish,
		UpdateCatalog, UpdateCourses,UpdateMyOffers,UpdateMyWishes,
		UpdateMyMessages, SendMessage, GetGCMMessage, MessagesRead,
		AddUser,UploadImage, UpdateRelevantOffers, AddOffersFromMap,
		UpdateOffersFromMap, AddWishesFromMap, UpdateWishesFromMap, UpdateUsers, GetUser
	}


	public RemoteDBService() {
		super("RemoteDBService");
	}

	private void notifyFinished(RemoteDBServiceOperations operation, Serializable data)
	{
		Intent i = new Intent(operation.toString());
		if (null != data)
		{
			i.putExtra(PARAM_OBJECT, data);
		}
		RemoteDBService.this.sendBroadcast(i);
	}

	private void notifyError(RemoteDBServiceOperations operation)
	{
		Intent i = new Intent(operation.toString());
		i.putExtra(PARAM_ERROR, true);
		RemoteDBService.this.sendBroadcast(i);
	}

	private String uploadImage(Intent intent) throws DBException
	{
		IUpload upload = new Upload(getBaseContext());
		String imagePath = intent.getExtras().getString(PARAM_OBJECT);
		Bitmap bm = SnapshotTaker.getFullScaledBitmapFromPath(imagePath);
		String url = upload.uploadImage(bm);
		CacheManager.getManager(this).addImage(url,bm);
		return url;
	};

	private void addOffer(Intent intent)
	{
		IOfferItem offer = (IOfferItem) intent.getExtras().getSerializable(PARAM_OBJECT);
		boolean storeUnsynced = intent.getExtras().getBoolean(PARAM_STORE_UNSYNCED, true);
		addOfferAux(offer, storeUnsynced);
	}

	// returns true if upload was successful, false otherwise
	private boolean addOfferAux(IOfferItem offer, boolean storeUnsynced) {
		if (NetworkStatusChecker.isNetworkAvailable(getApplicationContext())) {
			IOfferItem itemNew = (new RemoteDBDataManager()).addOffer(offer);
			if (null != itemNew) {
				CacheManager.getManager(this).addOffer(itemNew);
				return true;
			}
		}
		// either network is not available or we got null from remoteDBDataManager - either way - save locally for later sync
		if (storeUnsynced) {
			CacheManager.getManager(getApplicationContext()).addUnsyncedOffer(offer);
		}
		return false;
	};

	private void updateOffer(Intent intent) throws DBException
	{
		IOfferItem offer = (IOfferItem) intent.getExtras().getSerializable(PARAM_OBJECT);
		boolean storeUnsynced = intent.getExtras().getBoolean(PARAM_STORE_UNSYNCED, true);
		updateOfferAux(offer, storeUnsynced);
	}

	private boolean updateOfferAux(IOfferItem offer, boolean storeUnsynced)
			throws DBException {
		if (NetworkStatusChecker.isNetworkAvailable(getApplicationContext())) {
			try {
				(new RemoteDBDataManager()).updateOffer(offer);
				CacheManager.getManager(this).updateOffer(offer);
				return true;
			} catch (DBException e) {
				if (storeUnsynced) {
					Log.w("RemoteDBService - updateOffer", "remoteDB throwed exception: " + e.getMessage() + ". Updating unsynced tasks in local database");
					CacheManager.getManager(getApplicationContext()).addUnsyncedOffer(offer);
				}
				throw e;
			}
		}
		//  network is not available - save locally for later sync
		if (storeUnsynced) {
			CacheManager.getManager(getApplicationContext()).addUnsyncedOffer(offer);
		}
		return false;
	}

	private void removeOffer(Intent intent) throws DBException
	{
		IOfferItem offer = (IOfferItem) intent.getExtras().getSerializable(PARAM_OBJECT);
		boolean storeUnsynced = intent.getExtras().getBoolean(PARAM_STORE_UNSYNCED, true);
		if (NetworkStatusChecker.isNetworkAvailable(getApplicationContext())) {
			try {
				(new RemoteDBDataManager()).removeOffer(offer);
				CacheManager.getManager(this).removeOffer(offer);
				return;
			} catch (DBException e) {
				if (storeUnsynced) {
					Log.w("RemoteDBService - removeOffer", "remoteDB throwed exception - updating unsynced tasks in local database");
					offer.setState(ItemState.REMOVED);
				}
				CacheManager.getManager(getApplicationContext()).addUnsyncedOffer(offer);
				throw e;
			}
		}
		//  network is not available - save locally for later sync
		if (storeUnsynced) {
			offer.setState(ItemState.REMOVED);
			CacheManager.getManager(getApplicationContext()).addUnsyncedOffer(offer);
		}
	}

	private IWishlistItem addWish(Intent intent) throws DBException
	{
		IWishlistItem wish = (IWishlistItem) intent.getExtras().getSerializable(PARAM_OBJECT);
		boolean storeUnsynced = intent.getExtras().getBoolean(PARAM_STORE_UNSYNCED, true);
		return addWishAux(wish, storeUnsynced);
	}

	private IWishlistItem addWishAux(IWishlistItem wish, boolean storeUnsynced)
			throws DBException {
		if (NetworkStatusChecker.isNetworkAvailable(getApplicationContext())) {
			try {
				IWishlistItem returnedWish = (new RemoteDBDataManager()).addWish(wish);
				if (null != returnedWish) {
					CacheManager.getManager(this).addWish(returnedWish);
					return returnedWish;
				}
			} catch  (DBException e) {
				if (storeUnsynced) {
					Log.w("RemoteDBService - addWish", "remoteDB throwed exception - updating unsynced tasks in local database");
					CacheManager.getManager(getApplicationContext()).addUnsyncedWish(wish);
				}
				throw e;
			}
		}
		// either network is not available or we got null from remoteDBDataManager - either way - save locally for later sync
		if (storeUnsynced) {
			CacheManager.getManager(getApplicationContext()).addUnsyncedWish(wish);
		}
		return null;
	}

	private void updateWish(Intent intent) throws DBException
	{
		IWishlistItem wish = (IWishlistItem) intent.getExtras().getSerializable(PARAM_OBJECT);
		boolean storeUnsynced = intent.getExtras().getBoolean(PARAM_STORE_UNSYNCED, true);
		updateWishAux(wish, storeUnsynced);
	}

	private boolean updateWishAux(IWishlistItem wish, boolean storeUnsynced)
			throws DBException {
		if (NetworkStatusChecker.isNetworkAvailable(getApplicationContext())) {
			try {
				(new RemoteDBDataManager()).updateWish(wish);
				CacheManager.getManager(this).updateWish(wish);
				return true;
			} catch (DBException e) {
				if (storeUnsynced) {
					Log.w("RemoteDBService - updateWish", "remoteDB throwed exception - updating unsynced tasks in local database");
					CacheManager.getManager(getApplicationContext()).addUnsyncedWish(wish);
				}
				throw e;
			}
		}
		//  network is not available - save locally for later sync
		if (storeUnsynced) {
			CacheManager.getManager(getApplicationContext()).addUnsyncedWish(wish);
		}
		return false;
	}

	private void removeWish(Intent intent) throws DBException
	{
		IWishlistItem wish = (IWishlistItem) intent.getExtras().getSerializable(PARAM_OBJECT);
		boolean storeUnsynced = intent.getExtras().getBoolean(PARAM_STORE_UNSYNCED, true);
		if (NetworkStatusChecker.isNetworkAvailable(getApplicationContext())) {
			try {
				(new RemoteDBDataManager()).removeWish(wish);
				CacheManager.getManager(this).removeWish(wish);
				return;
			} catch (DBException e) {
				if (storeUnsynced) {
					Log.w("RemoteDBService - removeWish", "remoteDB throwed exception - updating unsynced tasks in local database");
					wish.setState(ItemState.REMOVED);
					CacheManager.getManager(getApplicationContext()).addUnsyncedWish(wish);
				}
				throw e;
			}
		}
		//  network is not available - save locally for later sync
		if (storeUnsynced) {
			wish.setState(ItemState.REMOVED);
			CacheManager.getManager(getApplicationContext()).addUnsyncedWish(wish);
		}
	}

	// returns list of keys for which operation was successful
	private LinkedList<Long> updateWishesFromMap(Intent intent) {
		Map<Long, IWishlistItem> m = (Map<Long, IWishlistItem>) intent.getExtras().getSerializable(PARAM_OBJECT);
		boolean storeUnsynced = intent.getExtras().getBoolean(PARAM_STORE_UNSYNCED, true);
		LinkedList<Long> successful = new LinkedList<Long>();
		try {
			for (Long key: m.keySet()) {
				IWishlistItem item = m.get(key);
				if(updateWishAux(item, storeUnsynced)) {
					// successfully added to remote database
					successful.add(key);
				}
			}
		} catch (DBException e) {
			// nothing i want to do here for now
		}
		return successful;
	}

	// returns list of keys for which operation was successful
	private LinkedList<Long> addWishesFromMap(Intent intent) {
		Map<Long, IWishlistItem> m = (Map<Long, IWishlistItem>) intent.getExtras().getSerializable(PARAM_OBJECT);
		boolean storeUnsynced = intent.getExtras().getBoolean(PARAM_STORE_UNSYNCED, true);
		LinkedList<Long> successful = new LinkedList<Long>();
		try {
			for (Long key: m.keySet()) {
				IWishlistItem item = m.get(key);
				if(null != addWishAux(item, storeUnsynced)) {
					// successfully added to remote database
					successful.add(key);
				}
			}
		} catch (DBException e) {
			// nothing i want to do here for now
		}
		return successful;
	}

	// returns list of keys for which operation was successful
	private LinkedList<Long> updateOffersFromMap(Intent intent) {
		Map<Long, IOfferItem> m = (Map<Long, IOfferItem>) intent.getExtras().getSerializable(PARAM_OBJECT);
		boolean storeUnsynced = intent.getExtras().getBoolean(PARAM_STORE_UNSYNCED, true);
		LinkedList<Long> successful = new LinkedList<Long>();
		try {
			for (Long key: m.keySet()) {
				IOfferItem item = m.get(key);
				if(updateOfferAux(item, storeUnsynced)) {
					// successfully added to remote database
					successful.add(key);
				}
			}
		} catch (DBException e) {
			// nothing i want to do here for now
		}
		return successful;
	}

	// returns list of keys for which operation was successful
	private LinkedList<Long> addOffersFromMap(Intent intent) {
		Map<Long, IOfferItem> m = (Map<Long, IOfferItem>) intent.getExtras().getSerializable(PARAM_OBJECT);
		boolean storeUnsynced = intent.getExtras().getBoolean(PARAM_STORE_UNSYNCED, true);
		LinkedList<Long> successful = new LinkedList<Long>();
		for (Long key: m.keySet()) {
			IOfferItem item = m.get(key);
			if(addOfferAux(item, storeUnsynced)) {
				// successfully added to remote database
				successful.add(key);
			}
		}
		return successful;
	}


	@Override
	protected void onHandleIntent(Intent intent)
	{
		RemoteDBServiceOperations operation = (RemoteDBServiceOperations) intent.getExtras().getSerializable(PARAM_OPERATION);
		Serializable returnData = null;
		try {
			switch (operation)
			{
				case AddOffer:
					addOffer(intent);
					break;
				case AddWish:
					returnData = addWish(intent);
					break;
				case RemoveOffer:
					removeOffer(intent);
					break;
				case RemoveWish:
					removeWish(intent);
					break;
				case UpdateOffer:
					updateOffer(intent);
					break;
				case UpdateWish:
					updateWish(intent);
					break;
				case GetGCMMessage:
					addMessageFromGCM(intent);
					break;
				case UpdateCatalog:
					updateCatalog();
					break;
				case UpdateCourses:
					updateCourses();
					break;
				case UpdateMyOffers:
					updateMyOffers(intent);
					break;
				case UpdateMyWishes:
					updateMyWishes(intent);
					break;
				case UploadImage:
					returnData = uploadImage(intent);
					break;
				case UpdateMyMessages:
					updateMyMessages(intent);
					break;
				case SendMessage:
					sendMessage(intent);
					break;
				case AddUser:
					addUser(intent);
					break;
				case MessagesRead:
					markRead(intent);
					break;
				case UpdateRelevantOffers:
					updateRelevantOffers(intent);
					break;
				case AddOffersFromMap:
					returnData = addOffersFromMap(intent);
					break;
				case UpdateOffersFromMap:
					returnData = updateOffersFromMap(intent);
					break;
				case AddWishesFromMap:
					returnData = addWishesFromMap(intent);
					break;
				case UpdateWishesFromMap:
					returnData = updateWishesFromMap(intent);
					break;
				case UpdateUsers:
					updateUsers();
					break;
				case GetUser:
					getUser(intent);
					break;
				default:
					break;
			}
			Log.v("Service", operation.toString());
			notifyFinished(operation,returnData);
		} catch (DBException e)

		{
			notifyError(operation);
		}
	}

	private void getUser(Intent intent) throws DBException {
		String sUser = intent.getStringExtra(PARAM_OBJECT);
		ArrayList<String> usersIds = new ArrayList<String>();
		usersIds.add(sUser);
		List<IUserNotebook> lst = (new RemoteDBDataManager()).getUsers(usersIds,0);
		if (null == lst)
		{
			throw new DBException(ReturnCodeNotebook.BAD_PARAM);
		}
		CacheManager.getManager(this).addUsers(lst);
	}

	private void updateUsers() throws DBException
	{
		List<String> lstUsers = new ArrayList<String>();
		Collection<IUserNotebook> existingUsers = CacheManager.getManager(this).getAllUsers();
		for (Message msg : CacheManager.getManager(this).getMyAllMessages()) {
			lstUsers.add(msg.sender);
			lstUsers.add(msg.receiver);
		}
		Set<String> usersToGet = new HashSet<String>();
		for (String sUser : lstUsers) {
			boolean bFound = false;
			for (IUserNotebook exists : existingUsers) {
				if (sUser.equalsIgnoreCase(exists.getUserId()))
				{
					bFound=true;
					break;
				}
			}
			if (!bFound)
			{
				usersToGet.add(sUser);
			}
		}
		List<IUserNotebook> lst = (new RemoteDBDataManager()).getUsers(new ArrayList<String>(usersToGet),0);
		if (null == lst)
		{
			throw new DBException(ReturnCodeNotebook.BAD_PARAM);
		}
		CacheManager.getManager(this).addUsers(lst);
	}

	private void updateRelevantOffers(Intent intent) throws DBException {
		List<Message> lstMessages = CacheManager.getManager(getApplicationContext()).getAllMyWishsMessages();
		List<Long> lstOffersIDs = new ArrayList<Long>();
		for (Message msg : lstMessages) {
			lstOffersIDs.add(msg.offerId);
		}
		List<IOfferItem> lstOffers = (new RemoteDBDataManager()).getAllOffers(lstOffersIDs);
		if (null == lstOffers)
		{
			throw new DBException(ReturnCodeNotebook.BAD_PARAM);
		}
		CacheManager.getManager(this).setRelevntOffers(lstOffers);

	}

	private void markRead(Intent intent) {
		List<Message> readMsgs = (List<Message>) intent.getExtras().getSerializable(PARAM_OBJECT);
		List<Long> msgsIds = new ArrayList<Long>();
		for (Message message : readMsgs) {
			msgsIds.add(message.id);
		}
		(new RemoteDBDataManager()).markRead(msgsIds);
		CacheManager.getManager(this).markRead(readMsgs);
	}

	private void addUser(Intent intent) {
		IUserNotebook user = (IUserNotebook) intent.getExtras().getSerializable(PARAM_OBJECT);
		(new RemoteDBDataManager()).addUser(user);
		CacheManager.getManager(this).addUser(user);
	}

	private void sendMessage(Intent intent) throws DBException {
		Message msg = (Message) intent.getSerializableExtra(PARAM_OBJECT);
		if (!msg.fromOffer)
		{
			IOfferItem offer = CacheManager.getManager(getApplicationContext()).getReleventOffer(msg.offerId);
			if (null == offer)
			{
				List<Long> lst = new ArrayList<Long>();
				lst.add(msg.offerId);
				List<IOfferItem> allOffers = (new RemoteDBDataManager()).getAllOffers(lst);
				CacheManager.getManager(getApplicationContext()).addReleventOffers(allOffers);
			}
			IUserNotebook user = CacheManager.getManager(getApplicationContext()).getUserByID(msg.getReceiver());
			if (null == user)
			{
				List<String> lst = new ArrayList<String>();
				lst.add(msg.getReceiver());
				List<IUserNotebook> lstUsers = (new RemoteDBDataManager()).getUsers(lst, 0);
				if (null == lstUsers)
				{
					throw new DBException(ReturnCodeNotebook.BAD_PARAM);
				}
				CacheManager.getManager(getApplicationContext()).addUsers(lstUsers);
			}
		}
		Message sentMsg = (new RemoteDBDataManager()).addMessage(msg);

			try {
				sentMsg.contents = URLDecoder.decode(sentMsg.contents,"UTF-8");
			} catch (UnsupportedEncodingException e) {
				throw new DBException(ReturnCodeNotebook.BAD_PARAM);
			}

		CacheManager.getManager(this).addMessage(sentMsg);
	}


	private void updateMyMessages(Intent intent) throws DBException {
		long timestamp = CacheManager.getManager(this).getLastUpdateTimestamp(CacheTables.MESSAGES);
		List<IOfferItem> myOffers = CacheManager.getManager(this).getMyOffers();
		List<IWishlistItem> myWishes = CacheManager.getManager(this).getMyWishes();
		List<Message> lstMessages = (new RemoteDBDataManager()).getMyMessages(0l,myOffers,myWishes);
		CacheManager.getManager(this).addToMyMessages(lstMessages);
	}

	private void addMessageFromGCM(Intent intent) {
		Message msg = (Message) intent.getExtras().getSerializable(PARAM_OBJECT);
		Log.v("addMessageFromGCM", "ID: " + msg.id.toString());
		ArrayList<Message> lst = new ArrayList<Message>();
    	lst.add(msg);
		CacheManager.getManager(this).addToMyMessages(lst);
	}

	private void updateMyWishes(Intent intent) throws DBException
	{
		String userId = (String) intent.getSerializableExtra(PARAM_OBJECT);
		long timestamp = CacheManager.getManager(this).getLastUpdateTimestamp(CacheTables.WISHES, userId);
		List<IWishlistItem> $ = (new RemoteDBDataManager()).getMyWishlist(timestamp, userId);
		if ($ == null)
		{
			throw new DBException(ReturnCodeNotebook.BAD_PARAM);
		}
		CacheManager.getManager(this).addToMyWishs($);
	}

	private void updateMyOffers(Intent intent) throws DBException
	{
		String userId = (String) intent.getSerializableExtra(PARAM_OBJECT);
		long timestamp = CacheManager.getManager(this).getLastUpdateTimestamp(CacheTables.OFFERS, userId);
		List<IOfferItem> $ = (new RemoteDBDataManager()).getMyOffers(timestamp, userId);
		if ($==null)
		{
			throw new DBException(ReturnCodeNotebook.BAD_PARAM);
		}
		CacheManager.getManager(this).addToMyOffers($);
	}

	private void updateCourses()
	{
		long timestamp = CacheManager.getManager(this).getLastUpdateTimestamp(CacheTables.COURSES);
		HashMap<Integer, ICourse> $ = (new RemoteDBDataManager()).getAllCourses(timestamp);
		CacheManager.getManager(this).addItemsToCourses($);
	}

	private void updateCatalog()
	{
		long timestamp = CacheManager.getManager(this).getLastUpdateTimestamp(CacheTables.CATALOG);
		HashMap<Long, ICatalogItem> $ = (new RemoteDBDataManager()).getCatalog(timestamp);
	    CacheManager.getManager(this).addItemsToCatalog($);
	}

}
