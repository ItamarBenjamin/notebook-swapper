package gilaad.itamar.idan.notebookswapper.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

public class GoogleDataService extends IntentService {
	public static final String PARAM_OPERATION = "operation";
	public static final String PARAM_ERROR = "error";
	public static final String PARAM_OBJECT = "object";

	public enum GoogleDataServiceOperations
	{
		GetUserImage
	}

	public GoogleDataService() {
		super("GoogleDataService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		GoogleDataServiceOperations operation = (GoogleDataServiceOperations) intent.getExtras().getSerializable(PARAM_OPERATION);
		try {
			switch (operation)
			{
				case GetUserImage:
					getUserImage(intent);
					break;
				default:
					break;
			}
			Log.v("Service(google)", operation.toString());
			notifyFinished(operation);
		} catch (Error e)	{
			notifyError(operation);
		}
	}

	private void getUserImage(Intent intent) {
		String url = (String) intent.getExtras().getSerializable(PARAM_OBJECT);


//		AbstractDataManager.getManager().addWish(wish);
//		CacheManager.getManager().addWish(wish);
	}


	private void notifyFinished(GoogleDataServiceOperations operation)
	{
		Intent i = new Intent(operation.toString());
		GoogleDataService.this.sendBroadcast(i);
	}

	private void notifyError(GoogleDataServiceOperations operation)
	{
		Intent i = new Intent(operation.toString());
		i.putExtra(PARAM_ERROR, true);
		GoogleDataService.this.sendBroadcast(i);
	}
}
