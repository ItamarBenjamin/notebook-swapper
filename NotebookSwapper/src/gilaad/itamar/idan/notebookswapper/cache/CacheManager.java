package gilaad.itamar.idan.notebookswapper.cache;

import gilaad.itamar.idan.notebookswapper.accounts.Account;
import gilaad.itamar.idan.notebookswapper.accounts.AccountManager;
import gilaad.itamar.idan.notebookswapper.cache.LocalCacheSQLiteOpenHelper.CacheTables;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.NotebookType;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IUserNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;
import gilaad.itamar.idan.notebookswapper.server.notebook.api.ReturnCodeNotebook;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService;
import gilaad.itamar.idan.notebookswapper.validation.DBException;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.util.LruCache;

public class CacheManager {
	private HashMap<Long, ICatalogItem> m_CatalogMap;
	private HashMap<Integer, ICourse> m_CoursesMap;
	private ArrayList<IOfferItem> m_MyOffers = new ArrayList<IOfferItem>();
	private ArrayList<IWishlistItem> m_MyWishes;
	private Map<Long, List<Message>> m_MyWishesMessagesByWishID;
	private Map<Long, List<Message>> m_MyOffersMessagesByOfferID;
	private Map<Long, List<IOfferItem>> m_OffersByCatalog;
	private Map<String,IUserNotebook> m_UserMap = new HashMap<String, IUserNotebook>();
	private final Map<Long,IOfferItem> m_mapReleventOffers = new HashMap<Long, IOfferItem>();
	private final Context m_Context;
	static private CacheManager m_instance;
	private List<Message> m_AllMyMessages;
	private ArrayList<IOfferItem> m_lstSyncingOffers = new ArrayList<IOfferItem>();
	private ArrayList<IWishlistItem> m_lstSyncingWishes = new ArrayList<IWishlistItem>();

	private LruCache<String, Bitmap> m_lruProfileImgsCache;

	static private String WISH_OFFERS_COUNTER_MAP = "WishOffersCoutnerMap";

	static public CacheManager getManager(Context context)
	{
		if (m_instance != null) {
			return m_instance;
		}
		Log.d("CacheManager","Cache manager was killed, trying to restart it");
		createManager(context);
		return m_instance;
	}

	static public void createManager(Context context)
	{
		if (m_instance == null)
		{
			LocalCacheSQLiteOpenHelper.createHelper(context.getApplicationContext(),1);
			m_instance = new CacheManager(context);
			Account suid = AccountManager.getCurrentAccount(context.getApplicationContext());
			if (suid != null)
			{
				m_instance.readCachedUserSpecificDataFromDatabase(suid.getUID());
			}
		}
	}


	public Bitmap getProfileImage(String url)
	{
		Bitmap bitmapFromMemCache = getBitmapFromMemCache(url);
		if (null == bitmapFromMemCache)
		{
			bitmapFromMemCache = getImage(url);
		}
		addBitmapToMemoryCache(url, bitmapFromMemCache);
		return bitmapFromMemCache;
	}


	public void addProfileImage(String url,Bitmap bm) throws DBException
	{
		addBitmapToMemoryCache(url, bm);
		addImage(url, bm);
	}

	public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
		if (key == null || bitmap==null)
		{
			return;
		}
	    m_lruProfileImgsCache.put(key, bitmap);
	}


	public Bitmap getBitmapFromMemCache(String key) {
	    return m_lruProfileImgsCache.get(key);
	}


	private CacheManager(Context context)
	{
		m_Context = context.getApplicationContext();
		readCachedDataFromDatabase();
		int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		int cacheSize = maxMemory/8;

		m_lruProfileImgsCache = new LruCache<String, Bitmap>(cacheSize){
			 @Override
		        protected int sizeOf(String key, Bitmap bitmap) {
		            // The cache size will be measured in kilobytes rather than
		            // number of items.
		            return bitmap.getByteCount() / 1024;
		        }
		};
	}

	private void readCachedDataFromDatabase() {
		LocalCacheSQLiteOpenHelper localCacheSQLiteOpenHelper = LocalCacheSQLiteOpenHelper.getInstance();
		m_CatalogMap = localCacheSQLiteOpenHelper.readCatalog();
		m_CoursesMap = localCacheSQLiteOpenHelper.readCourses();
		m_UserMap = localCacheSQLiteOpenHelper.readUsers();
	}

	public void readCachedUserSpecificDataFromDatabase(String user) {
		LocalCacheSQLiteOpenHelper localCacheSQLiteOpenHelper = LocalCacheSQLiteOpenHelper.getInstance();
		m_MyOffers = localCacheSQLiteOpenHelper.readMyOffers(user);
		m_MyWishes = localCacheSQLiteOpenHelper.readMyWishes(user);
		m_AllMyMessages = localCacheSQLiteOpenHelper.readMyMessages(user);
		m_MyWishesMessagesByWishID = getMyWishesMessages();
		m_MyOffersMessagesByOfferID = getMyOffersMessages();
	}

	private Map<Long, List<Message>> getMyWishesMessages() {
		Map<Long,List<Message>> mapWishesMessages = new HashMap<Long,List<Message>>();
		for (Message message : m_AllMyMessages) {
			for (IWishlistItem wishItem : m_MyWishes) {
				Long wishId = message.wishId;
				if (wishItem.getWishID().equals(wishId))
				{
					List<Message> lst = mapWishesMessages.get(wishId);
					if (lst == null)
					{
						lst = new ArrayList<Message>();
						mapWishesMessages.put(wishId, lst);
					}
					lst.add(message);
					break;
				}
			}
		}
		return mapWishesMessages;
	}

	private Map<Long, List<Message>> getMyOffersMessages()
	{
		Map<Long,List<Message>> mapOffersMessages = new HashMap<Long,List<Message>>();
		for (Message message : m_AllMyMessages) {
			for (IOfferItem offerItem : m_MyOffers) {
				Long offerId = message.offerId;
				if (offerItem.getOfferID().equals(offerId))
				{
					List<Message> lst = mapOffersMessages.get(offerId);
					if (lst == null)
					{
						lst = new ArrayList<Message>();
						mapOffersMessages.put(offerId, lst);
					}
					lst.add(message);
					break;
				}
			}
		}
		return mapOffersMessages;
	}


	private void updateCourses() {
		Intent i = new Intent(m_Context, RemoteDBService.class);
		i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.UpdateCourses);
		m_Context.startService(i);

	}

	private void updateCatalog() {
		Intent i = new Intent(m_Context, RemoteDBService.class);
		i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.UpdateCatalog);
		m_Context.startService(i);
	}

	public void addItemsToCatalog(HashMap<Long, ICatalogItem> items)
	{
		SQLiteDatabase database = LocalCacheSQLiteOpenHelper.getInstance().getWritableDatabase();
		for (ICatalogItem $ : items.values())
		{
			if ($.getState() == ItemState.REMOVED)
			{
				database.delete(CacheTables.CATALOG.tblName(), "ItemID = " + $.getCatalogItemID().toString(), null);
				m_CatalogMap.remove($.getCatalogItemID());
				continue;
			}
			ContentValues values = new ContentValues();
			values.put("ItemState", $.getState().getStatus());
			values.put("ItemID", Long.toString($.getCatalogItemID()));
			values.put("Edition", $.getEdition());
			values.put("Type", $.getType().toString());
			values.put("CourseNumber", Integer.toString($.getCourseNumber()));
			values.put("Barcode", $.getBarcode());
			values.put("Timestamp", $.getTimestamp().toString());
			database.insert(CacheTables.CATALOG.tblName(), null, values);
			m_CatalogMap.put($.getCatalogItemID(), $);
		}

	}

	public void addItemsToCourses(HashMap<Integer, ICourse> items) {
		SQLiteDatabase database = LocalCacheSQLiteOpenHelper.getInstance().getWritableDatabase();
		for (ICourse $ : items.values())
		{
			if ($.getState() == ItemState.REMOVED)
			{
				database.delete(CacheTables.COURSES.tblName(), "CourseNumber = " + $.getCourseNumber(), null);
				m_CoursesMap.remove($.getCourseNumber());
				continue;
			}
			ContentValues values = new ContentValues();
			values.put("CourseNumber", Integer.toString($.getCourseNumber()));
			values.put("CourseName", $.getCourseName());
			values.put("Faculty", $.getFaculty());
			values.put("ItemState", $.getState().getStatus());
			values.put("Timestamp", $.getTimestamp().toString());
			database.insert(CacheTables.COURSES.tblName(), null, values);
			m_CoursesMap.put($.getCourseNumber(), $);
		}

	}

	public void addUnsyncedOffer(IOfferItem offer) {
		synchronized (m_lstSyncingOffers) {
			m_lstSyncingOffers.remove(offer);
		}
		ContentValues cv = getOfferContentValues(offer);
		// fix the local offer timestamp
		cv.put("Timestamp", System.currentTimeMillis());
		LocalCacheSQLiteOpenHelper.getInstance().updateUnsynchedOffer(cv);
	}

	public ArrayList<IOfferItem> takeOutUnsyncedOffers() {
		synchronized (m_lstSyncingOffers) {
			m_lstSyncingOffers  = LocalCacheSQLiteOpenHelper.getInstance().readAndClearMyUnsyncedOffers();
			return new ArrayList<IOfferItem>(m_lstSyncingOffers);
		}
	}

	public List<IOfferItem> getUnsyncedOffers()
	{
		return LocalCacheSQLiteOpenHelper.getInstance().readMyUnsyncedOffers();
	}

	public List<IWishlistItem> getUnsyncedWishes()
	{
		return LocalCacheSQLiteOpenHelper.getInstance().readMyUnsyncedWishes();
	}

	public ArrayList<IWishlistItem> takeOutUnsyncedWishes() {
		synchronized (m_lstSyncingWishes) {
			m_lstSyncingWishes = LocalCacheSQLiteOpenHelper.getInstance().readAndClearMyUnsyncedWishes();
			return new ArrayList<IWishlistItem>(m_lstSyncingWishes);
		}
	}

	public boolean unsyncedItemsExists() {
		return LocalCacheSQLiteOpenHelper.getInstance().unsyncedTablesNotEmpty();
	}

	public void addUnsyncedWish(IWishlistItem wish) {
		synchronized (m_lstSyncingWishes) {
			m_lstSyncingWishes.remove(wish);
		}
		ContentValues cv = getWishContentValues(wish);
		// fix the wish timestamp
		cv.put("Timestamp", System.currentTimeMillis());
		LocalCacheSQLiteOpenHelper.getInstance().updateUnsynchedWishes(cv);
	}

	public void addToMyOffers(List<IOfferItem> offers)
	{
		SQLiteDatabase database = LocalCacheSQLiteOpenHelper.getInstance().getWritableDatabase();
		for (IOfferItem $ : offers)
		{
			if ($.getState() == ItemState.REMOVED)
			{
				database.delete(CacheTables.OFFERS.tblName(), "OfferID = " + $.getOfferID().toString(), null);
				synchronized (m_MyOffers)
				{
					m_MyOffers.remove($); //uses offerid for comparison
				}
				continue;
			}
			ContentValues values = getOfferContentValues($);
			synchronized (m_MyOffers)
			{
				if (m_MyOffers.contains($))
				{
					m_MyOffers.remove($);
					m_MyOffers.add($);
					database.update(CacheTables.OFFERS.tblName(),values, "OfferID = " + $.getOfferID().toString(), null);
				} else
				{
					database.insert(CacheTables.OFFERS.tblName(), null, values);
					m_MyOffers.add($);
				}
			}
		}

	}

	public ArrayList<IOfferItem> getMyOffers()
	{
		synchronized (m_MyOffers)
		{
			return new ArrayList<IOfferItem>(m_MyOffers);
		}
	}

	public List<IOfferItem> getSyncingOrUnsyncedOffers()
	{
		synchronized (m_lstSyncingOffers) {
			List<IOfferItem> unsyncedOffers = getUnsyncedOffers();
			List<IOfferItem> $ = new ArrayList<IOfferItem>(unsyncedOffers);
			for (IOfferItem offer : m_lstSyncingOffers) {
				boolean bExist = false;
				for (IOfferItem existsing : $) {
					if (existsing == offer)
					{
						bExist = true;
					}
				}
				if (!bExist)
				{
					$.add(offer);
				}
			}
			return $;
		}
	}

	public List<IWishlistItem> getSyncingOrUnsyncedWishs()
	{
		synchronized (m_lstSyncingWishes) {
			List<IWishlistItem> unsyncedWishes = getUnsyncedWishes();
			List<IWishlistItem> $ = new ArrayList<IWishlistItem>(unsyncedWishes);
			for (IWishlistItem wish : m_lstSyncingWishes) {
				boolean bExist = false;
				for (IWishlistItem existsing : $) {
					if (existsing == wish)
					{
						bExist = true;
					}
				}
				if (!bExist)
				{
					$.add(wish);
				}
			}
			return $;
		}
	}

	private ContentValues getOfferContentValues(IOfferItem offer) {
		ContentValues values = new ContentValues();
		values.put("OfferID", offer.getOfferID());
		values.put("UserID", offer.getUserID());
		values.put("CatalogID", offer.getCatalogItemID());
		values.put("Condition", offer.getCondition().toString());
		values.put("IsExchange", offer.isExchangable() ? 1 : 0);
		values.put("Price", offer.getPrice());
		values.put("AdditionalInfo", offer.getAdditionalInfo());
		values.put("ImageURI", offer.getImageUrl());
		values.put("ItemState", offer.getState().getStatus());
		values.put("Timestamp", offer.getTimestamp().toString());
		return values;
	}

	public void addOffer(IOfferItem offer)
	{
		synchronized (m_MyOffers)
		{
			if (m_MyOffers.contains(offer))
			{
				Log.d("FUCKYOU","Tried to add exiting offer");
				return;
			}
			m_MyOffers.add(offer);
		}
		synchronized (m_lstSyncingOffers) {
			m_lstSyncingOffers.remove(offer);
		}

		SQLiteDatabase database = LocalCacheSQLiteOpenHelper.getInstance().getWritableDatabase();
		ContentValues values = getOfferContentValues(offer);
		database.insert(CacheTables.OFFERS.tblName(), null, values);

	}


	public void removeOffer(IOfferItem offer)
	{
		synchronized (m_MyOffers)
		{
			for (IOfferItem $ : m_MyOffers)
			{
				if ($.getOfferID().compareTo(offer.getOfferID()) == 0)
				{
					m_MyOffers.remove($);
					break;
				}
			}
		}

		SQLiteDatabase database = LocalCacheSQLiteOpenHelper.getInstance().getWritableDatabase();
		database.delete(CacheTables.OFFERS.tblName(),"OfferID = " + offer.getOfferID().toString() ,null);

	}

	public void updateOffer(IOfferItem offer) {
		synchronized (m_MyOffers)
		{
			for (IOfferItem $ : m_MyOffers)
			{
				if ($.getOfferID().compareTo(offer.getOfferID()) == 0)
				{
					m_MyOffers.remove($);
					break;
				}
			}
			m_MyOffers.add(offer);
		}

		SQLiteDatabase database = LocalCacheSQLiteOpenHelper.getInstance().getWritableDatabase();
		ContentValues values = getOfferContentValues(offer);
		database.update(CacheTables.OFFERS.tblName(),values, "OfferID = " + offer.getOfferID().toString() ,null);

	}

	public void addToMyWishs(List<IWishlistItem> wishes)
	{
		SQLiteDatabase database = LocalCacheSQLiteOpenHelper.getInstance().getWritableDatabase();
		for (IWishlistItem $ : wishes)
		{
			if ($.getState() == ItemState.REMOVED)
			{
				database.delete(CacheTables.WISHES.tblName(), "WishID = " + $.getWishID().toString(), null);
				synchronized (m_MyWishes)
				{
					m_MyWishes.remove($); //uses wishid for comparison
				}
				continue;
			}
			ContentValues values = getWishContentValues($);
			synchronized (m_MyWishes)
			{
				if (m_MyWishes.contains($))
				{
					m_MyWishes.remove($);
					m_MyWishes.add($);
					database.update(CacheTables.WISHES.tblName(),values, "WishID = " + $.getWishID().toString(), null);
				} else
				{
					database.insert(CacheTables.WISHES.tblName(), null, values);
					m_MyWishes.add($);
				}
			}
		}

	}

	public void addWish(IWishlistItem wish) {
		synchronized (m_MyWishes)
		{
			if (m_MyWishes.contains(wish))
			{
				Log.d("FUCKYOU","Tried to add exiting wish");
				return;
			}
			m_MyWishes.add(wish);
		}
		synchronized (m_lstSyncingWishes) {
			m_lstSyncingWishes.remove(wish);
		}

		SQLiteDatabase database = LocalCacheSQLiteOpenHelper.getInstance().getWritableDatabase();
		ContentValues values = getWishContentValues(wish);
		database.insert(CacheTables.WISHES.tblName(), null, values);

	}

	private ContentValues getWishContentValues(IWishlistItem wish) {
		ContentValues values = new ContentValues();
		values.put("WishID", wish.getWishID());
		values.put("UserID", wish.getUserID());
		values.put("CourseNumber", wish.getCourseNumber());
		values.put("Type", wish.getType().toString());
		values.put("IsExchange", wish.isExchangable() ? 1 : 0);
		values.put("WantToBuy", wish.isExchangable() ? 1 : 0);
		values.put("ItemState", wish.getState().getStatus());
		values.put("Timestamp", wish.getTimestamp().toString());
		return values;
	}

	public void updateWish(IWishlistItem wish) {
		synchronized (m_MyWishes)
		{
			for (IWishlistItem $ : m_MyWishes)
			{
				if ($.equals(wish))
				{
					m_MyWishes.remove($);
					break;
				}
			}
			m_MyWishes.add(wish);
		}

		SQLiteDatabase database = LocalCacheSQLiteOpenHelper.getInstance().getWritableDatabase();
		ContentValues values = getWishContentValues(wish);
		database.update(CacheTables.WISHES.tblName(),values, "WishID = " + wish.getWishID().toString() ,null);

	}

	public void removeWish(IWishlistItem wish) {
		synchronized (m_MyWishes)
		{
			for (IWishlistItem $ : m_MyWishes)
			{
				if ($.getWishID().compareTo(wish.getWishID()) == 0)
				{
					m_MyWishes.remove($);
					break;
				}
			}
		}

		SQLiteDatabase database = LocalCacheSQLiteOpenHelper.getInstance().getWritableDatabase();
		database.delete(CacheTables.WISHES.tblName(),"WishID = " + wish.getWishID().toString() ,null);

	}

	public boolean isCourseExists(int id)
	{
		return m_CoursesMap.get(id) != null;
	}

	public ICatalogItem getCatalogItem(Long id)
	{
		return m_CatalogMap.get(id);
	}

	public ICourse getCourse(int id)
	{
		return m_CoursesMap.get(id);
	}

	public List<ICourse> searchCoursesByNameOrNumber(String text)
	{
		List<ICourse> ret = new ArrayList<ICourse>();
		Iterator<Entry<Integer, ICourse>> it = m_CoursesMap.entrySet().iterator();
	    while (it.hasNext()) {
			Map.Entry<Integer,ICourse> pairs = it.next();
	        ICourse current = pairs.getValue();
	        if (pairs.getKey().toString().startsWith(text))
	        {
	        	ret.add(current);
	        	continue;
	  		}
	        if (pairs.getValue().toString().startsWith(text))
       		{
	        	ret.add(current);
       		}
	    }
	    return ret;
	}

	public List<ICatalogItem> getCatalogItemsForCourse(ICourse course)
	{
		List<ICatalogItem> ret = new ArrayList<ICatalogItem>();
		Iterator<Entry<Long, ICatalogItem>> it = m_CatalogMap.entrySet().iterator();
	    while (it.hasNext()) {
			Map.Entry<Long, ICatalogItem> pairs = it.next();
			ICatalogItem current = pairs.getValue();
	        if (current.getCourseNumber()==course.getCourseNumber())
	        {
	        	ret.add(current);
	  		}
	    }
	    return ret;
	}

	public ICatalogItem getCatalogItemForCourseAndType(int course_num, NotebookType type)
	{
		ICourse course = getCourse(course_num);
		if (course == null) {
			return null;
		}
		List<ICatalogItem> items = getCatalogItemsForCourse(course);
		for (ICatalogItem $ : items) {
			if ($.getType() == type) {
				return $;
			}
		}
		return null;
	}

	public void update() {
		updateCourses();
		updateCatalog();
	}

	public List<ICourse> getAllCourses() {
		ICourse arr[] = new ICourse[m_CoursesMap.size()];
		return new ArrayList<ICourse>(Arrays.asList(m_CoursesMap.values().toArray(arr)));
	}

	public ArrayList<IWishlistItem> getMyWishes() {
		synchronized (m_MyWishes)
		{
			return new ArrayList<IWishlistItem>(m_MyWishes);
		}
	}

	public long getLastUpdateTimestamp(CacheTables table) {
		SQLiteDatabase database = LocalCacheSQLiteOpenHelper.getInstance().getReadableDatabase();
		Cursor cursor = database.rawQuery("SELECT MAX(Timestamp) as Value FROM " + table.tblName(), null);
		if (!cursor.moveToFirst())
		{
			return 0;
		}
		long l = cursor.getLong(cursor.getColumnIndex("Value"));

		return l;
	}

	public long getLastUpdateTimestamp(CacheTables table, String user) {
		SQLiteDatabase database = LocalCacheSQLiteOpenHelper.getInstance().getReadableDatabase();
		Cursor cursor = database.rawQuery("SELECT MAX(Timestamp) as Value FROM " + table.tblName() + " WHERE UserID = '"+user+"'", null);
		if (!cursor.moveToFirst())
		{
			return 0;
		}
		long l = cursor.getLong(cursor.getColumnIndex("Value"));

		return l;
	}

	public void addImage(String url, Bitmap bm) throws DBException
	{
		if (bm == null)
		{
			throw new DBException(ReturnCodeNotebook.BAD_PARAM);
		}
		SQLiteDatabase database = LocalCacheSQLiteOpenHelper.getInstance().getWritableDatabase();
		Log.d("ADDIMAGE","ADDIMAGE");
		ContentValues values = getImageContentValues(url, bm);
		database.insertWithOnConflict(CacheTables.IMAGES.tblName(), null, values,SQLiteDatabase.CONFLICT_REPLACE);
	}

	private ContentValues getImageContentValues(String url, Bitmap bm) {
		ContentValues values = new ContentValues();
		values.put("URL", url);
		String filename = "/sdcard/"+System.currentTimeMillis()+".png";
		OutputStream stream;
		try {
			stream = new FileOutputStream(filename);
		} catch (FileNotFoundException e) {
			return null;
		}
	    /* Write bitmap to file using JPEG and 80% quality hint for JPEG. */
	    bm.compress(CompressFormat.PNG, 70, stream);
//		ByteArrayOutputStream stream = new ByteArrayOutputStream();
//		bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
//		byte[] byteArray = stream.toByteArray();
		values.put("Data", filename);
		return values;
	}

	public Bitmap getImage(String url)
	{
		SQLiteDatabase database = LocalCacheSQLiteOpenHelper.getInstance().getReadableDatabase();
		Cursor cursor = database.rawQuery("SELECT * FROM  " + CacheTables.IMAGES.tblName() + " WHERE URL = '" + url + "'", null);
		Bitmap bitmap = null;
		cursor.moveToFirst();
		if (!cursor.isAfterLast())
		{
			String filename = cursor.getString(cursor.getColumnIndex("Data"));
			cursor.close();
			bitmap = BitmapFactory.decodeFile(filename);
	    }
		return bitmap;
	}

	public void addToMyMessages(List<Message> lstMessages) {
		List<Message> newMessages = new ArrayList<Message>();
		synchronized (m_AllMyMessages) {
			for (Message message : lstMessages) {
				boolean m_bFound = false;
				for (Message existing : m_AllMyMessages) {
					if (existing.id.equals(message.id))
					{
						m_bFound = true;
						break;
					}
				}
				if (!m_bFound)
				{
					newMessages.add(message);
				}
			}

			m_AllMyMessages.addAll(newMessages);
		}
		synchronized (m_MyOffersMessagesByOfferID)
		{
			for (Message message : newMessages) {
				Long offerId = message.offerId;
				List<Message> tmp = m_MyOffersMessagesByOfferID.get(offerId);
				if (null==tmp)
				{
					tmp = new ArrayList<Message>();
					m_MyOffersMessagesByOfferID.put(offerId, tmp);
				}
				tmp.add(message);

			}
		}
		synchronized (m_MyWishesMessagesByWishID)
		{
			for (Message message : newMessages) {
				Long wishId = message.wishId;
				List<Message> tmp = m_MyWishesMessagesByWishID.get(wishId);
				if (null==tmp)
				{
					tmp = new ArrayList<Message>();
					m_MyWishesMessagesByWishID.put(wishId, tmp);
				}
				tmp.add(message);
			}
		}

		SQLiteDatabase database = LocalCacheSQLiteOpenHelper.getInstance().getWritableDatabase();
		for (Message msg : newMessages)
		{
			ContentValues values = getMessageContentValues(msg);
			database.insert(CacheTables.MESSAGES.tblName(), null, values);
		}

	}

	private ContentValues getMessageContentValues(Message msg) {
		ContentValues values = new ContentValues();
		values.put("MessageID", msg.id);
		values.put("OfferID", msg.offerId);
		values.put("WishID", msg.wishId);
		values.put("Contents", msg.contents);
		values.put("Sender", msg.sender);
		values.put("Reciever", msg.receiver);
		values.put("FromOffer", msg.fromOffer ? 1 : 0);
		values.put("IsRead", msg.isRead ? 1 : 0);
		values.put("Timestamp", msg.timestamp.toString());
		return values;
	}

	public List<Message> getMyWishMessages(Long m_myWishID)
	{
		List<Message> list = m_MyWishesMessagesByWishID.get(m_myWishID);
		return list==null ? new ArrayList<Message>() : list;
	}

	public List<Message> getMyOffersMessages(Long m_offerID) {

//		if (m_MyOffersMessagesByOfferID == null)
//		{
//			String uid = AccountManager.getCurrentAccount(m_Context).getUID();
//			m_AllMyMessages = LocalCacheSQLiteOpenHelper.getInstance().readMyMessages(uid);
//			m_MyOffersMessagesByOfferID = getMyOffersMessages();
//		}
		List<Message> list = m_MyOffersMessagesByOfferID.get(m_offerID);
		return list==null ? new ArrayList<Message>() : list;
	}

	public void replaceOffersByCatalog(Long catid, List<IOfferItem> ret)
	{
		synchronized (m_OffersByCatalog)
		{
			m_OffersByCatalog.put(catid, ret);
		}
	}

	public List<IOfferItem> getOffersByCatalog(Long catid)
	{
		synchronized (m_OffersByCatalog)
		{
			return m_OffersByCatalog.get(catid);
		}
	}

	public void addMessage(Message msg) {
		for (Message existingMsg : m_AllMyMessages) {
			if (existingMsg.id.equals(msg.id))
			{
				Log.v("CacheManager", "Message with id " + msg.id + " was already found in the cache, won't add the new one");
				return;
			}
		}

		if (IsMyOffer(msg.offerId))
		{
			synchronized (m_MyOffersMessagesByOfferID)
			{
				Long offerId = msg.offerId;
				List<Message> tmp = m_MyOffersMessagesByOfferID.get(offerId);
				if (null==tmp)
				{
					tmp = new ArrayList<Message>();
					m_MyOffersMessagesByOfferID.put(offerId, tmp);
				}
				tmp.add(msg);
			}
		}
		if (IsMyWish(msg.wishId))
		{
			synchronized (m_MyWishesMessagesByWishID)
			{
				Long wishId = msg.wishId;
				List<Message> tmp = m_MyWishesMessagesByWishID.get(wishId);
				if (null==tmp)
				{
					tmp = new ArrayList<Message>();
					m_MyWishesMessagesByWishID.put(wishId, tmp);
				}
				tmp.add(msg);
			}
		}
		m_AllMyMessages.add(msg);
		SQLiteDatabase database = LocalCacheSQLiteOpenHelper.getInstance().getWritableDatabase();
		ContentValues values = getMessageContentValues(msg);
		database.insert(CacheTables.MESSAGES.tblName(), null, values);

	}

	private boolean IsMyWish(Long wishId) {
		for (IWishlistItem wishItem : m_MyWishes) {
			if (wishItem.getWishID().equals(wishId))
			{
				return true;
			}
		}
		return false;
	}

	private boolean IsMyOffer(Long offerId) {
		for (IOfferItem iOfferItem : m_MyOffers) {
			if (iOfferItem.getOfferID().equals(offerId))
			{
				return true;
			}
		}
		return false;
	}

	public void addUser(IUserNotebook user) {
		String key = user.getUserId();
		if (m_UserMap.get(key) != null)
		{
			return;
		}
		m_UserMap.put(key, user);
		SQLiteDatabase database = LocalCacheSQLiteOpenHelper.getInstance().getWritableDatabase();
		ContentValues values = getUserContentValues(user);
		database.insert(CacheTables.USERS.tblName(), null, values);

	}

	private ContentValues getUserContentValues(IUserNotebook user) {
		ContentValues values = new ContentValues();
		values.put("UserID", user.getUserId());
		values.put("Email", user.getEmail());
		values.put("ImageURL", user.getImgUrl());
		ItemState state = user.getState();
		if (state == null)
		{
			state = ItemState.REGULAR;
		}
		values.put("ItemState", state.getStatus());
		values.put("Timestamp", user.getTimestamp());
		values.put("Gender", user.getGender());
		values.put("UserName", user.getUsername());
		return values;
	}

	public void markRead(List<Message> readMsgs) {
		SQLiteDatabase database = LocalCacheSQLiteOpenHelper.getInstance().getWritableDatabase();
		for (Message message : readMsgs) {
			for (Message otherMsg : m_AllMyMessages) {
				if (otherMsg.id.equals(message.id))
				{
					otherMsg.isRead = true;
					break;
				}
			}
			message.isRead = true;
			ContentValues values = getMessageContentValues(message);
			database.update(CacheTables.MESSAGES.tblName(),values, "MessageID = " + message.id.toString() ,null);
		}

	}

	public List<Message> getAllMyWishsMessages() {
		List<Message> allMyWishMsgs = new ArrayList<Message>();
		for (List<Message> lstMessages : m_MyWishesMessagesByWishID.values()) {
			allMyWishMsgs.addAll(lstMessages);
		}
		return allMyWishMsgs;
	}

	public void setRelevntOffers(List<IOfferItem> lstOffers) {
		for (IOfferItem iOfferItem : lstOffers) {
			m_mapReleventOffers.put(iOfferItem.getOfferID(), iOfferItem);
		}
	}

	public IOfferItem getReleventOffer(Long offerId) {
		return m_mapReleventOffers.get(offerId);
	}

	public void addReleventOffers(List<IOfferItem> allOffers) {
		for (IOfferItem iOfferItem : allOffers) {
			m_mapReleventOffers.put(iOfferItem.getOfferID(), iOfferItem);
		}


	}

	public IWishlistItem getMyWish(Long wishId) {
		for (IWishlistItem wishItem : m_MyWishes) {
			if (wishItem.getWishID().equals(wishId))
			{
				return wishItem;
			}
		}
		return null;
	}

	public IUserNotebook getUserByID(String sUserID) {
		return m_UserMap.get(sUserID);
	}

	public List<Message> getMyAllMessages() {
		return m_AllMyMessages;
	}

	public Collection<IUserNotebook> getAllUsers() {
		return m_UserMap.values();
	}

	public void addUsers(List<IUserNotebook> lst) {
		for (IUserNotebook iUserNotebook : lst) {
			addUser(iUserNotebook);
		}
	}

	public void resetOffersCounter(Long wishID)
	{
		SharedPreferences sharedPreferences = m_Context.getSharedPreferences(WISH_OFFERS_COUNTER_MAP, 0);
		Editor editor = sharedPreferences.edit();
		editor.putInt(Long.toString(wishID), 0);
		editor.commit();
	}

	public void addedOffer(Long wishID) {

		SharedPreferences sharedPreferences = m_Context.getSharedPreferences(WISH_OFFERS_COUNTER_MAP, 0);
		String sWishID = Long.toString(wishID);
		int iNewOffers = sharedPreferences.getInt(sWishID, 0)+1;
		Editor editor = sharedPreferences.edit();
		editor.putInt(sWishID, iNewOffers);
		editor.commit();
	}

	public int getOffersCount(Long wishID) {
		if (wishID == null) {
			return 0;
		}
		SharedPreferences sharedPreferences = m_Context.getSharedPreferences(WISH_OFFERS_COUNTER_MAP, 0);
		String sWishID = Long.toString(wishID);
		return sharedPreferences.getInt(sWishID, 0);
	}
}
