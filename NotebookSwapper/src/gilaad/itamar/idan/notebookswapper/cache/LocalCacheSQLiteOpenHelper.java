package gilaad.itamar.idan.notebookswapper.cache;

import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.NotebookCondition;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.NotebookType;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IUserNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.CatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.CourseNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.OfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.UserNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.WishlistItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class LocalCacheSQLiteOpenHelper extends SQLiteOpenHelper
{
	// Use this id when you want to insert a wish or offer and you don't know what ID it will get yet
	public static long DUMMY_ID = -1;

	public enum CacheTables {
		CATALOG("Catalog"),COURSES("Courses"),OFFERS("Offers"),WISHES("Wishes"),USERS("Users"),IMAGES("Images"),MESSAGES("Messages"),UNSYNCED_OFFERS("UnsyncedOffers"),UNSYNCED_WISHES("UnsyncedWishes");
		String m_name;

		private CacheTables(String name)
		{
			m_name=name;
		}

		public String tblName() {return m_name; }
	}

	static private LocalCacheSQLiteOpenHelper m_instance;

	static public void createHelper(Context context, int version)
	{
		if (m_instance == null)
		{
			m_instance = new LocalCacheSQLiteOpenHelper(context,null,version);
		}
	}

	static public LocalCacheSQLiteOpenHelper getInstance()
	{
		if (m_instance != null) {
			return m_instance;
		}
		throw new RuntimeException("LocalCacheSQLiteOpenHelper wasn't created");
	}

	private LocalCacheSQLiteOpenHelper(Context context, CursorFactory factory, int version) {
		super(context, "LocalCache", factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + CacheTables.OFFERS.tblName() + " (OfferID INTEGER PRIMARY KEY , UserID TEXT, CatalogID INTEGER, Condition TEXT, IsExchange INTEGER, Price INTEGER, ItemState INTEGER, AdditionalInfo TEXT, Timestamp INTEGER, ImageURI TEXT);");
		db.execSQL("CREATE TABLE " + CacheTables.COURSES.tblName() + " (CourseNumber INTEGER PRIMARY KEY , CourseName TEXT, Faculty TEXT,ItemState INTEGER, Timestamp INTEGER);");
		db.execSQL("CREATE TABLE " + CacheTables.CATALOG.tblName() + " (ItemID INTEGER PRIMARY KEY , Edition TEXT, Type TEXT, CourseNumber INTEGER, Barcode BLOB,ItemState INTEGER, Timestamp INTEGER);");
		db.execSQL("CREATE TABLE " + CacheTables.WISHES.tblName() + " (WishID INTEGER PRIMARY KEY , UserID TEXT, CourseNumber INTEGER, IsExchange INTEGER, WantToBuy INTEGER, ItemState INTEGER, Type TEXT, Timestamp INTEGER);");
		db.execSQL("CREATE TABLE " + CacheTables.USERS.tblName() + " (UserID TEXT PRIMARY KEY ,Email TEXT, ImageURL TEXT, ItemState INTEGER, Timestamp INTEGER,Gender INTEGER,UserName TEXT);");
		db.execSQL("CREATE TABLE " + CacheTables.IMAGES.tblName() + " (URL TEXT PRIMARY KEY , Data TEXT);");
		db.execSQL("CREATE TABLE " + CacheTables.MESSAGES.tblName() + " (MessageID INTEGER PRIMARY KEY , OfferID INTEGER, WishID INTEGER, Contents TEXT, FromOffer INTEGER, IsRead INTEGER, Sender TEXT, Reciever TEXT, Timestamp INTEGER);");

		// Creating tables for storing unsynced operations
		db.execSQL("CREATE TABLE " + CacheTables.UNSYNCED_OFFERS.tblName() + " (LocalOfferID INTEGER PRIMARY KEY , OfferID INTEGER , UserID TEXT, CatalogID INTEGER, Condition TEXT, IsExchange INTEGER, Price INTEGER, ItemState INTEGER, AdditionalInfo TEXT, Timestamp INTEGER, ImageURI TEXT);");
		db.execSQL("CREATE TABLE " + CacheTables.UNSYNCED_WISHES.tblName() + " (LocalWishID INTEGER PRIMARY KEY , WishID INTEGER , UserID TEXT, CourseNumber INTEGER, IsExchange INTEGER, WantToBuy INTEGER, ItemState INTEGER, Type TEXT, Timestamp INTEGER);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(LocalCacheSQLiteOpenHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion + ". Old data will be destroyed");
		for (CacheTables table : CacheTables.values())
		{
			db.execSQL("DROP TABLE IF EXISTS " + table.tblName());
		}
        onCreate(db);
	}

	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(LocalCacheSQLiteOpenHelper.class.getName(), "Downgrading database from version " + oldVersion + " to " + newVersion + ". Old data will be destroyed");
		for (CacheTables table : CacheTables.values())
		{
			db.execSQL("DROP TABLE IF EXISTS " + table.tblName());
		}
		onCreate(db);
	}

	/**
	 * inserts new or updates existing unsynched offer
	 */
	public void updateUnsynchedOffer(ContentValues cv) {
		updateUnsyncedTable(cv, CacheTables.UNSYNCED_OFFERS.tblName(), "OfferID", "LocalOfferID");
	}

	/**
	 * inserts new or updates existing unsynched wish
	 */
	public void updateUnsynchedWishes(ContentValues cv) {
		updateUnsyncedTable(cv, CacheTables.UNSYNCED_WISHES.tblName(), "WishID", "LocalWishID");
	}

	private void updateUnsyncedTable(ContentValues cv, String tableName, String remoteIdColumnName, String localIdColumnName) {
		SQLiteDatabase db = getWritableDatabase();
		Long OfferId = cv.getAsLong(remoteIdColumnName);
		if (null != OfferId) {
			// the offer is already in the remote DB
			Cursor cursor = db.query(tableName, new String [] {localIdColumnName}, remoteIdColumnName + " = " + OfferId.toString(), null, null, null, null);
			if (cursor.moveToFirst()) {
				// the offer is already in the unsynced Offers as well - we need its local id
				Long localOfferId = cursor.getLong(0);
				cv.put(localIdColumnName, localOfferId);
				db.replaceOrThrow(tableName, null, cv);

				return;
			}
		} else {
			cv.put(remoteIdColumnName, -1L);
		}
		// the offer is not in the unsynced table yet - we need a new local id
		Long nextLocalId = getNextLocalId(db, tableName, localIdColumnName);
		cv.put(localIdColumnName, nextLocalId);
		db.insert(tableName, null, cv);

	}

	private long getNextLocalId(SQLiteDatabase db, String tableName, String localIdColumnName) {
		Cursor cursor = db.query(tableName, new String [] {"MAX(" + localIdColumnName + ")"}, null, null, null, null, null);
		long maxId = -1;
		if (cursor.moveToFirst()) {
			maxId = cursor.getLong(0);
		}

		return maxId+1;
	}

	public ArrayList<IOfferItem> readAndClearMyUnsyncedOffers()	{
		SQLiteDatabase db = getWritableDatabase();
		Cursor cursor = db.query(CacheTables.UNSYNCED_OFFERS.tblName(),null,null,null,null,null,null);
		ArrayList<IOfferItem> ret = readOffers(cursor);

		db.delete(CacheTables.UNSYNCED_OFFERS.tblName(), null, null);

		return ret;
	}

	public ArrayList<IOfferItem> readMyOffers(String user)
	{
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.query(CacheTables.OFFERS.tblName(),null,"UserID = '" +user+"'",null,null,null,null);
		ArrayList<IOfferItem> ret = readOffers(cursor);

		return ret;
	}

	private ArrayList<IOfferItem> readOffers(Cursor cursor) {
		ArrayList<IOfferItem> list = new ArrayList<IOfferItem>();
		cursor.moveToFirst();
		int idx_offerID = cursor.getColumnIndex("OfferID");
		int idx_userID = cursor.getColumnIndex("UserID");
		int idx_catalogID = cursor.getColumnIndex("CatalogID");
		int idx_condition = cursor.getColumnIndex("Condition");
		int idx_isExchange = cursor.getColumnIndex("IsExchange");
		int idx_price = cursor.getColumnIndex("Price");
		int idx_additionalInfo = cursor.getColumnIndex("AdditionalInfo");
		int idx_timestamp = cursor.getColumnIndex("Timestamp");
		int idx_imageURI = cursor.getColumnIndexOrThrow("ImageURI");
		while (!cursor.isAfterLast())
		{
			ItemState state = ItemState.fromInt(cursor.getInt(cursor.getColumnIndex("ItemState")));
			if (!state.equals(ItemState.REMOVED))
			{
				Long id = cursor.getLong(idx_offerID);
				id = id.equals(-1L) ? null : id;
				String uid = cursor.getString(idx_userID);
				Long catid = cursor.getLong(idx_catalogID);
				NotebookCondition condition = NotebookCondition.fromString(cursor.getString(idx_condition));
				boolean exch = cursor.getInt(idx_isExchange) == 0 ? false : true;
				int price = cursor.getInt(idx_price);
				String info = cursor.getString(idx_additionalInfo);
				long timestamp = cursor.getLong(idx_timestamp);
				OfferItem $ = new OfferItem(id,uid, catid, exch, state, price, condition, info, timestamp);
				$.setImageUrl(cursor.getString(idx_imageURI));
				list.add($);
			}
			cursor.moveToNext();
	    }

		return list;
	}


	public boolean unsyncedTablesNotEmpty() {
		SQLiteDatabase db = getReadableDatabase();
		boolean ret = db.query(CacheTables.UNSYNCED_OFFERS.tblName(),null,null,null,null,null,null).moveToFirst() ||
				db.query(CacheTables.UNSYNCED_WISHES.tblName(),null,null,null,null,null,null).moveToFirst();

		return ret;
	}

	public ArrayList<IWishlistItem> readAndClearMyUnsyncedWishes()
	{
		SQLiteDatabase db = getWritableDatabase();
		Cursor cursor = db.query(CacheTables.UNSYNCED_WISHES.tblName(),null,null,null,null,null,null);
		ArrayList<IWishlistItem> ret = readWishes(cursor);
		db.delete(CacheTables.UNSYNCED_WISHES.tblName(), null, null);

		return ret;
	}

	public ArrayList<IWishlistItem> readMyWishes(String user)
	{
		Cursor cursor = getReadableDatabase().query(CacheTables.WISHES.tblName(),null,"UserID = '" +user+"'",null,null,null,null);
		return readWishes(cursor);
	}

	private ArrayList<IWishlistItem> readWishes(Cursor cursor) {
		ArrayList<IWishlistItem> list = new ArrayList<IWishlistItem>();
		cursor.moveToFirst();
		int idx_wishID = cursor.getColumnIndex("WishID");
		int idx_userID = cursor.getColumnIndex("UserID");
		int idx_courseNumber = cursor.getColumnIndex("CourseNumber");
		int idx_type = cursor.getColumnIndex("Type");
		int idx_isExchange = cursor.getColumnIndex("IsExchange");
		int idx_wantToBuy = cursor.getColumnIndex("WantToBuy");
		int idx_timeStamp = cursor.getColumnIndex("Timestamp");
		while (!cursor.isAfterLast())
		{
			ItemState state = ItemState.fromInt(cursor.getInt(cursor.getColumnIndex("ItemState")));
			if (!state.equals(ItemState.REMOVED))
			{
				Long id = cursor.getLong(idx_wishID);
				id = id.equals(-1L) ? null : id;
				String uid = cursor.getString(idx_userID);
				int coursenum = cursor.getInt(idx_courseNumber);
				NotebookType type = NotebookType.fromString(cursor.getString(idx_type));
				boolean exch = cursor.getInt(idx_isExchange) == 0 ? false : true;
				boolean isbuy = cursor.getInt(idx_wantToBuy) == 0 ? false : true;
				long timestamp = cursor.getLong(idx_timeStamp);
				WishlistItem $ = new WishlistItem(id,uid, coursenum, exch, state, isbuy, type, timestamp);
				list.add($);
			}
			cursor.moveToNext();
	    }

		return list;
	}

	public HashMap<Long,ICatalogItem> readCatalog()
	{
		HashMap<Long,ICatalogItem> map = new HashMap<Long,ICatalogItem>();
		Cursor cursor = getReadableDatabase().query(CacheTables.CATALOG.tblName(),null,null,null,null,null,null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast())
		{
			ItemState state = ItemState.fromInt(cursor.getInt(cursor.getColumnIndex("ItemState")));
			if (!state.equals(ItemState.REMOVED))
			{
				long id = cursor.getLong(cursor.getColumnIndex("ItemID"));
				String edition = cursor.getString(cursor.getColumnIndex("Edition"));
				NotebookType type = NotebookType.fromString(cursor.getString(cursor.getColumnIndex("Type")));
				int coursenum = cursor.getInt(cursor.getColumnIndex("CourseNumber"));
				String barcode = cursor.getString(cursor.getColumnIndex("Barcode"));
				long timestamp = cursor.getLong(cursor.getColumnIndex("Timestamp"));
				ICatalogItem $ = new CatalogItem(id,coursenum,edition, type, state, barcode, timestamp);
				map.put(id,$);
			}
			cursor.moveToNext();
	    }

		return map;
	}

	public HashMap<Integer,ICourse> readCourses()
	{
		HashMap<Integer,ICourse> map = new HashMap<Integer,ICourse>();
		Cursor cursor = getReadableDatabase().query(CacheTables.COURSES.tblName(),null,null,null,null,null,null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast())
		{
			ItemState state = ItemState.fromInt(cursor.getInt(cursor.getColumnIndex("ItemState")));
			if (!state.equals(ItemState.REMOVED))
			{
				int coursenum = cursor.getInt(cursor.getColumnIndex("CourseNumber"));
				String name = cursor.getString(cursor.getColumnIndex("CourseName"));
				String fac = cursor.getString(cursor.getColumnIndex("Faculty"));
				long timestamp = cursor.getLong(cursor.getColumnIndex("Timestamp"));
				ICourse $ = new CourseNotebook(coursenum,name,fac, state, timestamp);
				map.put(coursenum,$);
			}
			cursor.moveToNext();
	    }

		return map;
	}

	public HashMap<String,Bitmap> readImages()
	{
		HashMap<String,Bitmap> map = new HashMap<String,Bitmap>();
		Cursor cursor = getReadableDatabase().query(CacheTables.IMAGES.tblName(),null,null,null,null,null,null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast())
		{
			String url = cursor.getString(cursor.getColumnIndex("URL"));
			String data = cursor.getString(cursor.getColumnIndex("Data"));
			byte[] bytes = data.getBytes();
			Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
			map.put(url,bitmap);
			cursor.moveToNext();
	    }

		return map;
	}

	public List<Message> readMyMessages(String user)
	{
		List<Message> $ = new ArrayList<Message>();
		Cursor cursor = getReadableDatabase().query(CacheTables.MESSAGES.tblName(),null,"Sender = '" +user+"' OR Reciever = '"+user+"'",null,null,null,null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast())
		{
			long id = cursor.getLong(cursor.getColumnIndex("MessageID"));
			long offerId = cursor.getLong(cursor.getColumnIndex("OfferID"));
			long wishId = cursor.getLong(cursor.getColumnIndex("WishID"));
			String contents = cursor.getString(cursor.getColumnIndex("Contents"));
			String sender = cursor.getString(cursor.getColumnIndex("Sender"));
			String reciever = cursor.getString(cursor.getColumnIndex("Reciever"));
			boolean fromOffer = cursor.getInt(cursor.getColumnIndex("FromOffer")) == 0 ? false : true;
			boolean isRead = cursor.getInt(cursor.getColumnIndex("IsRead")) == 0 ? false : true;
			long timestamp = cursor.getLong(cursor.getColumnIndex("Timestamp"));
			Message msg = new Message(id, offerId, wishId, contents, timestamp,sender, reciever, fromOffer, isRead);
			$.add(msg);
			cursor.moveToNext();
		}

		return $;
	}

	public Map<String, IUserNotebook> readUsers() {
		Map<String,IUserNotebook> $ = new HashMap<String, IUserNotebook>();
		Cursor cursor = getReadableDatabase().query(CacheTables.USERS.tblName(),null,null,null,null,null,null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast())
		{
			String uid = cursor.getString(cursor.getColumnIndex("UserID"));
			String sImageUrl = cursor.getString(cursor.getColumnIndex("ImageURL"));
			int itemstate = cursor.getInt(cursor.getColumnIndex("ItemState"));
			String email = cursor.getString(cursor.getColumnIndex("Email"));
			String username = cursor.getString(cursor.getColumnIndex("UserName"));
			int gender = cursor.getInt(cursor.getColumnIndex("Gender"));
			long timestamp = cursor.getLong(cursor.getColumnIndex("Timestamp"));
			UserNotebook user = new UserNotebook(uid, username, email, gender, sImageUrl, timestamp,itemstate);
			$.put(uid,user);
			cursor.moveToNext();
		}

		return $;
	}

	public List<IOfferItem> readMyUnsyncedOffers() {
		SQLiteDatabase db = getWritableDatabase();
		Cursor cursor = db.query(CacheTables.UNSYNCED_OFFERS.tblName(),null,null,null,null,null,null);
		ArrayList<IOfferItem> ret = readOffers(cursor);
		cursor.close();
		return ret;
	}

	public List<IWishlistItem> readMyUnsyncedWishes() {
		SQLiteDatabase db = getWritableDatabase();
		Cursor cursor = db.query(CacheTables.UNSYNCED_WISHES.tblName(),null,null,null,null,null,null);
		ArrayList<IWishlistItem> ret = readWishes(cursor);
		cursor.close();
		return ret;
	}
}
