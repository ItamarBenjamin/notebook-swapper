package gilaad.itamar.idan.notebookswapper.offer;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.accounts.AccountManager;
import gilaad.itamar.idan.notebookswapper.adapters.FullMessagesListAdapter;
import gilaad.itamar.idan.notebookswapper.animation.DefaultAnimation;
import gilaad.itamar.idan.notebookswapper.asynctasks.DownloadImageWithRunnable;
import gilaad.itamar.idan.notebookswapper.asynctasks.IGetFromURLS;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService.RemoteDBServiceOperations;
import gilaad.itamar.idan.notebookswapper.validation.DBException;

import java.util.ArrayList;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nhaarman.listviewanimations.swinginadapters.AnimationAdapter;

public class OfferFullMessagesActivity extends Activity {
	public static final String WISHID = "WISHID";
	public static final String OFFERID = "OFFERID";
	public static final String WISHER = "WISHER";
	private static final String MY_IMAGE = "MYIMAGE";
	private static final String OTHER_IMAGE = "OTHERIMAGE";
	private String m_Wisher;

	private FullMessagesListAdapter m_oAdapter;
	private AnimationAdapter m_AnimAdap;

	private BroadcastReceiver m_OfferMessegesReceiver;
	private BroadcastReceiver m_OfferMessegesSendReceiver;

	private ListView m_lstMessages;
	private Button m_btnSendMsg;
	private EditText m_editText;
	private TextView m_txtCourseTitle;
	private TextView m_txtOfferInfo;

	private Bitmap m_BitmapMe = null;
	private Bitmap m_BitmapOther = null;

	private String sReciever,sSender;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        final Long wishId = getIntent().getLongExtra(WISHID, -1);
        final Long OfferId = getIntent().getLongExtra(OFFERID, -1);

        sSender = AccountManager.getCurrentAccount(getApplicationContext()).getUID();
        m_Wisher = getIntent().getStringExtra(WISHER);
        sReciever = m_Wisher;

        setOfferInfo(OfferId);

        findControllers();
        m_btnSendMsg.setEnabled(false);

        if (null != savedInstanceState)
        {
        	m_BitmapMe = savedInstanceState.getParcelable(MY_IMAGE);
        	m_BitmapOther = savedInstanceState.getParcelable(OTHER_IMAGE);
        }

        m_oAdapter = new FullMessagesListAdapter(this,wishId,OfferId,false,m_BitmapMe,m_BitmapOther);
        m_AnimAdap = DefaultAnimation.getDefaultListViewAnimationAdapter(m_oAdapter, m_lstMessages);
        if (m_BitmapOther == null)
        {
        	getOtherImage();
        }
        if (null == m_BitmapMe)
        {
        	getMyImage();
        }


		m_lstMessages.setAdapter(m_AnimAdap);
		m_AnimAdap.notifyDataSetChanged();
		addListeners(wishId, OfferId);

	}

	private void addListeners(final Long wishId, final Long OfferId) {
		m_editText.addTextChangedListener(new TextWatcher(){
	        @Override
			public void afterTextChanged(Editable s)
	        {
	        	m_btnSendMsg.setEnabled(s.length() != 0);
	        }

	        @Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after){}
	        @Override
			public void onTextChanged(CharSequence s, int start, int before, int count){}
	    });
		m_btnSendMsg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				m_editText.setEnabled(false);
				m_btnSendMsg.setEnabled(false);
				String text = m_editText.getText().toString();
				Message msg = new Message(OfferId, wishId, text, sSender, sReciever, true);
				Intent i = new Intent(OfferFullMessagesActivity.this, RemoteDBService.class);
				i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.SendMessage);
				i.putExtra(RemoteDBService.PARAM_OBJECT, msg);
				startService(i);
			}
		});
	}

	private void getOtherImage() {
    	CacheManager manager = CacheManager.getManager(getApplicationContext());
    	final String sOtherURL = manager.getUserByID(sReciever).getImgUrl();
    	m_BitmapOther = CacheManager.getManager(getApplicationContext()).getProfileImage(sOtherURL);
    	if (m_BitmapMe != null)
    	{
    		m_oAdapter.setOtherImg(m_BitmapOther);
    		m_AnimAdap.notifyDataSetChanged();
    		return;
    	}

		String[] urls = {sOtherURL};
		new DownloadImageWithRunnable(new IGetFromURLS() {

			@Override
			public void onGetURLS(Bitmap bitmap) {
				if (bitmap !=null)
				{
					m_BitmapOther = bitmap;
					m_oAdapter.setOtherImg(bitmap);
					m_AnimAdap.notifyDataSetChanged();
					try {
						CacheManager.getManager(getApplicationContext()).addProfileImage(sOtherURL, bitmap);
					} catch (DBException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		},this).execute(urls);
	}

	private void getMyImage() {

    	CacheManager manager = CacheManager.getManager(getApplicationContext());
    	final String sMyURL = manager.getUserByID(sSender).getImgUrl();
    	m_BitmapMe = CacheManager.getManager(getApplicationContext()).getProfileImage(sMyURL);
    	if (m_BitmapMe != null)
    	{
    		m_oAdapter.setMyImg(m_BitmapMe);
    		m_AnimAdap.notifyDataSetChanged();
    		return;
    	}

		String[] urls = {sMyURL};
		new DownloadImageWithRunnable(new IGetFromURLS() {

			@Override
			public void onGetURLS(Bitmap bitmap) {
				if (null!=bitmap)
				{
					m_BitmapMe = bitmap;
					m_oAdapter.setMyImg(bitmap);
					m_AnimAdap.notifyDataSetChanged();
					try {
						CacheManager.getManager(getApplicationContext()).addProfileImage(sMyURL, bitmap);
					} catch (DBException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		},this).execute(urls);
	}

	private void findControllers() {
		m_txtCourseTitle = (TextView)findViewById(R.id.txtMessageCourseTitle);
        m_txtOfferInfo = (TextView)findViewById(R.id.txtMessageCourseOfferInfo);
        m_lstMessages = (ListView)findViewById(R.id.lstAllMessagesFull);
        m_btnSendMsg = (Button)findViewById(R.id.btButtonSendMessage);
        m_editText = (EditText)findViewById(R.id.etEdittext);
	}

	private void setOfferInfo(Long offerId) {
		m_txtCourseTitle = (TextView)findViewById(R.id.txtMessageCourseTitle);
		ArrayList<IOfferItem> myOffers = CacheManager.getManager(getApplicationContext()).getMyOffers();
		IOfferItem offer = null;
		for (IOfferItem iOfferItem : myOffers) {
			if (iOfferItem.getOfferID().equals(offerId))
			{
				offer = iOfferItem;
			}
		}
		Long catalogItemID = offer.getCatalogItemID();
		ICatalogItem catalogItem = CacheManager.getManager(getApplicationContext()).getCatalogItem(catalogItemID);
		ICourse course = CacheManager.getManager(getApplicationContext()).getCourse(catalogItem.getCourseNumber());
		String sTitle = course.getCourseNumber() + " " +  course.getCourseName() + " - " + catalogItem.getType().toName();
		m_txtCourseTitle.setText(sTitle);

		m_txtOfferInfo = (TextView)findViewById(R.id.txtMessageCourseOfferInfo);
		String sOfferInfo = "";
		if (offer.isFree())
		{
			sOfferInfo = getString(R.string.Giveaway);
		}
		else
		{
			sOfferInfo = getString(R.string.Sale) + Integer.toString(offer.getPrice()) + getString(R.string.NewShekel) ;
		}
		if (offer.isExchangable())
		{
			if (!sOfferInfo.isEmpty())
			{
				sOfferInfo = "/" + sOfferInfo;
			}
			sOfferInfo = getString(R.string.Exchange) + sOfferInfo;
		}
		m_txtOfferInfo.setText(sOfferInfo);
	}

	@Override
	protected void onResume() {
		m_OfferMessegesReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.v("OfferFullMessageList", "Got offer messages update");
				m_AnimAdap.notifyDataSetChanged();
				m_lstMessages.post(new Runnable() {
			        @Override
			        public void run() {
			        	m_lstMessages.setSelection(m_lstMessages.getCount());
			        }
			    });
			}
		};
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(RemoteDBServiceOperations.UpdateMyMessages.toString());
		intentFilter.addAction(RemoteDBServiceOperations.GetGCMMessage.toString());
		registerReceiver(m_OfferMessegesReceiver, intentFilter);
		m_OfferMessegesSendReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.v("OfferFullMessagesActivity", "Offer message was sent");
				Boolean bError = intent.getBooleanExtra(RemoteDBService.PARAM_ERROR,false);
				if (bError)
				{
					handleFail("Failed to send message");
				}
				else
				{
					m_AnimAdap.notifyDataSetChanged();
					m_editText.setText("");
					m_lstMessages.post(new Runnable() {
				        @Override
				        public void run() {
				        	m_lstMessages.setSelection(m_lstMessages.getCount());
				        }
				    });
				}
				m_editText.setEnabled(true);
				m_btnSendMsg.setEnabled(false);
			}
		};
		intentFilter = new IntentFilter();
		intentFilter.addAction(RemoteDBServiceOperations.SendMessage.toString());
		registerReceiver(m_OfferMessegesSendReceiver, intentFilter);
		super.onResume();
	}

	private void handleFail(String sError) {
		Toast.makeText(this, sError, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onPause() {
		if (m_OfferMessegesReceiver != null) {
			unregisterReceiver(m_OfferMessegesReceiver);
		}
		if (m_OfferMessegesSendReceiver != null) {
			unregisterReceiver(m_OfferMessegesSendReceiver);
		}
		m_OfferMessegesReceiver = null;
		m_OfferMessegesSendReceiver = null;
		m_oAdapter = null;
		m_lstMessages = null;
		super.onPause();
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id)
        {
        	case android.R.id.home:
        		NavUtils.navigateUpFromSameTask(this);
        		return true;
        }
		return super.onOptionsItemSelected(item);
    }

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		outState.putParcelable(MY_IMAGE, m_BitmapMe);
		outState.putParcelable(OTHER_IMAGE, m_BitmapOther);
	}

}
