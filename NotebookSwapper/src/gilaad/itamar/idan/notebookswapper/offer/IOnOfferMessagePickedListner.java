package gilaad.itamar.idan.notebookswapper.offer;

import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;

public interface IOnOfferMessagePickedListner {
	void OnMessagePicked(Message msg);
}
