package gilaad.itamar.idan.notebookswapper.offer;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.adapters.OfferMessagesListAdapter;
import gilaad.itamar.idan.notebookswapper.asynctasks.DownloadImageTask;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.common.MenuItemSelected;
import gilaad.itamar.idan.notebookswapper.custom_view.InformativeRowCustomView;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;
import gilaad.itamar.idan.notebookswapper.edit.EditOfferActivity;
import gilaad.itamar.idan.notebookswapper.image.ImageActivity;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService.RemoteDBServiceOperations;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class OfferActivity extends Activity implements IOnOfferMessagePickedListner {
	public static final int EDIT_REQUEST_CODE = 0;
	public static final int EDIT_RESULT_CODE = 88;
	public static final String EXTRA_OFFERITEM = "offerItem";
	public static final String EXTRA_EDITEDITEM = "editedItem";

	private OfferMessagesListAdapter m_oAdapter;
	private BroadcastReceiver m_OfferMessegesReceiver;
	private IOfferItem m_Offer;

	private InformativeRowCustomView m_irPrice;
	private InformativeRowCustomView m_irExch;
	private InformativeRowCustomView m_irNotebookName;
	private InformativeRowCustomView m_irEdition;
	private InformativeRowCustomView m_irCondition;
	private InformativeRowCustomView m_irExtraInfo;
	private ListView m_lstMessages;
	private TextView m_ivImageTitle;
	private TextView m_converTitle;
	private ImageView m_ivImage;
	private RelativeLayout m_rvImage;
	private View m_Header;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers_main);

        m_lstMessages = (ListView)findViewById(R.id.lstGeneric);

        m_Offer = (IOfferItem) getIntent().getSerializableExtra(EXTRA_OFFERITEM);
        m_oAdapter = new OfferMessagesListAdapter(this, m_Offer.getOfferID());
        m_lstMessages.setAdapter(m_oAdapter);
        m_oAdapter.notifyDataSetChanged();

        m_Header = getLayoutInflater().inflate(R.layout.activity_offers, null);
        m_lstMessages.addHeaderView(m_Header);
        findControllers();
	}


    private void findControllers() {

		m_irNotebookName = (InformativeRowCustomView) m_Header.findViewById(R.id.irName);
		m_irEdition = (InformativeRowCustomView) m_Header.findViewById(R.id.irEdition);
		m_irCondition = (InformativeRowCustomView) m_Header.findViewById(R.id.irCondition);
		m_irPrice = (InformativeRowCustomView)m_Header.findViewById(R.id.irPrice);
		m_irExch = (InformativeRowCustomView)m_Header.findViewById(R.id.irForExchange);
		m_irExtraInfo = ((InformativeRowCustomView) m_Header.findViewById(R.id.irExtraInfo));
		m_ivImageTitle = ((TextView) m_Header.findViewById(R.id.txtImageTitle));
		m_ivImage = ((ImageView) m_Header.findViewById(R.id.ivImage));
		m_rvImage = ((RelativeLayout) m_Header.findViewById(R.id.rvImage));
		m_converTitle = ((TextView) m_Header.findViewById(R.id.txtConversationTitle));
	}


	private void setNotebookInfo() {
    	ICatalogItem cat = CacheManager.getManager(this).getCatalogItem(m_Offer.getCatalogItemID());
		String courseName = Integer.toString(cat.getCourseNumber()) + " - " + CacheManager.getManager(this).getCourse(cat.getCourseNumber()).getCourseName();
		m_irNotebookName.setContent(courseName + " - " + cat.getType().toName());
		m_irEdition.setContent(cat.getEdition());
		boolean free = m_Offer.isFree();
		if (!free)
		{
			String sPrice = Integer.toString(m_Offer.getPrice());
			m_irPrice.setContent(sPrice);
		}
		else
		{
			m_irPrice.setContent("חינם");
		}
		m_irExch.setContent(m_Offer.isExchangable() ? "כן" : "לא");
		m_irCondition.setContent(m_Offer.getCondition().toString());
		m_irExtraInfo.setContent(m_Offer.getAdditionalInfo());
		String imageURL = m_Offer.getImageUrl();
		if (imageURL == null || imageURL.isEmpty())
		{
			m_rvImage.setVisibility(View.GONE);
		} else
		{
			m_rvImage.setVisibility(View.VISIBLE);
			final Bitmap image = CacheManager.getManager(getApplicationContext()).getImage(imageURL);
			if (image != null)
			{
				m_ivImage.setImageBitmap(image);
				m_ivImage.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent = new Intent(OfferActivity.this,ImageActivity.class);
						intent.putExtra(ImageActivity.EXTRA_IMAGE, image);
						startActivity(intent);
					}
				});
			} else
			{
				new DownloadImageTask(m_ivImage, this, true).execute(new String[] {imageURL});
			}
		}
		if (m_oAdapter.isEmpty())
		{
			m_converTitle.setVisibility(View.GONE);
		}
	}

	@Override
	protected void onResume() {
        m_OfferMessegesReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent)
			{
				Log.v("WishMessageList", "Got wish messages update");
				m_oAdapter.notifyDataSetChanged();
				if (m_oAdapter.isEmpty())
				{
					m_converTitle.setVisibility(View.GONE);
				} else
				{
					m_converTitle.setVisibility(View.VISIBLE);
				}
			}
		};
		m_oAdapter.notifyDataSetChanged();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(RemoteDBServiceOperations.UpdateMyMessages.toString());
		intentFilter.addAction(RemoteDBServiceOperations.GetGCMMessage.toString());
		registerReceiver(m_OfferMessegesReceiver, intentFilter);
        setNotebookInfo();
		super.onResume();
	}

    @Override
    protected void onPause() {
		if (null != m_OfferMessegesReceiver) {
			unregisterReceiver(m_OfferMessegesReceiver);
		}
		super.onPause();
    }


	@Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.offer_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId())
        {
        	case android.R.id.home:
        		NavUtils.navigateUpFromSameTask(this);
        		break;
        	case R.id.action_edit:
        		Intent intent = new Intent(this,EditOfferActivity.class);
        		intent.putExtra(OfferActivity.EXTRA_OFFERITEM, m_Offer);
        		startActivityForResult(intent, EDIT_REQUEST_CODE);
        		return true;
        	case R.id.action_delete:
        		//TODO: ask user if he is sure
        		Intent i = new Intent(this, RemoteDBService.class);
        		i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.RemoveOffer);
        		i.putExtra(RemoteDBService.PARAM_OBJECT, m_Offer);
        		startService(i);
        		finish();
        		return true;
        	case R.id.action_logout:
        		if (MenuItemSelected.commonOnOptionsItemSelected(this, item))
        		{
        			return true;
        		}

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == EDIT_REQUEST_CODE && resultCode == EDIT_RESULT_CODE)
         {
        	 if (data != null)
        	 {
        		 m_Offer = (IOfferItem) data.getSerializableExtra(EXTRA_EDITEDITEM);
        		 setNotebookInfo();
        	 }
         }
 }


	@Override
	public void OnMessagePicked(Message msg) {
		Intent intent = new Intent(this,OfferFullMessagesActivity.class);
		intent.putExtra(OfferFullMessagesActivity.OFFERID, msg.offerId);
		intent.putExtra(OfferFullMessagesActivity.WISHID, msg.wishId);
		String sWisher = msg.fromOffer ? msg.receiver : msg.sender;
		intent.putExtra(OfferFullMessagesActivity.WISHER, sWisher);
		startActivity(intent);
	}
}
