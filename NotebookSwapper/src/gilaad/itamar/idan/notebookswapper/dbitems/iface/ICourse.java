package gilaad.itamar.idan.notebookswapper.dbitems.iface;

import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;

import java.io.Serializable;

public interface ICourse extends Serializable{

  int getCourseNumber();

  String getCourseName();

  String getFaculty();

  Long getCourseId();

  void setTimestamp(long timeInMillis);

  ItemState getState();

  void setState(ItemState newState);

  Long getTimestamp();
}
