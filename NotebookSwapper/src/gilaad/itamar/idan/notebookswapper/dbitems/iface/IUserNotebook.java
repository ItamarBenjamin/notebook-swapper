package gilaad.itamar.idan.notebookswapper.dbitems.iface;

import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;

import java.io.Serializable;

public interface IUserNotebook extends Serializable{

  String getUserId();

  String getUsername();

  String getEmail();

  void setTimestamp(long timeInMillis);

  ItemState getState();

  void setState(ItemState newState);

	Integer getGender();

	String getImgUrl();

	long getTimestamp();
}
