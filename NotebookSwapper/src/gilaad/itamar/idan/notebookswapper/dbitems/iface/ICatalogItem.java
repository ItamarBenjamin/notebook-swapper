package gilaad.itamar.idan.notebookswapper.dbitems.iface;

import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.NotebookType;

import java.io.Serializable;

public interface ICatalogItem extends Serializable{

  Long getCatalogItemID();

  int getCourseNumber();

  String getEdition();

  String getBarcode();

  NotebookType getType();

  void setTimestamp(long timeInMillis);

  ItemState getState();

  void setState(ItemState newState);

  Long getTimestamp();
}
