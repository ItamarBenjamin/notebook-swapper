package gilaad.itamar.idan.notebookswapper.dbitems.iface;

import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.NotebookType;

import java.io.Serializable;

public interface IWishlistItem extends Serializable{

  int getCourseNumber();

  String getUserID();

  void setExchangable(boolean b);

  void setForPurchase(boolean b);

  void setType(NotebookType type);

  void setCourseNumber(int number);

  NotebookType getType();

  Long getWishID();

  boolean isExchangable();

  boolean isForPurchase();

  void setTimestamp(long timeInMillis);

  ItemState getState();

  void setState(ItemState newState);

  Long getTimestamp();
}
