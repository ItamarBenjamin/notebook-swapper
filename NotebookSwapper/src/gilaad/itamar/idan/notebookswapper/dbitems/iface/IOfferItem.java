package gilaad.itamar.idan.notebookswapper.dbitems.iface;

import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.NotebookCondition;

import java.io.Serializable;

public interface IOfferItem extends Serializable{

  Long getOfferID();

  String getUserID();

  Long getCatalogItemID();

  boolean isExchangable();

  boolean isFree();

  int getPrice();

  String getAdditionalInfo();

  void setCatalogItemID(long id);

  void setExchangable(boolean b);

  void setPrice(int price);

  void setAdditionalInfo(String info);

  NotebookCondition getCondition();

  void setCondition(NotebookCondition cond);

  Long getTimestamp();

  void setTimestamp(Long time);

  ItemState getState();

  void setState(ItemState newState);

  String getImageUrl();

  void setImageUrl(String url);
}
