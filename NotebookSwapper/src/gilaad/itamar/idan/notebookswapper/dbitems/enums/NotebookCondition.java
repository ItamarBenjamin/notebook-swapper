package gilaad.itamar.idan.notebookswapper.dbitems.enums;

public enum NotebookCondition {
	LIKE_NEW("כחדשה",0),
	USED("משומשת",1),
	TORN("קרועה",2),
	UNKNOWN("לא צוין",3);

	private NotebookCondition(final String text, final int num) {
       this.text = text;
       this.num = num;
    }

    private final String text;
    private final int num;

    @Override
    public String toString() {
        return text;
    }

    public static NotebookCondition fromString(String text) {
        if (text != null) {
          for (NotebookCondition b : NotebookCondition.values()) {
            if (text.equalsIgnoreCase(b.text)) {
              return b;
            }
          }
        }
        return NotebookCondition.UNKNOWN;
    }

    public int toNum()
    {
    	return num;
    }

    public static NotebookCondition fromInt(Integer number)
    {
        if (number != null)
        {
        	switch (number)
        	{
        		case 0:
        			return LIKE_NEW;
        		case 1:
        			return USED;
        		case 2:
        			return TORN;
        		default:
        			return UNKNOWN;
        	}
        }
        return UNKNOWN;
    }
}
