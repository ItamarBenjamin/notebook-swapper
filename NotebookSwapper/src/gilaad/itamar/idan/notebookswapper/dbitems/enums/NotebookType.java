package gilaad.itamar.idan.notebookswapper.dbitems.enums;

public enum NotebookType {
	LECTURES("הרצאות"),
	TUTORIALS("תרגולים"),
	EXAMS("מבחנים"),
	OTHER("Other");

	private NotebookType(final String text) {
       this.text = text;
    }

    private final String text;

    public String toName() {
        return text;
    }

    public static NotebookType fromString(String text) {
        if (text != null) {
        	if (text.equalsIgnoreCase("LECTURES")) {
				return LECTURES;
			}
        	if (text.equalsIgnoreCase("TUTORIALS")) {
				return TUTORIALS;
			}
        	if (text.equalsIgnoreCase("EXAMS")) {
				return EXAMS;
			}
        }
        return NotebookType.OTHER;
      }
}
