package gilaad.itamar.idan.notebookswapper.dbitems.enums;

import android.util.Log;

public enum ItemState {
	REGULAR(0),		// for most items
	REMOVED(1),		// if the other side totally removed this item
	INACTIVE(2),	// Offer\Wish only. If the other side marked this as inactive
	EXCHANGED(3);	// Offer only. If notebook was physically exchanged.

	private ItemState(int state) {
		m_iState = state;
	}

	private final int m_iState;

	@Override
    public String toString()
	{
        return Integer.toString(m_iState);
    }

	public int getStatus() {
		return m_iState;
	}

    public static ItemState fromInt(int state) {
    	for (ItemState b : ItemState.values()) {
    		if (state == b.m_iState) {
    			return b;
    		}
    	}
    	// The following shouldn't happen... If it did, check the calling function
    	Log.e("From ItemStatus", "fromInt was called with invalid status - status "+ Integer.toString(state));
        return ItemState.REGULAR;
      }

	public static ItemState fromString(String string) {
		// TODO Auto-generated method stub
		return null;
	}
}
