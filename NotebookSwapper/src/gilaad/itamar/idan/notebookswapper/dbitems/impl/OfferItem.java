package gilaad.itamar.idan.notebookswapper.dbitems.impl;

import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.NotebookCondition;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;

public class OfferItem implements IOfferItem {
  /**
	 *
	 */
	private static final long serialVersionUID = 4391825465254785261L;
private Long m_ID;
  private String m_UID; // user id from google plus
  private Long m_CatItemID; // catalog id
  private boolean m_bExchangable;
  private int m_iPrice;
  private String m_sInfo;
  private NotebookCondition m_Condition;
  private long m_timestamp; // convert date to milliseconds
  private ItemState m_state;
  private String m_imageUrl;

  OfferItem() {

  }

  public OfferItem(Long id, String uid, Long catid, boolean exch, ItemState state,
      int price, NotebookCondition cond, String info, long timestamp) {
    m_ID = id;
    m_UID = uid;
    m_CatItemID = catid;
    m_bExchangable = exch;
    m_iPrice = price;
    m_sInfo = info;
    m_Condition = cond;
    m_timestamp = timestamp;
    m_state = state;
    m_imageUrl = null;
  }

  public OfferItem(String uid, Long catid, boolean exch, ItemState state,
      int price, NotebookCondition cond, String info) {
    this(null, uid, catid, exch, state, price, cond, info, 0l);
  }

  @Override
	public boolean equals(Object o)
  	{
		if (null == o || !(o instanceof OfferItem)) {
			return false;
		}
		if (((OfferItem)o).getOfferID() == null)
		{
			if (!((OfferItem)o).m_CatItemID.equals(m_CatItemID))
			{
				return false;
			}
			if (!((OfferItem)o).m_bExchangable == m_bExchangable)
			{
				return false;
			}
			if (!(((OfferItem)o).m_iPrice == m_iPrice))
			{
				return false;
			}
			if (!((OfferItem)o).m_sInfo.equals(m_sInfo))
			{
				return false;
			}
			if (!((OfferItem)o).m_Condition.equals(m_Condition))
			{
				return false;
			}
			return true;
		}
		return ((OfferItem) o).getOfferID().equals(getOfferID());
	}

  @Override
  public Long getOfferID() {
    return m_ID;
  }

  @Override
  public String getUserID() {
    return m_UID;
  }

  @Override
  public Long getCatalogItemID() {
    return m_CatItemID;
  }

  @Override
  public boolean isExchangable() {
    return m_bExchangable;
  }

  @Override
  public boolean isFree() {
    return m_iPrice == 0;
  }

  @Override
  public int getPrice() {
    return m_iPrice;
  }

  @Override
  public String getAdditionalInfo() {
    return m_sInfo;
  }

  @Override
  public void setCatalogItemID(long id) {
    m_CatItemID = id;

  }

  @Override
  public void setExchangable(boolean b) {
    m_bExchangable = b;

  }

  @Override
  public void setPrice(int price) {
    m_iPrice = price;

  }

  @Override
  public void setAdditionalInfo(String info) {
    m_sInfo = info;
  }

  @Override
  public NotebookCondition getCondition() {
    return m_Condition;
  }

  @Override
  public void setCondition(NotebookCondition cond) {
    m_Condition = cond;
  }

  @Override
  public Long getTimestamp() {
    return m_timestamp;
  }

  @Override
  public void setTimestamp(Long time) {
    m_timestamp = time;
  }

  @Override
  public ItemState getState() {
    return m_state;
  }

  @Override
  public void setState(ItemState newState) {
    m_state = newState;
  }

  @Override
  public String getImageUrl() {
    return m_imageUrl;
  }

  @Override
  public void setImageUrl(String url) {
    m_imageUrl = url;
  }
}
