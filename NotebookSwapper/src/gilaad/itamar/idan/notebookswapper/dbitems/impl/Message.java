package gilaad.itamar.idan.notebookswapper.dbitems.impl;

import java.io.Serializable;



public class Message implements Serializable{
  /**
	 *
	 */
	private static final long serialVersionUID = 3986726564927122529L;

	public Long id;
	public Long offerId;
	public Long wishId;
	public String contents;
	public Long timestamp;
	public String sender;
	public String receiver;
	public boolean fromOffer;
	public boolean isRead;

	public Message()
	{}

	public Message(Long id,Long offerId,Long wishId,String contents,Long timestamp,String sender, String receiver,boolean fromOffer,boolean isRead)
	{
		this.id = id;
		this.offerId = offerId;
		this.wishId = wishId;
		this.contents = contents;
		this.sender = sender;
	    this.receiver = receiver;
		this.timestamp = timestamp;
		this.fromOffer = fromOffer;
		this.isRead = isRead;
	}

	public Message(Long offerID, long wishID, String contents, String sender, String receiver, boolean bFromOffer) {
		this(null,offerID,wishID,contents,0L,sender,receiver,bFromOffer,false);
	}
	
	  public String getSender() {
		    return sender;
		  }

		  public String getReceiver() {
		    return receiver;
		  }

		  public void setTimestamp(long timeInMillis) {
		    timestamp = timeInMillis;
		  }

		  public void read() {
		    isRead = true;
		  }

		  public Long getTimestamp() {
		    return timestamp;
		  }
	
}
