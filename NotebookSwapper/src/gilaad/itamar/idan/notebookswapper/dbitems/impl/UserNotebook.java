package gilaad.itamar.idan.notebookswapper.dbitems.impl;

import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IUserNotebook;

public class UserNotebook implements IUserNotebook {
  /**
	 *
	 */
	private static final long serialVersionUID = 1977751502164507439L;
	Long Id; // auto generated
	String userId; // user id from google plus - unique
	String username;
	String email;
	Integer gender;
	String image_url;
	private long m_timestamp; // convert date to milliseconds
	private ItemState m_state;

  UserNotebook() {

  }

  public UserNotebook(String uid, String _username, String _email, int _gender, String _image_url, long timestamp, int itemstate) {
    userId = uid;
    username = _username;
    email = _email;
    gender = _gender;
    image_url = _image_url;
    m_state = ItemState.fromInt(itemstate);
    m_timestamp = timestamp;
  }

  @Override
  public String getUserId() {
    return userId;
  }

  @Override
  public String getUsername() {
    return username;
  }

  @Override
  public String getEmail() {
    return email;
  }

  @Override
  public Integer getGender() {
	  return gender;
  }

  @Override
  public String getImgUrl() {
	  return image_url;
  }

  @Override
  public void setTimestamp(long timeInMillis) {
    m_timestamp = timeInMillis;
  }

  @Override
  public long getTimestamp()
  {
	  return m_timestamp;
  }

  @Override
  public ItemState getState() {
    return m_state;
  }

  @Override
  public void setState(ItemState newState) {
    m_state = newState;
  }
}
