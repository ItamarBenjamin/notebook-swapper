package gilaad.itamar.idan.notebookswapper.dbitems.impl;

import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.NotebookType;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;

public class CatalogItem implements ICatalogItem {
  /**
	 *
	 */
	private static final long serialVersionUID = -5658384114349551790L;
private final Long m_CatItemID;
  private final int m_CourseNum;
  private final String m_sEdition;
  private final String m_sBarcode;
  private final NotebookType m_Type;
  private long m_timestamp; // convert date to milliseconds
  private ItemState m_state;

  public CatalogItem(Long id, int coursenum, String edition, NotebookType type,ItemState state, String barcode, long timestamp)
  {
	  m_CatItemID = id;
	  m_CourseNum = coursenum;
	  m_sEdition = edition;
	  m_Type= type;
	  m_state = state;
	  m_sBarcode = barcode;
	  m_timestamp = timestamp;
  }

  @Override
  public Long getCatalogItemID() {
    return m_CatItemID;
  }

  @Override
  public int getCourseNumber() {
    return m_CourseNum;
  }

  @Override
  public String getEdition() {
    return m_sEdition;
  }

  @Override
  public String getBarcode() {
    return m_sBarcode;
  }

  @Override
  public NotebookType getType() {
    return m_Type;
  }

  @Override
  public void setTimestamp(long timeInMillis) {
    m_timestamp = timeInMillis;
  }

  @Override
  public ItemState getState() {
    return m_state;
  }

  @Override
  public void setState(ItemState newState) {
    m_state = newState;
  }

@Override
public Long getTimestamp() {
	return m_timestamp;
}

}
