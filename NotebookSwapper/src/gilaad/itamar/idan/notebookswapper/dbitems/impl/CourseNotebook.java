package gilaad.itamar.idan.notebookswapper.dbitems.impl;

import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;

public class CourseNotebook implements ICourse {
	private static final long serialVersionUID = -1769387692833531238L;
	private Long cId;
	private int m_Number;
	private String m_Name, m_Faculty;
	private long m_timestamp; // convert date to milliseconds
	private ItemState m_state;

  CourseNotebook() {

  }

  public CourseNotebook(int num, String name, String faculty)
  {
	  this (num,name,faculty,ItemState.REGULAR,0L);
  }
  public CourseNotebook(int num, String name, String faculty, ItemState state, long timestamp) {
    m_Number = num;
    m_Name = name;
    m_Faculty = faculty;
    m_state = state;
    m_timestamp = timestamp;
  }

  @Override
  public int getCourseNumber() {
    return m_Number;
  }

  @Override
  public String getCourseName() {
    return m_Name;
  }

  @Override
  public String getFaculty() {
    return m_Faculty;
  }

  @Override
  public Long getCourseId() {
    return cId;
  }

  @Override
  public void setTimestamp(long timeInMillis) {
    m_timestamp = timeInMillis;
  }

  @Override
  public ItemState getState() {
    return m_state;
  }

  @Override
  public void setState(ItemState newState) {
    m_state = newState;
  }

@Override
public Long getTimestamp() {
	return m_timestamp;
}
}
