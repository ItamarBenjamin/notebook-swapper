package gilaad.itamar.idan.notebookswapper.dbitems.impl;

import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.NotebookType;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;

public class WishlistItem implements IWishlistItem {
  /**
	 *
	 */
	private static final long serialVersionUID = 8678195245395055137L;
private Long m_ID;
  private String m_UID; // user id from google plus
  private int m_CourseNum;
  private boolean m_bExchangable, m_bForPurchase;
  private NotebookType m_Type;
  private long m_timestamp; // convert date to milliseconds
  private ItemState m_state;

  WishlistItem() {

  }

  @Override
  public boolean equals(Object o)
	{
		if (null == o || !(o instanceof WishlistItem))
		{
			return false;
		}
		if (((WishlistItem)o).getWishID()==null)
		{
			if (!((WishlistItem)o).m_Type.equals(m_Type))
			{
				return false;
			}
			if (!(((WishlistItem)o).m_CourseNum == m_CourseNum))
			{
				return false;
			}
			if (!(((WishlistItem)o).m_bForPurchase == m_bForPurchase))
			{
				return false;
			}
			return true;
		}
		return ((WishlistItem) o).getWishID().equals(getWishID());
	}

  public WishlistItem(Long id, String uid, int cnum, boolean exch,
      ItemState state, boolean purch, NotebookType type, long timestamp) {
    m_ID = id;
    m_UID = uid;
    m_CourseNum = cnum;
    m_bExchangable = exch;
    m_bForPurchase = purch;
    m_Type = type;
    m_timestamp = timestamp;
    m_state=state;
  }

  public WishlistItem(String uid, int cnum, boolean exch,ItemState state,  boolean purch,
      NotebookType type) {
    this(null, uid, cnum, exch,state ,purch, type, 0l);
  }

  @Override
  public Long getWishID() {
    return m_ID;
  }

  @Override
  public String getUserID() {
    return m_UID;
  }

  @Override
  public int getCourseNumber() {
    return m_CourseNum;
  }

  @Override
  public boolean isExchangable() {
    return m_bExchangable;
  }

  @Override
  public boolean isForPurchase() {
    return m_bForPurchase;
  }

  @Override
  public NotebookType getType() {
    return m_Type;
  }

  @Override
  public void setCourseNumber(int number) {
    m_CourseNum = number;
  }

  @Override
  public void setExchangable(boolean b) {
    m_bExchangable = b;

  }

  @Override
  public void setForPurchase(boolean b) {
    m_bForPurchase = b;

  }

  @Override
  public void setType(NotebookType type) {
    m_Type = type;

  }

  @Override
public Long getTimestamp() {
    return m_timestamp;
  }

  @Override
  public void setTimestamp(long timeInMillis) {
    m_timestamp = timeInMillis;
  }

  @Override
  public ItemState getState() {
    return m_state;
  }

  @Override
  public void setState(ItemState newState) {
    m_state = newState;
  }

}
