package gilaad.itamar.idan.notebookswapper.server.blobstore;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.Log;

public class Upload implements IUpload {
  private final String createBlobStoreUploadUrlAddres = "https://android-236504-i.appspot.com/CreateBlobStoreUploadUrl";

  private final DefaultHttpClient httpclient = new DefaultHttpClient();
  private final MultipartFormRequest multipartFormRequest = new MultipartFormRequest();
  private final HttpPost createBlobStoreUploadUrlHttpPost = new HttpPost(
      createBlobStoreUploadUrlAddres);
  private final Context c;

  public Upload(Context context) {
    c = context;
  }

  private String upload(File file) {
    ArrayList<String> list = null;
    StringBuilder s = new StringBuilder();
    try {
      final HttpResponse response = httpclient
          .execute(createBlobStoreUploadUrlHttpPost);
      final BufferedReader reader = new BufferedReader(new InputStreamReader(
          response.getEntity().getContent(), "UTF-8"));
      String sResponse;
      while ((sResponse = reader.readLine()) != null) {
		s = s.append(sResponse);
	}
      multipartFormRequest.init(s.toString(), "UTF-8");
      multipartFormRequest.addFilePart("myFile", file);
      list = new ArrayList<String>(multipartFormRequest.finish());
    } catch (final Exception e) {
      return null;
    }
    Log.v("TAG", list.get(0));
    return list.get(0);
  }

  public static Uri getImageUri(Context inContext, Bitmap inImage) {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    inImage.compress(Bitmap.CompressFormat.PNG, 70, bytes);
    String path = Images.Media.insertImage(inContext.getContentResolver(),
        inImage, "Title", null);
    return Uri.parse(path);
  }

  public String getRealPathFromURI(Uri uri) {
    Cursor cursor = c.getContentResolver().query(uri, null, null, null, null);
    cursor.moveToFirst();
    int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
    return cursor.getString(idx);
  }

  @Override
  public String uploadImage(Bitmap bm) {
    Uri tempUri = getImageUri(c, bm);
    File finalFile = new File(getRealPathFromURI(tempUri));
    return upload(finalFile);
  }

  @Override
  public String uploadFile(File file) {
    return upload(file);
  }

}
