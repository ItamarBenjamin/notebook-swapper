package gilaad.itamar.idan.notebookswapper.server.blobstore;

import java.io.File;

import android.graphics.Bitmap;

public interface IUpload {

  String uploadImage(Bitmap bm);

  String uploadFile(File file);
}
