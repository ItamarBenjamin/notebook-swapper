package gilaad.itamar.idan.notebookswapper.server.notebook.api.iface;

import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IUserNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.WishlistItem;
import gilaad.itamar.idan.notebookswapper.server.notebook.api.ReturnCodeNotebook;

import java.util.List;


public interface INotebookQuery {
  public List<ICatalogItem> getCatalog(Long timestamp);

  public List<ICourse> getAllCourses(Long timestamp);

  public List<IOfferItem> getMyOffers(Long timestamp, String userId);

  public List<IWishlistItem> getMyWishList(Long timestamp, String userId);

  public List<IOfferItem> getOffersByCatalogID(List<Long> ids, Long timestamp);

  public List<IWishlistItem> getWishesByCourseNumber(List<Integer> ids,
      Long timestamp);

  public List<Message> getMessagesByWish(List<Long> wishesIds, Long timestamp);

  public List<Message> getMessagesByOffer(List<Long> offersIds, Long timestamp);

  Message sendMessage(Message message);

  public ReturnCodeNotebook setIsReadMessages(List<Long> messagesIds);

  public List<IOfferItem> getOffersByIds(List<Long> offersIds, Long timestamp);

  public List<WishlistItem> getWishByIds(List<Long> wishesIds, Long timestamp);

  public List<IUserNotebook> getUsersById(List<String> usersIds, Long timestamp);

}
