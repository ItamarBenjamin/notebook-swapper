package gilaad.itamar.idan.notebookswapper.server.notebook.api;

import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IUserNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.CatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.CourseNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.OfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.UserNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.WishlistItem;
import gilaad.itamar.idan.notebookswapper.server.communicate.Communicator;
import gilaad.itamar.idan.notebookswapper.server.notebook.api.iface.INotebookDatastoreApi;

import java.util.List;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class NotebookDatastoreApi implements INotebookDatastoreApi {

  @Override
  public ICourse addCourse(final ICourse course) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet",
        "function", NotebookFunctions.ADD_COURSE.toString(), "course",
        new Gson().toJson(course)), CourseNotebook.class);
  }

  @Override
  public IWishlistItem addWishItem(final IWishlistItem wishItem) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet",
        "function", NotebookFunctions.ADD_WISH.toString(), "wish-item",
        new Gson().toJson(wishItem)), WishlistItem.class);
  }

  @Override
  public ICatalogItem addCatalogItem(final ICatalogItem catalogItem) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet",
        "function", NotebookFunctions.ADD_CATALOG.toString(), "catalog-item",
        new Gson().toJson(catalogItem)), CatalogItem.class);
  }

  @Override
  public IOfferItem addOfferItem(final IOfferItem offerItem) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet",
        "function", NotebookFunctions.ADD_OFFER.toString(), "offer-item",
        new Gson().toJson(offerItem)), OfferItem.class);
  }

  @Override
  public ReturnCodeNotebook removeCourse(final ICourse course) {
    String res = Communicator.execute("NotebookServlet", "function",
        NotebookFunctions.REMOVE_COURSE.toString(), "course",
        new Gson().toJson(course));
    Log.v("TAG", res);
    return new Gson().fromJson(res, ReturnCodeNotebook.class);
  }

  @Override
  public ReturnCodeNotebook removeWishItem(final IWishlistItem wishItem) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet",
        "function", NotebookFunctions.REMOVE_WISH.toString(), "wish-item",
        new Gson().toJson(wishItem)), ReturnCodeNotebook.class);
  }

  @Override
  public ReturnCodeNotebook removeCatalogItem(final ICatalogItem catalogItem) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet",
        "function", NotebookFunctions.REMOVE_CATALOG.toString(),
        "catalog-item", new Gson().toJson(catalogItem)),
        ReturnCodeNotebook.class);
  }

  @Override
  public ReturnCodeNotebook removeOfferItem(final IOfferItem offerItem) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet",
        "function", NotebookFunctions.REMOVE_OFFER.toString(), "offer-item",
        new Gson().toJson(offerItem)), ReturnCodeNotebook.class);
  }

  @Override
  public ReturnCodeNotebook updateCourse(final ICourse course) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet",
        "function", NotebookFunctions.UPDATE_COURSE.toString(), "course",
        new Gson().toJson(course)), ReturnCodeNotebook.class);
  }

  @Override
  public ReturnCodeNotebook updateWishItem(final IWishlistItem wishItem) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet",
        "function", NotebookFunctions.UPDATE_WISH.toString(), "wish-item",
        new Gson().toJson(wishItem)), ReturnCodeNotebook.class);
  }

  @Override
  public ReturnCodeNotebook updateCatalogItem(final ICatalogItem catalogItem) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet",
        "function", NotebookFunctions.UPDATE_CATALOG.toString(),
        "catalog-item", new Gson().toJson(catalogItem)),
        ReturnCodeNotebook.class);
  }

  @Override
  public ReturnCodeNotebook updateOfferItem(final IOfferItem offerItem) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet",
        "function", NotebookFunctions.UPDATE_OFFER.toString(), "offer-item",
        new Gson().toJson(offerItem)), ReturnCodeNotebook.class);
  }

  @Override
  public IUserNotebook addUser(final IUserNotebook user) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet",
        "function", NotebookFunctions.ADD_USER.toString(), "user",
        new Gson().toJson(user)), UserNotebook.class);
  }

  @Override
  public ReturnCodeNotebook removeUser(final IUserNotebook user) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet",
        "function", NotebookFunctions.REMOVE_USER.toString(), "user",
        new Gson().toJson(user)), ReturnCodeNotebook.class);
  }

  @Override
  public ReturnCodeNotebook updateUser(final IUserNotebook user) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet",
        "function", NotebookFunctions.UPDATE_USER.toString(), "user",
        new Gson().toJson(user)), ReturnCodeNotebook.class);
  }

  @Override
  public List<IWishlistItem> addWishes(List<IWishlistItem> wishItems) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet",
        "function", NotebookFunctions.ADD_WISHES.toString(), "wishes",
        new Gson().toJson(wishItems)), new TypeToken<List<WishlistItem>>() {
      // default usage
    }.getType());
  }

  @Override
  public List<IOfferItem> addOffers(List<IOfferItem> offerItems) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet",
        "function", NotebookFunctions.ADD_OFFERS.toString(), "offers",
        new Gson().toJson(offerItems)), new TypeToken<List<OfferItem>>() {
      // default usage
    }.getType());
  }
}
