package gilaad.itamar.idan.notebookswapper.server.notebook.api.iface;

import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IUserNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import gilaad.itamar.idan.notebookswapper.server.notebook.api.ReturnCodeNotebook;

import java.util.List;


public interface INotebookDatastoreApi {

  ICourse addCourse(ICourse course);

  IWishlistItem addWishItem(IWishlistItem wishItem);

  ICatalogItem addCatalogItem(ICatalogItem catalogItem);

  IOfferItem addOfferItem(IOfferItem offerItem);

  IUserNotebook addUser(IUserNotebook user);

  ReturnCodeNotebook removeCourse(ICourse course);

  ReturnCodeNotebook removeWishItem(IWishlistItem wishItem);

  ReturnCodeNotebook removeCatalogItem(ICatalogItem catalogItem);

  ReturnCodeNotebook removeOfferItem(IOfferItem offerItem);

  ReturnCodeNotebook removeUser(IUserNotebook user);

  ReturnCodeNotebook updateCourse(ICourse course);

  ReturnCodeNotebook updateWishItem(IWishlistItem wishItem);

  ReturnCodeNotebook updateCatalogItem(ICatalogItem catalogItem);

  ReturnCodeNotebook updateOfferItem(IOfferItem offerItem);

  ReturnCodeNotebook updateUser(IUserNotebook user);

  List<IWishlistItem> addWishes(List<IWishlistItem> wishItems);

  List<IOfferItem> addOffers(List<IOfferItem> offerItem);
}
