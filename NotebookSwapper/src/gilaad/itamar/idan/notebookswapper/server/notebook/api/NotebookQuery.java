package gilaad.itamar.idan.notebookswapper.server.notebook.api;

import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IUserNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.CatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.CourseNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.OfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.UserNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.WishlistItem;
import gilaad.itamar.idan.notebookswapper.server.communicate.Communicator;
import gilaad.itamar.idan.notebookswapper.server.notebook.api.iface.INotebookQuery;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class NotebookQuery implements INotebookQuery {


  public NotebookQuery() {
  }

  @Override
  public List<ICatalogItem> getCatalog(Long timestamp) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet", "query",
        "", "function", NotebookFunctions.GET_CATALOG.toString(), "timestamp",
        new Gson().toJson(timestamp)), new TypeToken<List<CatalogItem>>() {
      // default usage
    }.getType());
  }

  @Override
  public List<ICourse> getAllCourses(Long timestamp) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet", "query",
        "", "function", NotebookFunctions.GET_ALL_COURSES.toString(),
        "timestamp", new Gson().toJson(timestamp)),
        new TypeToken<List<CourseNotebook>>() {
          // default usage
        }.getType());
  }

  @Override
  public List<IOfferItem> getMyOffers(Long timestamp, String userId) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet", "query",
        "", "function", NotebookFunctions.GET_MY_OFFERS.toString(), "user-id",
        userId, "timestamp", new Gson().toJson(timestamp)),
        new TypeToken<List<OfferItem>>() {
          // default usage
        }.getType());
  }

  @Override
  public List<IWishlistItem> getMyWishList(Long timestamp, String userId) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet", "query",
        "", "function", NotebookFunctions.GET_MY_WISH_LIST.toString(),
        "user-id", userId, "timestamp", new Gson().toJson(timestamp)),
        new TypeToken<List<WishlistItem>>() {
          // default usage
        }.getType());
  }

  @Override
  public List<IOfferItem> getOffersByCatalogID(List<Long> ids, Long timestamp) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet", "query",
        "", "function", NotebookFunctions.GET_OFFER_BY_CATALOG_ID.toString(),
        "catalog-ids", new Gson().toJson(ids), "timestamp",
        new Gson().toJson(timestamp)), new TypeToken<List<OfferItem>>() {
      // default usage
    }.getType());
  }

  @Override
  public List<IWishlistItem> getWishesByCourseNumber(List<Integer> ids,
      Long timestamp) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet", "query",
        "", "function",
        NotebookFunctions.GET_WISHES_BY_COURSE_NUMBER.toString(),
        "courses-ids", new Gson().toJson(ids), "timestamp",
        new Gson().toJson(timestamp)), new TypeToken<List<IWishlistItem>>() {
      // default usage
    }.getType());
  }

  @Override
  public List<Message> getMessagesByWish(List<Long> wishIds, Long timestamp) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet", "query",
        "", "function", NotebookFunctions.GET_MESSAGE_BY_WISH.toString(),
        "ids", new Gson().toJson(wishIds), "timestamp",
        new Gson().toJson(timestamp)), new TypeToken<List<Message>>() {
      // default usage
    }.getType());
  }

  @Override
  public List<Message> getMessagesByOffer(List<Long> offersIds, Long timestamp) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet", "query",
        "", "function", NotebookFunctions.GET_MESSAGE_BY_OFFER.toString(),
        "ids", new Gson().toJson(offersIds), "timestamp",
        new Gson().toJson(timestamp)), new TypeToken<List<Message>>() {
      // default usage
    }.getType());
  }

  @Override
  public Message sendMessage(Message message) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet", "query",
        "", "function", NotebookFunctions.SEND_MESSAGE.toString(), "message",
        new Gson().toJson(message)), Message.class);
  }

  @Override
  public List<IOfferItem> getOffersByIds(List<Long> offersIds, Long timestamp) {
    return new Gson().fromJson(
        Communicator.execute("NotebookServlet", "query", "", "function",
            NotebookFunctions.GET_OFFERS_BY_IDS.toString(), "ids",
            new Gson().toJson(offersIds), "timestamp",
            new Gson().toJson(timestamp)), new TypeToken<List<OfferItem>>() {
          // default usage
        }.getType());
  }

  @Override
  public List<WishlistItem> getWishByIds(List<Long> wishesIds, Long timestamp) {
    return new Gson().fromJson(
        Communicator.execute("NotebookServlet", "query", "", "function",
            NotebookFunctions.GET_WISHES_BY_IDS.toString(), "ids",
            new Gson().toJson(wishesIds), "timestamp",
            new Gson().toJson(timestamp)), new TypeToken<List<WishlistItem>>() {
          // default usage
        }.getType());
  }

  @Override
  public List<IUserNotebook> getUsersById(List<String> usersIds, Long timestamp) {
    return new Gson().fromJson(
        Communicator.execute("NotebookServlet", "query", "", "function",
            NotebookFunctions.GET_USERS_BY_IDS.toString(), "ids",
            new Gson().toJson(usersIds), "timestamp",
            new Gson().toJson(timestamp)), new TypeToken<List<UserNotebook>>() {
          // default usage
        }.getType());
  }

  @Override
  public ReturnCodeNotebook setIsReadMessages(List<Long> messagesIds) {
    return new Gson().fromJson(Communicator.execute("NotebookServlet", "query",
        "", "function", NotebookFunctions.SET_IS_READ_MESSAGES.toString(),
        "ids", new Gson().toJson(messagesIds)), ReturnCodeNotebook.class);
  }

}
