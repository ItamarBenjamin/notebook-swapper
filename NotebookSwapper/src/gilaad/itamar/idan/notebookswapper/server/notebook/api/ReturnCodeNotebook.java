package gilaad.itamar.idan.notebookswapper.server.notebook.api;

public enum ReturnCodeNotebook {
  BAD_PARAM("BAD_PARAM"), SUCCESS("SUCCESS"), ENTITY_NOT_EXISTS(
      "ENTITY_NOT_EXISTS");

  private final String value;

  private ReturnCodeNotebook(String s) {
    value = s;
  }

  public String value() {
    return value;
  }
}
