package gilaad.itamar.idan.notebookswapper.server.gcm;

public class GcmInfo {

  String subject;
  String message;

  /**
   * @param subject1
   * @param message1
   */
  public GcmInfo(String subject1, String message1) {
    super();
    subject = subject1;
    message = message1;
  }

  GcmInfo() {
  }

  /**
   * @return the subject
   */
  public String getSubject() {
    return subject;
  }

  /**
   * @param subject1
   *          the subject to set
   */
  public void setSubject(String subject1) {
    subject = subject1;
  }

  /**
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * @param message1
   *          the message to set
   */
  public void setMessage(String message1) {
    message = message1;
  }
}
