package gilaad.itamar.idan.notebookswapper.server.gcm;

import gilaad.itamar.idan.notebookswapper.server.communicate.Communicator;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;

/**
 *
 * @modified 05/04/2014
 * @author Omer Shpigelman <omer.shpigelman@gmail.com>
 *
 *         Google's Gcm registration class with my modifications.
 *
 */

public class GcmRegistration extends Activity {

  public static final String EXTRA_MESSAGE = "message";
  public static final String PROPERTY_REG_ID = "registration_id";
  private static final String PROPERTY_APP_VERSION = "appVersion";
  private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
  String SENDER_ID = "703000269582";

  static final String TAG = "GcmRegistration";
  GoogleCloudMessaging mGcm;
  static Context mContext;
  Communicator mCommunicator;
  String mAppName;
  String mUserId;
  String mRegId;

  public void doRegistration(Communicator communicator, Context context,
      String userId, String appName) {
    Log.d(TAG, "doRegistration");
    if (communicator == null || context == null || userId == null
        || appName == null) {
      Log.d(TAG, "doRegistration: Invalid parameters");
      return;
    }
    mContext = context;
    mCommunicator = communicator;
    mUserId = userId;
    mAppName = appName;

    // Check device for Play Services APK. If check succeeds, proceed with
    // GCM registration.

    if (checkPlayServices()) {
      mGcm = GoogleCloudMessaging.getInstance(mContext);
      mRegId = getRegistrationId();

		registerInBackground();
    } else {
		Log.d(TAG, "No valid Google Play Services APK found");
	}
  }

  /**
   * Gets the current registration ID for application on GCM service.
   * <p>
   * If result is empty, the app needs to register.
   *
   * @return registration ID, or empty string if there is no existing
   *         registration ID.
   */
  private static String getRegistrationId() {
    Log.d(TAG, "getRegistrationId");
    final SharedPreferences prefs = getGCMPreferences();
    String registrationId = prefs.getString(PROPERTY_REG_ID, "");
    if (registrationId.isEmpty()) {
      Log.i(TAG, "Registration not found.");
      return "";
    }
    // Check if app was updated; if so, it must clear the registration ID
    // since the existing regID is not guaranteed to work with the new
    // app version.
    int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
        Integer.MIN_VALUE);
    int currentVersion = getAppVersion();
    if (registeredVersion != currentVersion) {
      Log.i(TAG, "App version changed.");
      return "";
    }
    return registrationId;
  }

  /**
   * @return Application's {@code SharedPreferences}.
   */
  private static SharedPreferences getGCMPreferences() {
    // This sample app persists the registration ID in shared preferences, but
    // how you store the regID in your app is up to you.
    return mContext.getSharedPreferences(GcmRegistration.class.getSimpleName(),
        Context.MODE_PRIVATE);
  }

  /**
   * @return Application's version code from the {@code PackageManager}.
   */
  private static int getAppVersion() {
    Log.d(TAG, "getAppVersion");
    try {
      PackageInfo packageInfo = mContext.getPackageManager().getPackageInfo(
          mContext.getPackageName(), 0);
      return packageInfo.versionCode;
    } catch (NameNotFoundException e) {
      // should never happen
      throw new RuntimeException("Could not get package name: " + e);
    }
  }

  /**
   * Registers the application with GCM servers asynchronously.
   * <p>
   * Stores the registration ID and app versionCode in the application's shared
   * preferences.
   *
   * @param communicator
   * @throws ExecutionException
   * @throws InterruptedException
   */
  private void registerInBackground() {
    Log.d(TAG, "registerInBackground");
    new AsyncTask<Void, Void, String>() {
      @Override
      protected String doInBackground(Void... params) {
        String msg = "";
        double recoverySleep = 0.5;
        while (msg.equals("")) {
			try {
			    if (mGcm == null) {
					mGcm = GoogleCloudMessaging.getInstance(mContext);
				}
			    mRegId = mGcm.register(SENDER_ID);
			    msg = "Device registered, registration ID=" + mRegId;

			    // You should send the registration ID to your server
			    // over HTTP,
			    // so it can use GCM/HTTP or CCS to send messages to
			    // your app.
			    // The request to your server should be authenticated if
			    // your app
			    // is using accounts.
			    sendRegistrationIdToBackend();

			    // For this demo: we don't need to send it because the
			    // device
			    // will send upstream messages to a server that echo
			    // back the
			    // message using the 'from' address in the message.

			    // Persist the regID - no need to register again.
			    storeRegistrationId();
			  } catch (IOException ex) {
			    if (ex.getMessage().equals("SERVICE_NOT_AVAILABLE")) {
			      recoverySleep *= 2;
			      try {
			        Thread.sleep((long) recoverySleep);
			      } catch (InterruptedException e) {
			        e.printStackTrace();
			      }
			      continue;
			    }
			    msg = "Error: " + ex.getMessage();
			    // If there is an error, don't just keep trying to
			    // register.
			    // Require the user to click a button again, or perform
			    // exponential back-off.
			  }
		}
        return msg;
      }

      @Override
      protected void onPostExecute(String msg) {
        Log.d(TAG, msg + "\n");
      }
    }.execute(null, null, null);

  }

  /**
   * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
   * or CCS to send messages to your app. Not needed for this demo since the
   * device sends upstream messages to a server that echoes back the message
   * using the 'from' address in the message.
   *
   */
  String sendRegistrationIdToBackend() {
    Log.d(TAG, "sendRegistrationIdToBackend");
    Device d = new Device(null, mUserId, mRegId, mAppName);
    return Communicator.execute("GcmServlet", "device", new Gson().toJson(d),
        "message", "Hello notification!");
  }

  /**
   * Stores the registration ID and app versionCode in the application's
   * {@code SharedPreferences}.
   *
   * @param context
   *          application's context.
   * @param regId
   *          registration ID
   */
  void storeRegistrationId() {
    final SharedPreferences prefs = getGCMPreferences();
    int appVersion = getAppVersion();
    Log.i(TAG, "Saving regId on app version " + appVersion);
    SharedPreferences.Editor editor = prefs.edit();
    editor.putString(PROPERTY_REG_ID, mRegId);
    editor.putInt(PROPERTY_APP_VERSION, appVersion);
    editor.commit();
  }

  /**
   * Check the device to make sure it has the Google Play Services APK. If it
   * doesn't, display a dialog that allows users to download the APK from the
   * Google Play Store or enable it in the device's system settings.
   */
  private boolean checkPlayServices() {
	  return true;
/*    int resultCode = GooglePlayServicesUtil
        .isGooglePlayServicesAvailable(mContext);
    if (resultCode != ConnectionResult.SUCCESS) {
      if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
		GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) mContext,
            PLAY_SERVICES_RESOLUTION_REQUEST).show();

	} else {
        Log.i(TAG, "This device is not supported.");
        Toast.makeText(mContext, "This device is not supported.",
            Toast.LENGTH_SHORT).show();
        finish();
      }
      return false;
    }
    return true;*/
  }

  public void clearRegisterData() {
	    SharedPreferences share = getGCMPreferences();
	    SharedPreferences.Editor editor = share.edit();
	    editor.clear();
	    editor.commit();
	  }

	  public boolean deleteDevice() {
	    SharedPreferences share = mContext.getSharedPreferences(GcmRegistration.class.getSimpleName(),
	            Context.MODE_PRIVATE);
	    String registrationId = share.getString(PROPERTY_REG_ID, "");
	    String res = Communicator.execute("GcmDeleteServlet", "regId",
	        registrationId);
	    return res.equals("OK");
	  }
}
