package gilaad.itamar.idan.notebookswapper.server.gcm.match;

public interface NotebookMatcher {

  String getReceiverId();

  String getAction();
}
