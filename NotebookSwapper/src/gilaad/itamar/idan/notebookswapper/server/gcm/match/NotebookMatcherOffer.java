package gilaad.itamar.idan.notebookswapper.server.gcm.match;

public class NotebookMatcherOffer implements NotebookMatcher {
	public String receiver;
	public Long offerId;
	public Long wishId;

  NotebookMatcherOffer(String reciever, Long offid, Long wishid)
  {
	receiver = reciever;
	offerId = offid;
	wishId = wishid;
  }

  @Override
  public String getReceiverId() {
    return receiver;
  }

  @Override
  public String getAction() {
    return "offer-match";
  }
}
