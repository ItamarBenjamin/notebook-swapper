package gilaad.itamar.idan.notebookswapper.server.gcm;

public class Device {
  Long Id;
  String regId;
  String userId;
  String appName;

  /**
   * @param id
   * @param regId1
   * @param userId1
   * @param appName1
   */
  public Device(Long id, String userId1, String regId1, String appName1) {
    Id = id;
    userId = userId1;
    regId = regId1;
    appName = appName1;
  }

  Device() {
  }

  /**
   * @return the userId
   */
  public String getUserId() {
    return userId;
  }

  /**
   * @param userId1
   *          the userId to set
   */
  public void setUserId(String userId1) {
    userId = userId1;
  }

  /**
   * @return the regId
   */
  public String getRegId() {
    return regId;
  }

  /**
   * @param regId1
   *          the regId to set
   */
  public void setRegId(String regId1) {
    regId = regId1;
  }

  /**
   * @return the appName
   */
  public String getAppName() {
    return appName;
  }

  /**
   * @param appName1
   *          the appName to set
   */
  public void setAppName(String appName1) {
    appName = appName1;
  }

}
