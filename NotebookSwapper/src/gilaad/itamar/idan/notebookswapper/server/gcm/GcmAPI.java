package gilaad.itamar.idan.notebookswapper.server.gcm;

import gilaad.itamar.idan.notebookswapper.server.communicate.Communicator;
import android.content.Context;

public class GcmAPI implements IGcmAPI {

  @Override
  public void registerDevice(Communicator communicator, Context context,
      String userId, String appName) {
    new GcmRegistration()
        .doRegistration(communicator, context, userId, appName);
  }

  @Override
  public void clearRegisterData() {
    new GcmRegistration().clearRegisterData();
  }

  @Override
  public boolean deleteDevice() {
    return new GcmRegistration().deleteDevice();
  }
}
