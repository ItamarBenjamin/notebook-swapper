package gilaad.itamar.idan.notebookswapper.server.gcm;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.accounts.Account;
import gilaad.itamar.idan.notebookswapper.accounts.AccountManager;
import gilaad.itamar.idan.notebookswapper.basket.MainActivity;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.dbhandler.RemoteDBDataManager;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IUserNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;
import gilaad.itamar.idan.notebookswapper.server.gcm.match.NotebookMatcherOffer;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;

public class GcmIntentService extends IntentService {
  public static final int NOTIFICATION_ID = 1;
  private NotificationManager mNotificationManager;
  static final String TAG = "GcmIntentService";

  public final static String OFFER_MATCH_NOTIFICATION = "OFFER_MATCH";
  NotificationCompat.Builder builder;

  public GcmIntentService() {
    super("GcmIntentService");
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    Bundle extras = intent.getExtras();
    GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
    // The getMessageType() intent parameter must be the intent you received
    // in your BroadcastReceiver.
    String messageType = gcm.getMessageType(intent);

    if (!extras.isEmpty()) {
		/*
		   * Filter messages based on message type. Since it is likely that GCM will
		   * be extended in the future with new message types, just ignore any
		   * message types you're not interested in, or that you don't recognize.
		   */
		  if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
			sendNotification("", "Send error: " + extras.toString());
		} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
			sendNotification("", "Deleted messages on server: " + extras.toString());
		} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
		    // This loop represents the service doing some work.
		    for (int i = 0; i < 3; i++) {
		      Log.i(TAG,
		          "Working... " + (i + 1) + "/3 @ " + SystemClock.elapsedRealtime());
		      try {
		        Thread.sleep(1000);
		      } catch (InterruptedException e) {
		        e.printStackTrace();
		      }
		    }
		    Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
		    // Post notification of received message.AddCourses
		    sendNotification(extras.getString("action"), extras.getString("data"));
		    Log.i(TAG, "Received: " + extras.toString());
		  }
	}
    // Release the wake lock provided by the WakefulBroadcastReceiver.
    WakefulBroadcastReceiver.completeWakefulIntent(intent);
  }

  // Put the message into a notification and post it.
  // This is just one simple example of what you might choose to do with
  // a GCM message. comes from match


  //action = "Chat" jsonobject = class Message
  public final String GCM_ACTION_CHAT = "Chat";
  public final String GCM_ACTION_MATCH = "offer-match";

  private void sendNotification(String action, String jsonObject)
  {
    if (action == null) {
		return;
	}
    Account currentAccount = AccountManager.getCurrentAccount(getApplicationContext());
    if (currentAccount == null)
    {
    	return;
    }
    String me = currentAccount.getUID();
    mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

    String title;
    String content = "";
    if (action.equalsIgnoreCase(GCM_ACTION_CHAT))
    {
    	int max_content_size = 15;
    	title = getResources().getString(R.string.gcm_new_message_recieved);
    	Message msg = new Gson().fromJson(jsonObject, Message.class);
    	if (!msg.receiver.equals(me))
    	{
    		return;
    	}
    	try {
			msg.contents = URLDecoder.decode(msg.contents, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return;
		}
    	if (msg.contents.length() <= max_content_size) {
			content = msg.contents;
		} else {
			content = msg.contents.substring(0, max_content_size-1) + "...";
		}
    	String senderID = msg.getSender();
    	IUserNotebook userByID = CacheManager.getManager(getApplicationContext()).getUserByID(senderID);
    	if (null == userByID)
    	{
    		Log.v("GCM", "User with id " + userByID + " was not found, retreiving it before adding message");
    		List<String> lstUsers = new ArrayList<String>();
    		lstUsers.add(senderID);
    		List<IUserNotebook> users = new RemoteDBDataManager().getUsers(lstUsers, 0);
    		if (null == users || users.isEmpty())
    		{
    			Log.e("GCM", "FAILED to retrieve user with id " + userByID + " won't send message");
    			return;
    		}
    		CacheManager.getManager(getApplicationContext()).addUsers(users);
    	}

    	List<Message> myAllMessages = CacheManager.getManager(getApplicationContext()).getMyAllMessages();
    	for (Message message : myAllMessages) {
			if (message.id.equals(msg.id))
			{
				Log.v("GCM", "Message already found in cache, won't add it again");
				return;
			}
		}

    	Intent i = new Intent(getApplicationContext(), RemoteDBService.class);
		i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.GetGCMMessage);
		i.putExtra(RemoteDBService.PARAM_OBJECT, msg);
		startService(i);
    } else if (action.equalsIgnoreCase(GCM_ACTION_MATCH))
    {
    	NotebookMatcherOffer match = new Gson().fromJson(jsonObject, NotebookMatcherOffer.class);
    	if (!match.receiver.equals(me))
    	{
    		return;
    	}
    	IWishlistItem myWish = CacheManager.getManager(getApplicationContext()).getMyWish(match.wishId);
    	if (myWish == null)
    	{
    		Log.e("GCM","Got GCM Match but wish is not in cache, wishid: " + match.wishId.toString());
    		return;
    	}
    	ICourse course = CacheManager.getManager(getApplicationContext()).getCourse(myWish.getCourseNumber());
    	if (course == null)
    	{
    		Log.e("GCM","Got GCM Match but course is not in cache, coursenum: " + myWish.getCourseNumber());
    		return;
    	}

    	title = getResources().getString(R.string.gcm_new_match_recieved);
    	title += " " + course.getCourseName() + "-" + myWish.getType().toName();
    	CacheManager.getManager(getApplicationContext()).addedOffer(myWish.getWishID());
		Intent i = new Intent(OFFER_MATCH_NOTIFICATION);
		sendBroadcast(i);
    }
    else
	{
    	return;
	}

    PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
        new Intent(this, MainActivity.class), 0);

    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
        .setSmallIcon(gilaad.itamar.idan.notebookswapper.R.drawable.ic_launcher_shukbook)
        .setContentTitle(title)
        .setStyle(
            new NotificationCompat.BigTextStyle().bigText(title
                + "\n" + content))
        .setContentText(content);

    mBuilder.setContentIntent(contentIntent);
    mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
  }
}
