//package gilaad.itamar.idan.notebookswapper.server.gcm.match;
//
//import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;
//import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
//import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
//import gilaad.itamar.idan.notebookswapper.dbitems.impl.CatalogItem;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class Matcher implements IMatcher {
//
//  @Override
//  public List<NotebookMatcher> findMatches(IOfferItem offerItem) {
//	List<NotebookMatcher> lstNotebookMatches = new ArrayList<NotebookMatcher>();
//
//	List<IWishlistItem> lstAllWishes = getAllWishes();
//    CatalogItem catalogItem = getCatalogItemById(offerItem.getCatalogItemID());
//
//    for (IWishlistItem wishlistItem : lstAllWishes)
//    {
//    	if (catalogItem.getCourseNumber() == wishlistItem.getCourseNumber())
//    	{
//    		if (catalogItem.getType().equals(wishlistItem.getType()))
//    		{
//    			if (wishlistItem.getState() == ItemState.REGULAR)
//    			{
//    				if ((wishlistItem.isExchangable() && offerItem.isExchangable()) || offerItem.isFree() || wishlistItem.isForPurchase())
//    				{
//    					lstNotebookMatches.add(new NotebookMatcherOffer(wishlistItem.getUserID(),offerItem.getOfferID(), wishlistItem.getWishID()));
//    				}
//    			}
//    		}
//    	}
//	}
//    return lstNotebookMatches;
//  }
//}
