package gilaad.itamar.idan.notebookswapper.server.gcm;

import gilaad.itamar.idan.notebookswapper.server.communicate.Communicator;
import android.content.Context;

/**
 *
 * @modified 05/04/2014
 * @author Omer Shpigelman <omer.shpigelman@gmail.com>
 *
 */

public interface IGcmAPI {

  /**
   * Connect the user to the application and save his data and state
   *
   * @param id
   *          user's id
   * @param username
   *          user's Nickname
   * @param communicator
   * @return IUserManager.SUCCESS in case the user logged in, "Fail" otherwise.
   */

  /**
   * Register the user's device to the Gcm services.
   *
   * @param communicator
   *          the communicator to use when registering to the server
   * @param context
   *          the context of the activity that called us
   * @param appName
   *          the name of the app
   * @param userId
   *          the id of the registered user
   */
  public void registerDevice(Communicator communicator, Context context,
      String userId, String appName);

  public void clearRegisterData();

  public boolean deleteDevice();


}
