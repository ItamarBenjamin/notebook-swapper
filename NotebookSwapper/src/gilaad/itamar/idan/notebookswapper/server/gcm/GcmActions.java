package gilaad.itamar.idan.notebookswapper.server.gcm;

public enum GcmActions {
  ACTION("action"), UPDATE_WORD_URL("update_word_url"), REMOVE_WORD_URL(
      "remove_word_url");

  private final String value;

  private GcmActions(String s) {
    value = s;
  }

  public String value() {
    return value;
  }

  public enum UpdateWordUrlExtras {

    WORD("word"), LANGUAGE("language"), URL("url"), REPORTS("reports"), PRIVILIGE(
        "privilage"), USER("user");

    private final String value;

    private UpdateWordUrlExtras(String s) {
      value = s;
    }

    public String value() {
      return value;
    }
  }

  public enum RemoveWordUrlExtras {

    WORD("word"), URL("url");

    private final String value;

    private RemoveWordUrlExtras(String s) {
      value = s;
    }

    public String value() {
      return value;
    }
  }

}
