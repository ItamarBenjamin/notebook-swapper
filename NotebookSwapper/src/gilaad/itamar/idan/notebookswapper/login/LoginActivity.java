package gilaad.itamar.idan.notebookswapper.login;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.accounts.Account;
import gilaad.itamar.idan.notebookswapper.accounts.AccountManager;
import gilaad.itamar.idan.notebookswapper.accounts.UserAccount;
import gilaad.itamar.idan.notebookswapper.animation.DefaultAnimation;
import gilaad.itamar.idan.notebookswapper.asynctasks.GetOffersByCatIDAsyncTask;
import gilaad.itamar.idan.notebookswapper.basket.MainActivity;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.UserNotebook;
import gilaad.itamar.idan.notebookswapper.searchnotebook.SearchNotebookActivity;
import gilaad.itamar.idan.notebookswapper.server.communicate.Communicator;
import gilaad.itamar.idan.notebookswapper.server.gcm.GcmFactory;
import gilaad.itamar.idan.notebookswapper.server.gcm.GcmRegistration;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService.RemoteDBServiceOperations;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

public class LoginActivity extends Activity implements ConnectionCallbacks, OnConnectionFailedListener {
	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private static final int RC_SIGN_IN = 0;
	// Values for email and password at the time of the login attempt.
	private boolean m_registeredToBroadcast;

	// UI references.
	private SignInButton m_SignInGoogle;
	private Button m_SignInGuest;
	private Button m_SignInDeveloper;
	private BroadcastReceiver m_Reciever;
	private Integer m_iCounter = 0;
	private Boolean m_bFailed = false;
	private ImageView m_LogoImage;
	private int m_clicks = 0;
	private boolean m_signout = false;
	public final static String PREF_CONNECTED_GOOGLE = "gilaad.itamar.idan.notebookswapper.login.isConnectedWithGoogle";
	public static final String REVOKE_ACCESS = "REVOKE_ACCESS";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		GetOffersByCatIDAsyncTask.EXCHANGE = getString(R.string.Exchange);
		GetOffersByCatIDAsyncTask.GIVEAWAY = getString(R.string.Giveaway);
		GetOffersByCatIDAsyncTask.SALE = getString(R.string.Sale);
		super.onCreate(savedInstanceState);
		Log.v("login activity", "onCreate called");
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_login);

		CacheManager.createManager(getApplicationContext());
//
		// Initializing google plus api client
		mGoogleApiClient = new GoogleApiClient.Builder(this)
        		.addConnectionCallbacks(this)
        		.addOnConnectionFailedListener(this).addApi(Plus.API)
        		.addScope(Plus.SCOPE_PLUS_LOGIN).build();
		// Set up the login form.
		checkIntent(getIntent());
		findControls();

		setButtonsHandlers();

		CacheManager.getManager(this).update();
	}

	private void checkIntent(Intent intent) {
		if (intent != null && intent.getAction() != null && intent.getAction().equals(REVOKE_ACCESS))
		{
			(new AsyncTask<Void, Void, Void>(){

				@Override
				protected Void doInBackground(Void... arg0) {
					try{
					  final String PROPERTY_REG_ID = "registration_id";
					SharedPreferences share = getApplicationContext().getSharedPreferences(GcmRegistration.class.getSimpleName(),
				            Context.MODE_PRIVATE);
				    String registrationId = share.getString(PROPERTY_REG_ID, "");
				    Communicator.execute("GcmDeleteServlet", "regId",
				        registrationId);
					}catch(Throwable t){

					}
					return null;
				}

			}).execute();

			if (!mGoogleApiClient.isConnected()) {
				mGoogleApiClient.connect();
				m_signout = true;
			} else
			{
				Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
		        Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient);
			}

		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		checkIntent(intent);
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.v("login activity", "onStart called");
		SharedPreferences prefs = getSharedPreferences(LoginActivity.class.getPackage().getName(), Context.MODE_PRIVATE);
		Boolean b = prefs.getBoolean(PREF_CONNECTED_GOOGLE, false);
		if (b) {
			setControllersActive(false);
			mGoogleApiClient.connect();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.v("login activity", "onResume called");
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.v("login activity", "onPause called");
	}


	@Override
	protected void onStop() {
		super.onStop();
		Log.v("login activity", "onStop called");
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
	}


	private void setButtonsHandlers() {
		final Animation animPress = DefaultAnimation.getDefaultButtonAnimation(this);
		m_SignInGoogle.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						setControllersActive(false);
						view.startAnimation(animPress);
						signInWithGplus();
					}
				});

		m_SignInGuest.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						m_SignInGoogle.setEnabled(false);
						m_SignInDeveloper.setEnabled(false);
						m_SignInGuest.setEnabled(false);
						v.startAnimation(animPress);
						guestLogin();
					}
				});

		m_SignInDeveloper.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						m_SignInGoogle.setEnabled(false);
						m_SignInDeveloper.setEnabled(false);
						m_SignInGuest.setEnabled(false);
						v.startAnimation(animPress);
						developerLogin();
					}
				});
		m_LogoImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				m_clicks++;
				if (m_clicks == 5)
				{
					m_SignInDeveloper.setVisibility(View.VISIBLE);
				}
			}
		});
	}

	void developerLogin() {
		setProgressBarIndeterminateVisibility(true);
		AccountManager.setDeveloperAccount(getApplicationContext());
		registerUserOnServer();
		GcmFactory.getGcmAPI().registerDevice(new Communicator(), getApplication(), AccountManager.getCurrentAccount(getApplicationContext()).getUID(),"NotebookSwapper");
    	m_registeredToBroadcast = false;
		trackUserSpecificDataUpdate();
		performUserSpecificDataUpdate();
	}


	private void registerUserOnServer() {
		Account mCurrentAccount = AccountManager.getCurrentAccount(getApplicationContext());
		Intent i = new Intent(getApplicationContext(), RemoteDBService.class);
		i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.AddUser);
		i.putExtra(RemoteDBService.PARAM_OBJECT, new UserNotebook(mCurrentAccount.getUID(), mCurrentAccount.getName(),
							mCurrentAccount.getEmail(),mCurrentAccount.getGender(),mCurrentAccount.getImageURL(),0l,ItemState.REGULAR.getStatus()));
		startService(i);
	}


	void guestLogin() {
		setProgressBarIndeterminateVisibility(true);
		AccountManager.setGuestAccount(getApplicationContext());
		gotoActivity(SearchNotebookActivity.class);
	}

	/**
     * Sign-in into google
     * */
    void signInWithGplus() {
    	setProgressBarIndeterminateVisibility(true);
    	if (mGoogleApiClient.isConnected()) {
    		afterConnectionEstablished();
    	} else if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInError();
        }
    }

	private void findControls() {
		m_SignInGoogle = (SignInButton) findViewById(R.id.sign_in_button);
		m_SignInGuest = (Button) findViewById(R.id.guest_login_button);
		m_SignInDeveloper = (Button) findViewById(R.id.developer_login_button);
		m_LogoImage = (ImageView) findViewById(R.id.imgLogo);
	}

	private void trackUserSpecificDataUpdate() {
		if (m_registeredToBroadcast) {
			return;
		}
		// perform the user login attempt.
		m_bFailed = false;
		final int number_of_recieves = 5;
		m_Reciever = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.v("Login", "Got update from " + intent.getAction());
				synchronized (m_bFailed) {
					if (m_bFailed) {
						return;
					}
					if (intent.getBooleanExtra(RemoteDBService.PARAM_ERROR, false))
					{
						handleLoginFail();
						m_bFailed=true;
						return;
					}
				}
				synchronized (m_iCounter)
				{
					m_iCounter++;
					if (m_iCounter == number_of_recieves)
					{
						gotoActivity(MainActivity.class);
					}
					else
					{
						if (m_iCounter == 2)
						{
							//Updated wishes and offers, now update messages
							Intent i = new Intent(getApplicationContext() , RemoteDBService.class);
							i = new Intent(getApplicationContext(), RemoteDBService.class);
							i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.UpdateMyMessages);
							startService(i);
						}
						else if (m_iCounter == 3)
						{
							Intent i = new Intent(getApplicationContext() , RemoteDBService.class);
							i = new Intent(getApplicationContext(), RemoteDBService.class);
							i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.UpdateRelevantOffers);
							startService(i);
						} else if (m_iCounter == 4)
						{
							Intent i = new Intent(getApplicationContext() , RemoteDBService.class);
							i = new Intent(getApplicationContext(), RemoteDBService.class);
							i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.UpdateUsers);
							startService(i);
						}
					}
				}
			}
		};
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(RemoteDBServiceOperations.UpdateMyWishes.toString());
		intentFilter.addAction(RemoteDBServiceOperations.UpdateMyOffers.toString());
		intentFilter.addAction(RemoteDBServiceOperations.UpdateMyMessages.toString());
		intentFilter.addAction(RemoteDBServiceOperations.UpdateRelevantOffers.toString());
		intentFilter.addAction(RemoteDBServiceOperations.UpdateUsers.toString());
		registerReceiver(m_Reciever, intentFilter);
		m_registeredToBroadcast = true;
	}

	protected void handleLoginFail() {
		Log.v("Login", "Login has failed");
		Toast.makeText(getApplicationContext(), "ההתחברות נכשלה, אנא נסה שנית", Toast.LENGTH_SHORT).show();
		setProgressBarIndeterminateVisibility(false);
		setControllersActive(true);
		synchronized (m_iCounter) {
			m_iCounter = 0;
		}
	}

	private void setControllersActive(boolean b) {
		m_SignInGoogle.setEnabled(b);
		m_SignInDeveloper.setEnabled(b);
		m_SignInGuest.setEnabled(b);
	}

	private void performUserSpecificDataUpdate() {
		String uid = AccountManager.getCurrentAccount(getApplicationContext()).getUID();
		CacheManager.getManager(this).readCachedUserSpecificDataFromDatabase(uid);
		Intent i = new Intent(getApplicationContext() , RemoteDBService.class);
		i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.UpdateMyOffers);
		i.putExtra(RemoteDBService.PARAM_OBJECT, AccountManager.getCurrentAccount(getApplicationContext()).getUID());
		startService(i);
		i = new Intent(getApplicationContext(), RemoteDBService.class);
		i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.UpdateMyWishes);
		i.putExtra(RemoteDBService.PARAM_OBJECT, AccountManager.getCurrentAccount(getApplicationContext()).getUID());
		startService(i);
	}
/*
 	private void finishUpdating()
	{
		showProgress(false);
		unregisterReceiver(m_Reciever);
		gotoMainActivity();
	}
*/

	private void gotoActivity(Class activity) {
		setProgressBarIndeterminateVisibility(false);
		if (m_Reciever != null) {
			Log.d("LoginActivity","Unregistering");
			unregisterReceiver(m_Reciever);
		}
		Intent intent = new Intent(LoginActivity.this,
				activity);
		startActivity(intent);
		finish();
	}


	// additions to support google+ accounts
    /**
     * Method to resolve any signin errors
     * */
    void resolveSignInError() {
    	if (null != mConnectionResult) {
	        if (mConnectionResult.hasResolution()) {
	            try {
	                mIntentInProgress = true;
	                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
	            } catch (SendIntentException e) {
	                mIntentInProgress = false;
	                mGoogleApiClient.connect();
	            }
	        }
    	} else {
			mGoogleApiClient.connect();
    	}
    }
    @Override
	protected void onActivityResult(int requestCode, int responseCode,
    		Intent intent) {
    	if (RC_SIGN_IN == requestCode)
    	{
    		if (RESULT_OK == responseCode)
    		{
    			mGoogleApiClient.connect();
    		} else
    		{
    			setProgressBarIndeterminateVisibility(false);
    			setControllersActive(true);
    		}
    	}
    }

	private GoogleApiClient mGoogleApiClient;
	/**
     * A flag indicating that a PendingIntent is in progress and prevents us
     * from starting further intents.
     */
	private boolean mSignInClicked;
    private ConnectionResult mConnectionResult;
    private boolean mIntentInProgress;

    /**
     * Fetching user's information name, email, profile pic
     * */
    boolean updateProfileInformation() {
        try {
        	Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            if (currentPerson != null) {
            	AccountManager.setGoogleAccount(mGoogleApiClient, getApplicationContext());
            	UserAccount currentAccount = (UserAccount) AccountManager.getCurrentAccount(getApplicationContext());
                Toast.makeText(getApplicationContext(), "Welcome " + currentAccount.getName() , Toast.LENGTH_LONG).show();
                GcmFactory.getGcmAPI().registerDevice(new Communicator(), getApplication(), currentAccount.getUID(),"NotebookSwapper");
                return true;
            } else {
            	UserAccount currentAccount = (UserAccount) AccountManager.getCurrentAccount(getApplicationContext());
            	GcmFactory.getGcmAPI().registerDevice(new Communicator(), getApplication(), currentAccount.getUID(),"NotebookSwapper");
                Toast.makeText(getApplicationContext(),
                        "Person information is null", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
        	Toast.makeText(getApplicationContext(), "Identification error, please try again later" , Toast.LENGTH_LONG).show();
        }
        return false;
    }

    @Override
	public void onConnectionFailed(ConnectionResult result) {
    	Log.v("login activity", "onConnectionFailed called");
		if (!result.hasResolution()) {
			GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this, 0).show();
			handleLoginFail();
			return;
		}

		if (!mIntentInProgress) {
	        // Store the ConnectionResult for later usage
	        mConnectionResult = result;

	        if (mSignInClicked) {
	            // The user has already clicked 'sign-in' so we attempt to
	            // resolve all errors until the user is signed in, or they cancel.
	            resolveSignInError();
	        }
	    }
	}

    @Override
    public void onConnectionSuspended(int arg0) {
    	Log.v("login activity", "onConnectionSuspended called");
    	mGoogleApiClient.connect();
    }

    @Override
	public void onConnected(Bundle arg0) {
    	Log.v("login activity", "onConnected called");
    	if (m_signout)
    	{
    		Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
    		mGoogleApiClient.disconnect();
    		mGoogleApiClient.connect();
	        m_signout = false;
    	} else
    	{
    		afterConnectionEstablished();
    	}
	}

    private void afterConnectionEstablished() {
    	SharedPreferences prefs = getSharedPreferences(LoginActivity.class.getPackage().getName(), Context.MODE_PRIVATE);
		prefs.edit().putBoolean(LoginActivity.PREF_CONNECTED_GOOGLE, true).commit();
    	mSignInClicked = false;
	    // Get user's information
	    if (updateProfileInformation()) {
	    	registerUserOnServer();
	    	m_registeredToBroadcast = false;
			trackUserSpecificDataUpdate();
			performUserSpecificDataUpdate();
	    	// Lunch main activity iff identification succeeded
			//gotoActivity(MainActivity.class);
	    }
    }

}
