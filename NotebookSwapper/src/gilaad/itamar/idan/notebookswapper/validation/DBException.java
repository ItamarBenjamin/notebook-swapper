package gilaad.itamar.idan.notebookswapper.validation;

import gilaad.itamar.idan.notebookswapper.server.notebook.api.ReturnCodeNotebook;

public class DBException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = -2012063014891917367L;
	ReturnCodeNotebook m_Code;
	public DBException(ReturnCodeNotebook code)
	{
		m_Code = code;
	}

	@Override
	public String getMessage() {
		return m_Code.toString();
	}
}
