package gilaad.itamar.idan.notebookswapper.validation;

public class NotebookValidationException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = -8832235433699241444L;
	public String sErrorText;
	public NotebookValidationException(String error)
	{
		sErrorText = error;
	}

	@Override
	public String getMessage() {
		return sErrorText;
	}

}
