package gilaad.itamar.idan.notebookswapper.animation;

import gilaad.itamar.idan.notebookswapper.R;
import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.nhaarman.listviewanimations.swinginadapters.AnimationAdapter;
import com.nhaarman.listviewanimations.swinginadapters.prepared.AlphaInAnimationAdapter;


public class DefaultAnimation
{
	public static Animation getDefaultButtonAnimation(Context cotnext)
	{
		return AnimationUtils.loadAnimation(cotnext, R.anim.anim_pressing);
	}
	public static AnimationAdapter getDefaultListViewAnimationAdapter(BaseAdapter adapter, ListView view)
	{
		AlphaInAnimationAdapter alphaInAnimationAdapter = new AlphaInAnimationAdapter(adapter);
		alphaInAnimationAdapter.setInitialDelayMillis(200);
		alphaInAnimationAdapter.setAbsListView(view);
		return alphaInAnimationAdapter;
	}
}