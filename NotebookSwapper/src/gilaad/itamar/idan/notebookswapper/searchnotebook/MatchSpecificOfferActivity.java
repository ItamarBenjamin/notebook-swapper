package gilaad.itamar.idan.notebookswapper.searchnotebook;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.accounts.AccountManager;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.common.MenuItemSelected;
import gilaad.itamar.idan.notebookswapper.custom_view.InformativeRowCustomView;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.NotebookType;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IUserNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.WishlistItem;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService.RemoteDBServiceOperations;

import java.util.ArrayList;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MatchSpecificOfferActivity extends Activity {
	public static final String EXTRA_OFFER = "OFFER";

	private BroadcastReceiver m_CreateWishReceiver;
	private BroadcastReceiver m_CreateMessageReceiver;
	private BroadcastReceiver m_GetUserReceiver;

	private IOfferItem m_Offer;
	private IWishlistItem m_SuitableWish;
	private ICatalogItem m_CatalogItem;
	private ICourse m_Course;

	private Button m_AddButton;
	private InformativeRowCustomView m_irNotebookName;
	private InformativeRowCustomView m_irOfferedBy;
	private InformativeRowCustomView m_irStatus;
	private InformativeRowCustomView m_irCondition;
	private InformativeRowCustomView m_irOfferedFor;
	private InformativeRowCustomView m_irExtraInfo;
	private EditText m_Message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specific_offer);

		Intent intent = getIntent();
		m_Offer = (IOfferItem) intent.getSerializableExtra(EXTRA_OFFER);
		m_CatalogItem = CacheManager.getManager(this).getCatalogItem(m_Offer.getCatalogItemID());
		m_Course = CacheManager.getManager(this).getCourse(m_CatalogItem.getCourseNumber());

		findControllers();

		m_AddButton.setEnabled(false);

		findSuitableWish();
		setButtonTest();
		setData();
		initButton();

    }

	private void initButton() {
		m_AddButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				m_AddButton.setEnabled(false);
				m_Message.setEnabled(false);

				if (null == m_SuitableWish)
				{
					String uid = AccountManager.getCurrentAccount(getApplicationContext()).getUID();
					IWishlistItem data = new WishlistItem(uid,m_Course.getCourseNumber(),true,ItemState.REGULAR,true,m_CatalogItem.getType());
					Intent i = new Intent(MatchSpecificOfferActivity.this, RemoteDBService.class);
					i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.AddWish);
					i.putExtra(RemoteDBService.PARAM_OBJECT, data);
					startService(i);
				}
				else
				{
					sendMessage(m_SuitableWish);
				}
			}
		});

		m_Message.addTextChangedListener(new TextWatcher(){
	        @Override
			public void afterTextChanged(Editable s)
	        {
	        	m_AddButton.setEnabled(s.length() != 0);
	        }

	        @Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after){}
	        @Override
			public void onTextChanged(CharSequence s, int start, int before, int count){}
	    });
	}

	private void setData() {
		String sNotebook = m_Course.getCourseNumber() + " " +  m_Course.getCourseName() + " - " + m_CatalogItem.getType().toName();
		m_irNotebookName.setContent(sNotebook);
		String sTypeAndDiscription = "";
		if (m_Offer.isFree())
		{
			sTypeAndDiscription = getString(R.string.Giveaway);
		}
		else
		{
			sTypeAndDiscription = getString(R.string.Sale) + " - " + Integer.toString(m_Offer.getPrice()) + " " + getString(R.string.NewShekel) ;
		}
		if (m_Offer.isExchangable())
		{
			sTypeAndDiscription += ", " + getString(R.string.Exchange) ;
		}
		m_irOfferedFor.setContent(sTypeAndDiscription);
		m_irCondition.setContent(m_Offer.getCondition().toString());
		m_irExtraInfo.setContent(m_Offer.getAdditionalInfo());
		if (m_Offer.getState() == ItemState.REGULAR)
		{
			m_irStatus.setContent(getResources().getString(R.string.available));
		} else
		{
			m_irStatus.setContent(getResources().getString(R.string.removed));
		}
		setUser();
	}

    private void findControllers() {
    	m_irNotebookName = (InformativeRowCustomView) findViewById(R.id.irName);
		m_irOfferedBy = (InformativeRowCustomView) findViewById(R.id.irOfferedBy);
		m_irStatus = (InformativeRowCustomView) findViewById(R.id.irStatus);
		m_irCondition = (InformativeRowCustomView) findViewById(R.id.irCondition);
		m_irOfferedFor = (InformativeRowCustomView)findViewById(R.id.irOfferedFor);
		m_irExtraInfo = ((InformativeRowCustomView) findViewById(R.id.irExtraInfo));
    	m_AddButton = (Button)findViewById(R.id.btnAddAndSendMsg);
    	m_Message = (EditText)findViewById(R.id.editMsgContent);
	}

	private void setButtonTest() {
		if (null == m_SuitableWish)
		{
			m_AddButton.setText(getResources().getString(R.string.add_and_send));
		} else
		{
			m_AddButton.setText(getResources().getString(R.string.Send));
		}
	}

	private void findSuitableWish() {
    	ArrayList<IWishlistItem> myWishes = CacheManager.getManager(getApplicationContext()).getMyWishes();
		int offerCourseNumber = m_CatalogItem.getCourseNumber();
		NotebookType offerType = m_CatalogItem.getType();
		m_SuitableWish = null;
		for (IWishlistItem wishItem : myWishes) {
			if (wishItem.getCourseNumber() == offerCourseNumber && wishItem.getType() == offerType)
			{
				m_SuitableWish = wishItem;
				break;
			}
		}
	}

	private void setUser() {
    	IUserNotebook user = CacheManager.getManager(getApplicationContext()).getUserByID(m_Offer.getUserID());
		if (user != null)
		{
			m_irOfferedBy.setContent(user.getUsername());
		}
		else
		{
			Intent i = new Intent(MatchSpecificOfferActivity.this, RemoteDBService.class);
			i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.GetUser);
			i.putExtra(RemoteDBService.PARAM_OBJECT, m_Offer.getUserID());
			startService(i);
		}
	}

	protected void handleFail(String sError) {
		m_AddButton.setEnabled(true);
		m_Message.setEnabled(true);
		Toast.makeText(this, sError, Toast.LENGTH_SHORT).show();
	}

	protected void sendMessage(IWishlistItem wish)
	{
		String text = ((EditText)findViewById(R.id.editMsgContent)).getText().toString();
		Message msg = new Message(m_Offer.getOfferID(), wish.getWishID(), text, wish.getUserID(), m_Offer.getUserID(), false);
		Intent i = new Intent(MatchSpecificOfferActivity.this, RemoteDBService.class);
		i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.SendMessage);
		i.putExtra(RemoteDBService.PARAM_OBJECT, msg);
		startService(i);
	}

	@Override
	protected void onResume() {
		m_GetUserReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				Log.v("MatchSpecificOffer", "Got user update");
				setUser();
			}
		};
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(RemoteDBServiceOperations.GetUser.toString());
		registerReceiver(m_GetUserReceiver, intentFilter);

        m_CreateWishReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.v("WishMessageList", "Got wish messages update");
				IWishlistItem wish = (IWishlistItem)intent.getSerializableExtra(RemoteDBService.PARAM_OBJECT);
				if (wish == null)
				{
					handleFail("Failed to create new wish");
				}
				else
				{
					sendMessage(wish);
				}
			}
		};
		intentFilter = new IntentFilter();
		intentFilter.addAction(RemoteDBServiceOperations.AddWish.toString());
		registerReceiver(m_CreateWishReceiver, intentFilter);

		m_CreateMessageReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.v("WishMessageList", "Got wish messages update");
				Boolean bError = intent.getBooleanExtra(RemoteDBService.PARAM_ERROR,false);
				if (bError)
				{
					handleFail("Failed to send message");
				}
				else
				{
					finish();
				}
			}
		};
		intentFilter = new IntentFilter();
		intentFilter.addAction(RemoteDBServiceOperations.SendMessage.toString());
		registerReceiver(m_CreateMessageReceiver, intentFilter);
		super.onResume();
	}

    @Override
    protected void onPause() {
		unregisterReceiver(m_CreateWishReceiver);
		unregisterReceiver(m_CreateMessageReceiver);
		unregisterReceiver(m_GetUserReceiver);
		super.onPause();
    }

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.match_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
		if (MenuItemSelected.commonOnOptionsItemSelected(this, item)) {
			return true;
		}
		if (id == android.R.id.home)
		{
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
		}
		return super.onOptionsItemSelected(item);
    }


}
