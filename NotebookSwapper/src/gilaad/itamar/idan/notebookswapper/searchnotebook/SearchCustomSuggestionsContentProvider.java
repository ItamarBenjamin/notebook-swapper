package gilaad.itamar.idan.notebookswapper.searchnotebook;

import gilaad.itamar.idan.notebookswapper.cache.LocalCacheSQLiteOpenHelper;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.NotebookType;
import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;

public class SearchCustomSuggestionsContentProvider extends ContentProvider {
	// fields for my content provider
	     static final String PROVIDER_NAME = "gilaad.itamar.idan.notebookswapper.searchnotebook";
	     static final String URL = "content://" + PROVIDER_NAME + "/search";
	     static final Uri CONTENT_URI = Uri.parse(URL);

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		String userInput = selectionArgs[0];
		String where = "";
		if (null == userInput || userInput.isEmpty())
		{
			return null;
		} else
		{
			String[] words = userInput.split(" +");
			boolean first = true;
			for (String word : words)
			{
				if (word.matches("[0-9]+"))
				{
					where += (first ? "" : " AND ") + "Catalog.CourseNumber LIKE '" + word + "%'";
					first = false;
				} else if (word.matches("[a-zA-Zא-ת0-9]+"))
				{
					where += (first ? "" : " AND ") + "Courses.CourseName LIKE '" + word + "%'";
					first = false;
				}
			}
		}
		SQLiteDatabase readableDatabase = LocalCacheSQLiteOpenHelper.getInstance().getReadableDatabase();
		//String select = String.format("SELECT DISTINCT Catalog.ItemID AS %s, Catalog.ItemID AS %s, Courses.CourseName AS %s, (Courses.CourseNumber || \" - \" || (%s)) AS %s ", BaseColumns._ID,SearchManager.SUGGEST_COLUMN_INTENT_DATA,SearchManager.SUGGEST_COLUMN_TEXT_1,getTypeQuery(),SearchManager.SUGGEST_COLUMN_TEXT_2);
		String select = String.format("SELECT DISTINCT Catalog.CourseNumber AS %s, Catalog.CourseNumber AS %s, Courses.CourseName AS %s, Catalog.CourseNumber AS %s", BaseColumns._ID,SearchManager.SUGGEST_COLUMN_INTENT_DATA,SearchManager.SUGGEST_COLUMN_TEXT_1,SearchManager.SUGGEST_COLUMN_TEXT_2);
		String fromjoin = " FROM Catalog JOIN Courses ON Courses.courseNumber = Catalog.CourseNumber ";
		String query = select + fromjoin + (where.isEmpty() ? "" : "WHERE " + where);
		return readableDatabase.rawQuery(query, null);
	}

	private Object getTypeQuery() {
		String ret = "SELECT CASE Catalog.Type ";
		for (NotebookType $ : NotebookType.values())
		{
			ret += String.format("WHEN '%s' THEN '%s' ",$.toString(),$.toName());
		}
		ret += "ELSE 'אחר' END";
		return ret;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}
}
