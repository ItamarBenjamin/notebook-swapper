package gilaad.itamar.idan.notebookswapper.searchnotebook;

import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;

import java.io.Serializable;

public class NotebookItem implements ISearchItem, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1744660418791395650L;
	private final ICatalogItem m_item;
	private final ICourse m_course;

	public NotebookItem(ICatalogItem item,ICourse course) {
		m_item = item;
		m_course = course;
	}

	public ICatalogItem getCatalogItem()
	{
		return m_item;
	}

	public ICourse getCourse()
	{
		return m_course;
	}
}
