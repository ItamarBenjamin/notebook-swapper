package gilaad.itamar.idan.notebookswapper.searchnotebook;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.accounts.AccountManager;
import gilaad.itamar.idan.notebookswapper.adapters.SearchCatalogAdapter;
import gilaad.itamar.idan.notebookswapper.animation.DefaultAnimation;
import gilaad.itamar.idan.notebookswapper.common.MenuItemSelected;
import android.app.ActionBar;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SearchView;

import com.nhaarman.listviewanimations.swinginadapters.AnimationAdapter;

public class SearchNotebookActivity extends Activity implements IMatchCatalogListener {

	private final String QUERY = "QUERY";
    private ListView lvMatchingNotebooks;
    private SearchCatalogAdapter m_Adapter;
    String m_sQuery;
    boolean m_frombundle;
	private AnimationAdapter m_AnimationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        m_frombundle = false;
        if (savedInstanceState != null)
        {
        	m_sQuery = savedInstanceState.getString(QUERY);
        	m_frombundle = true;
        }

        setContentView(R.layout.activity_search_results);
		ActionBar actionBar = getActionBar();
		if (AccountManager.getCurrentAccount(getApplicationContext()).isGuest()) {
			actionBar.setDisplayHomeAsUpEnabled(false);
		} else {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}

		lvMatchingNotebooks = (ListView) findViewById(R.id.matchingNotebooks);
		m_Adapter = new SearchCatalogAdapter(this);
		m_AnimationAdapter = DefaultAnimation.getDefaultListViewAnimationAdapter(m_Adapter, lvMatchingNotebooks);
        lvMatchingNotebooks.setAdapter(m_AnimationAdapter);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	outState.putString(QUERY, m_sQuery);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
    	if (AccountManager.getCurrentAccount(getApplicationContext()).isGuest()) {
    		getMenuInflater().inflate(R.menu.guest_menu, menu);
    	} else {
    		getMenuInflater().inflate(R.menu.main, menu);
    	}
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
				.getActionView();
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id)
        {
        	case android.R.id.home:
        		NavUtils.navigateUpFromSameTask(this);
        		return true;
        }
		if (MenuItemSelected.commonOnOptionsItemSelected(this, item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
    }

	@Override
	public void onMatchCatalog(NotebookItem item) {
		Intent intent = new Intent(this,MatchNotebookActivity.class);
		intent.putExtra(MatchNotebookActivity.EXTRA_NOTEBOOK, item);
		startActivity(intent);
	}

	@Override
	protected void onNewIntent(Intent intent) {
	    setIntent(intent);
	    handleIntent(intent);
	}

	private void handleIntent(Intent intent) {
	    if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
	      m_sQuery = intent.getStringExtra(SearchManager.QUERY);
	      doSearch();
	    }
	}

	@Override
	protected void onResume() {
		super.onResume();
		Intent intent = getIntent();
		if (!m_frombundle)
		{
	    	if (intent.hasExtra(SearchManager.QUERY))
	    	{
	    		m_sQuery = getIntent().getStringExtra(SearchManager.QUERY);
	    	} else if (intent.getAction() != null && intent.getAction().equalsIgnoreCase(intent.ACTION_VIEW))
	    	{
	    		m_sQuery = intent.getDataString();
	    	} else
	    	{
	    		return;
	    	}
		}
    	doSearch();
	}

	private void doSearch() {
		NotebookItem notebookItem = m_Adapter.setQuery(m_sQuery);
    	if (null == notebookItem)
    	{
    		m_AnimationAdapter.notifyDataSetChanged();
    	} else
    	{
    		onMatchCatalog(notebookItem);
    	}
	}
}
