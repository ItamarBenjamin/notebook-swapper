package gilaad.itamar.idan.notebookswapper.searchnotebook;

public class MatchOfferSection implements IMatchOfferItem{

	private final String m_sTitle;

	public MatchOfferSection(String sTitle)
	{
		m_sTitle = sTitle;
	}

	public String getTitle()
	{
		return m_sTitle;
	}

}
