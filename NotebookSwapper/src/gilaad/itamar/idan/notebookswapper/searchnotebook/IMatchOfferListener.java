package gilaad.itamar.idan.notebookswapper.searchnotebook;

import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;

public interface IMatchOfferListener {
	public void onMatchOffer(IOfferItem offerItem);
}
