package gilaad.itamar.idan.notebookswapper.searchnotebook;

public class SectionItem implements ISearchItem{

	private final String m_sHeader;

	public SectionItem(String header) {
		m_sHeader = header;
	}

	public String getHeader()
	{
		return m_sHeader;
	}
}
