package gilaad.itamar.idan.notebookswapper.searchnotebook;

import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;

public class MatchOfferItem implements IMatchOfferItem{
	private final IOfferItem m_offerItem;
	private final boolean m_bShowPrice;

	public MatchOfferItem(IOfferItem item,boolean bShowPrice)
	{
		m_offerItem = item;
		m_bShowPrice = bShowPrice;
	}

	public IOfferItem getOfferItem()
	{
		return m_offerItem;
	}

	public boolean showPrice()
	{
		return m_bShowPrice;
	}
}
