package gilaad.itamar.idan.notebookswapper.searchnotebook;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.accounts.AccountManager;
import gilaad.itamar.idan.notebookswapper.adapters.MatchingOffersListAdapter;
import gilaad.itamar.idan.notebookswapper.animation.DefaultAnimation;
import gilaad.itamar.idan.notebookswapper.asynctasks.GetOffersByCatIDAsyncTask;
import gilaad.itamar.idan.notebookswapper.common.MenuItemSelected;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.wishactivity.AddWishActivity;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.widget.ListView;
import android.widget.TextView;

import com.nhaarman.listviewanimations.swinginadapters.AnimationAdapter;

public class MatchNotebookActivity extends RefreshedListViewActivity implements IMatchOfferListener{

	public static final String EXTRA_NOTEBOOK = "NOTEBOOK";
	private ListView lstView;
	private boolean m_isGuest = false;
	private TextView tvLookingForBooks;
	private MatchingOffersListAdapter m_matchingOffersListAdapter;
	private NotebookItem m_lNoteboook;
	private TextView tvAddme;
	private AnimationAdapter m_AnimationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_matching);

        findControllers();

        m_isGuest = AccountManager.getCurrentAccount(getApplicationContext()).isGuest();
		Intent intent = getIntent();
		m_lNoteboook = (NotebookItem) intent.getSerializableExtra(EXTRA_NOTEBOOK);

		m_matchingOffersListAdapter = new MatchingOffersListAdapter(this,null,!m_isGuest);
		m_AnimationAdapter = DefaultAnimation.getDefaultListViewAnimationAdapter(m_matchingOffersListAdapter, lstView);
		lstView.setAdapter(m_AnimationAdapter);

		if (m_isGuest)
		{
			tvAddme.setVisibility(View.GONE);
		}
		else
		{
			tvAddme.setVisibility(View.VISIBLE);
			ObjectAnimator animation = ObjectAnimator.ofFloat(tvAddme, "rotation", -5f, 5f);
			animation.setDuration(1200);
			animation.setRepeatCount(ObjectAnimator.INFINITE);
			animation.setRepeatMode(ObjectAnimator.REVERSE);
			animation.setInterpolator(new AccelerateDecelerateInterpolator());
			animation.start();

			tvAddme.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					final Animation animPress = DefaultAnimation.getDefaultButtonAnimation(MatchNotebookActivity.this);
					v.startAnimation(animPress);
					Intent intent = new Intent(MatchNotebookActivity.this,AddWishActivity.class);
					intent.putExtra(AddWishActivity.NOTEBOOK, m_lNoteboook);
					MatchNotebookActivity.this.startActivity(intent);
				}
			});
		}
		showNotebookDetails();
    }

	private void findControllers() {
		lstView = (ListView)findViewById(R.id.lstMatchingBookOffers);
		tvLookingForBooks = (TextView) findViewById(R.id.txtNotAvailable);
		tvAddme = (TextView) findViewById(R.id.txtAddMe);
	}

    private void showNotebookDetails() {
    	TextView tvCourseNum = (TextView) findViewById(R.id.txtCourseNumber);
    	TextView tvTitle = (TextView) findViewById(R.id.txtTitle);
    	TextView tvEdition = (TextView) findViewById(R.id.txtCourseEdition);
    	tvCourseNum.setText(Integer.toString(m_lNoteboook.getCourse().getCourseNumber()));
    	tvTitle.setText(m_lNoteboook.getCourse().getCourseName()+ " - " + m_lNoteboook.getCatalogItem().getType().toName());
    	tvEdition.setText(m_lNoteboook.getCatalogItem().getEdition());
    }

	@Override
    protected void onResume() {
		String uid = AccountManager.getCurrentAccount(getApplicationContext()).getUID();
    	new GetOffersByCatIDAsyncTask(m_matchingOffersListAdapter, m_lNoteboook.getCatalogItem().getCatalogItemID(),true,true,uid,this,tvLookingForBooks).execute(new Void[0]);
    	super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.match_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id)
        {
        	case android.R.id.home:
        		NavUtils.navigateUpFromSameTask(this);
        		return true;
        }
		if (MenuItemSelected.commonOnOptionsItemSelected(this, item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
    }

	@Override
	public void onMatchOffer(IOfferItem offerItem) {
		Intent intent = new Intent(this,MatchSpecificOfferActivity.class);
		intent.putExtra(MatchSpecificOfferActivity.EXTRA_OFFER, offerItem);
		startActivity(intent);
	}

	@Override
	public void refreshList() {
		m_AnimationAdapter.notifyDataSetChanged();
	}

}
