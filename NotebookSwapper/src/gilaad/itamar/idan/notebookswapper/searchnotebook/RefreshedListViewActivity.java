package gilaad.itamar.idan.notebookswapper.searchnotebook;

import android.app.Activity;

public abstract class RefreshedListViewActivity extends Activity{
	public abstract void refreshList();
}
