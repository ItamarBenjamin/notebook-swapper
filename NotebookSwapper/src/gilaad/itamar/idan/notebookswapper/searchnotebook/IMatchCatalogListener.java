package gilaad.itamar.idan.notebookswapper.searchnotebook;

public interface IMatchCatalogListener {
	public void onMatchCatalog(NotebookItem item);
}
