package gilaad.itamar.idan.notebookswapper.asynctasks;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.accounts.DeveloperAccount;
import gilaad.itamar.idan.notebookswapper.adapters.MatchingOffersListAdapter;
import gilaad.itamar.idan.notebookswapper.dbhandler.RemoteDBDataManager;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.searchnotebook.IMatchOfferItem;
import gilaad.itamar.idan.notebookswapper.searchnotebook.MatchOfferItem;
import gilaad.itamar.idan.notebookswapper.searchnotebook.MatchOfferSection;
import gilaad.itamar.idan.notebookswapper.searchnotebook.RefreshedListViewActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class GetOffersByCatIDAsyncTask extends AsyncTask<Void, Void, Void> {

	public static String EXCHANGE;
	public static String GIVEAWAY;
	public static String SALE;

	MatchingOffersListAdapter m_Adapter;
	RefreshedListViewActivity m_Activity;
	long m_CatalogID;
	private final boolean m_bShowSale;
	private final boolean m_bShowExchange;
	private final String m_sUserID;
	private final TextView m_tvWaiting;
	private IMatchOfferItem[] m_Results;

	public GetOffersByCatIDAsyncTask(MatchingOffersListAdapter matchingOffersAdapter,long catID,boolean bShowSale,
			boolean bShowExchange,String sUserID, RefreshedListViewActivity activity, TextView tv)
	{
		m_Adapter = matchingOffersAdapter;
		m_CatalogID = catID;
		m_bShowSale = bShowSale;
		m_bShowExchange = bShowExchange;
		m_sUserID = sUserID;
		m_Activity = activity;
		m_tvWaiting = tv;
	}

	@Override
    protected void onPreExecute() {
      super.onPreExecute();
      m_Activity.setProgressBarIndeterminateVisibility(true);
      m_tvWaiting.setVisibility(View.VISIBLE);
      m_tvWaiting.setText(m_Activity.getResources().getString(R.string.LookingForBooks));
    }

    @Override
    protected Void doInBackground(Void... a) {

      Log.v("ATask:", "Get Offers for catalog item " + String.valueOf(m_CatalogID));
      List<Long> lst = new ArrayList<Long>();
      lst.add(m_CatalogID);
      List<IOfferItem> lstResult = (new RemoteDBDataManager()).getOffersByCatalogID(lst,0L);

      Map<String,List<MatchOfferItem>> mapTypeToItems = new HashMap<String, List<MatchOfferItem>>();
      mapTypeToItems.put(EXCHANGE, new ArrayList<MatchOfferItem>());
      mapTypeToItems.put(SALE, new ArrayList<MatchOfferItem>());
      mapTypeToItems.put(GIVEAWAY, new ArrayList<MatchOfferItem>());
      for (IOfferItem offerItem : lstResult)
      {
    	  if (offerItem.getUserID().equalsIgnoreCase(m_sUserID) && !m_sUserID.equalsIgnoreCase(DeveloperAccount.id)) {
    		  continue;
    	  }
    	  if (!offerItem.getState().equals(ItemState.REGULAR))
    	  {
    		  continue;
    	  }
    	  if (offerItem.isExchangable() && m_bShowExchange)
    	  {
			mapTypeToItems.get(EXCHANGE).add(new MatchOfferItem(offerItem, false));
    	  }
    	  if (offerItem.isFree())
    	  {
    		  mapTypeToItems.get(GIVEAWAY).add(new MatchOfferItem(offerItem, false));
    	  }else{
    		  if (m_bShowSale)
    		  {
    			  mapTypeToItems.get(SALE).add(new MatchOfferItem(offerItem, true));
    		  }
    	  }
      }

      List<IMatchOfferItem> lstMatchOfferItems = new ArrayList<IMatchOfferItem>();
		for (Entry<String, List<MatchOfferItem>> ent : mapTypeToItems.entrySet()) {
			if (ent.getValue().isEmpty()) {
				continue;
			}
			lstMatchOfferItems.add(new MatchOfferSection(ent.getKey()));
			for (MatchOfferItem offer : ent.getValue()) {
				lstMatchOfferItems.add(offer);
			}
		}

		m_Results = lstMatchOfferItems.toArray(new IMatchOfferItem[lstMatchOfferItems.size()]);
		m_Adapter.setData(m_Results);
		return null;
    }

    @Override
    protected void onPostExecute(Void a) {
    	m_Activity.refreshList();
    	m_Activity.setProgressBarIndeterminateVisibility(false);
    	if (m_Results.length == 0) {
    		m_tvWaiting.setText(m_Activity.getResources().getString(R.string.didnt_find_notebooks));
		} else {
			m_tvWaiting.setVisibility(View.GONE);
		}
    }
}
