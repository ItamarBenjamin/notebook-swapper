package gilaad.itamar.idan.notebookswapper.asynctasks;

import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.image.ImageActivity;
import gilaad.itamar.idan.notebookswapper.validation.DBException;

import java.io.InputStream;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage;
    Context m_Context;
    String m_URL;
    boolean m_Clickable;

    public DownloadImageTask(ImageView bmImage, Context context, boolean setClickHandler) {
        this.bmImage = bmImage;
        m_Context = context;
        m_Clickable = setClickHandler;
    }

    @Override
	protected Bitmap doInBackground(String... urls) {
        m_URL = urls[0];
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(m_URL).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
        	return null;
            //Log.e("Error", e.getMessage());
           // e.printStackTrace();
        }
        return mIcon11;
    }

    @Override
	protected void onPostExecute(final Bitmap result) {
    	if (result == null)
    	{
    		return;
    	}
    	if (bmImage != null)
    	{
			bmImage.setImageBitmap(result);
		}
        try {
			CacheManager.getManager(m_Context).addImage(m_URL, result);
		} catch (DBException e) {
			e.printStackTrace();
			return;
		}
        if (m_Clickable)
        {
	        bmImage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(m_Context,ImageActivity.class);
					intent.putExtra(ImageActivity.EXTRA_IMAGE, result);
					m_Context.startActivity(intent);
				}
			});
        }
    }
}