package gilaad.itamar.idan.notebookswapper.asynctasks;

import android.graphics.Bitmap;

public interface IGetFromURLS {
	void onGetURLS(Bitmap bitmap);
}
