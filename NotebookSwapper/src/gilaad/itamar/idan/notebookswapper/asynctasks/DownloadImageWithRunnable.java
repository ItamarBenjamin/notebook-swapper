package gilaad.itamar.idan.notebookswapper.asynctasks;

import java.io.InputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

public class DownloadImageWithRunnable extends AsyncTask<String, Void, Bitmap> {

    Context m_Context;
	private final IGetFromURLS runWhenFinish;

    public DownloadImageWithRunnable(IGetFromURLS runWhenFinish, Context context) {
        this.runWhenFinish = runWhenFinish;
		m_Context = context;
    }

    @Override
	protected Bitmap doInBackground(String... urls) {

        try {
            InputStream in = new java.net.URL(urls[0]).openStream();
            Bitmap b = BitmapFactory.decodeStream(in);
            Log.v("IMGGetter", "Found img for url " + urls[0]);
            return b;
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    @Override
	protected void onPostExecute(final Bitmap result) {
    	runWhenFinish.onGetURLS(result);
    }
}