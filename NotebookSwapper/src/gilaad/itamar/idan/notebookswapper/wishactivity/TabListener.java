package gilaad.itamar.idan.notebookswapper.wishactivity;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Fragment;
import android.app.FragmentTransaction;

public class TabListener<T extends Fragment> implements ActionBar.TabListener {
    private AbstractWishFragment mFragment;
    private final WishActivity mActivity;
    private final String mTag;
    private final Class<T> mClass;
    IWishlistItem m_item;

    /** Constructor used each time a new tab is created.
      * @param activity  The host Activity, used to instantiate the fragment
      * @param tag  The identifier tag for the fragment
      * @param clz  The fragment's Class, used to instantiate the fragment
      */
    public TabListener(WishActivity activity, String tag, Class<T> clz, IWishlistItem item) {
        mActivity = activity;
        mTag = tag;
        mClass = clz;
        m_item = item;
    }

    /* The following are each of the ActionBar.TabListener callbacks */

    @Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
    	if(tab.getPosition() > mActivity.m_iSelectedTab)
    	{
    		ft.setCustomAnimations(R.anim.slidein_left, R.anim.slideout_left);
    	}
		// else slide in from right
		else
		{
		     ft.setCustomAnimations(R.anim.slidein_right, R.anim.slideout_right);
		}

    	ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        mActivity.m_iSelectedTab = tab.getPosition();
        // Check if the fragment is already initialized
        if (mFragment == null) {
            // If not, instantiate and add it to the activity
        	mFragment = (AbstractWishFragment) Fragment.instantiate(mActivity, mClass.getName());
        	mFragment.setWishItem(m_item);
            ft.add(android.R.id.content, mFragment, mTag);
        } else {
            // If it exists, simply attach it in order to show it
            ft.attach(mFragment);
        }

    }

    @Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
        if (mFragment != null) {
            // Detach the fragment, because another one is being attached
            ft.detach(mFragment);
        }
    }

    @Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
        // User selected the already selected tab. Usually do nothing.
    }

	public void setWishItem(IWishlistItem wishItem) {
		m_item = wishItem;
		if (null != mFragment)
		{
			mFragment.setWishItem(wishItem);
		}
	}

	public void refreshList()
	{
		mFragment.refreshList();
	}
}
