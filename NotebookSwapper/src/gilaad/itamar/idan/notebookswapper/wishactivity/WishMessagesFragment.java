package gilaad.itamar.idan.notebookswapper.wishactivity;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.adapters.WishMessagesListAdapter;
import gilaad.itamar.idan.notebookswapper.animation.DefaultAnimation;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService.RemoteDBServiceOperations;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.nhaarman.listviewanimations.swinginadapters.AnimationAdapter;

public class WishMessagesFragment extends AbstractWishFragment
{

	private ListView m_lstMessages;
	private AnimationAdapter m_oAdapter;
	private BroadcastReceiver m_WishMessegesReceiver = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.wish_generic_fragment, container, false);
		View checkboxes = inflater.inflate(R.layout.wish_checkboxes, container, false);
		setCommonInfo(rootView,checkboxes);

		TextView title = (TextView) rootView.findViewById(R.id.txtSpecificTitle);
		title.setText(getResources().getString(R.string.wish_messages_tab_title));
		m_lstMessages = ((ListView)rootView.findViewById(R.id.lstGeneric));
		m_oAdapter = DefaultAnimation.getDefaultListViewAnimationAdapter(new WishMessagesListAdapter(getActivity(),m_lstMessages,getWishlistItem().getWishID()),m_lstMessages);
		m_lstMessages.setAdapter(m_oAdapter);
		m_oAdapter.notifyDataSetChanged();
		return rootView;
	}

	@Override
	public void onPause() {
		if (m_WishMessegesReceiver != null) {
			getActivity().unregisterReceiver(m_WishMessegesReceiver);
		}
		super.onPause();
	}

	@Override
	public void onDestroyView() {
		m_WishMessegesReceiver = null;
		m_oAdapter = null;
		m_lstMessages = null;
		super.onDestroyView();
	}

	@Override
	protected void onResumeSpecific() {
		m_WishMessegesReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.v("WishMessageList", "Got wish messages update");
				m_oAdapter.notifyDataSetChanged();
			}
		};
		m_oAdapter.notifyDataSetChanged();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(RemoteDBServiceOperations.UpdateMyMessages.toString());
		intentFilter.addAction(RemoteDBServiceOperations.GetGCMMessage.toString());
		getActivity().registerReceiver(m_WishMessegesReceiver, intentFilter);
	}

	@Override
	public void refreshList() {
		m_oAdapter.notifyDataSetChanged();
	}



}
