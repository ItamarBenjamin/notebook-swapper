package gilaad.itamar.idan.notebookswapper.wishactivity;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.accounts.AccountManager;
import gilaad.itamar.idan.notebookswapper.adapters.FullMessagesListAdapter;
import gilaad.itamar.idan.notebookswapper.animation.DefaultAnimation;
import gilaad.itamar.idan.notebookswapper.asynctasks.DownloadImageWithRunnable;
import gilaad.itamar.idan.notebookswapper.asynctasks.IGetFromURLS;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService.RemoteDBServiceOperations;
import gilaad.itamar.idan.notebookswapper.validation.DBException;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nhaarman.listviewanimations.swinginadapters.AnimationAdapter;

public class WishFullMessagesActivity extends Activity {
	private ListView m_lstMessages;
	private FullMessagesListAdapter m_oAdapter;
	private BroadcastReceiver m_WishMessegesReceiver;
	private Button m_btnSendMsg;
	private EditText m_editText;
	private BroadcastReceiver m_WishMessegesSendReceiver;
	private TextView m_txtCourseTitle;
	private TextView m_txtOfferInfo;
	public static final String WISHID = "WISHID";
	public static final String OFFERID = "OFFERID";
	private static final String MY_IMAGE = "MYIMAGE";
	private static final String OTHER_IMAGE = "OTHERIMAGE";
	private Bitmap m_BitmapMe = null;
	private Bitmap m_BitmapOther = null;
	private String sMe;
	private String sOtherUser;
	private AnimationAdapter m_AnimationAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        final Long wishId = getIntent().getLongExtra(WISHID, -1);
        final Long OfferId = getIntent().getLongExtra(OFFERID, -1);

        findControllers();

    	m_btnSendMsg.setEnabled(false);
        sMe = AccountManager.getCurrentAccount(getApplicationContext()).getUID();
        sOtherUser = CacheManager.getManager(getApplicationContext()).getReleventOffer(OfferId).getUserID();

        setOfferInfo(OfferId);

        if (null != savedInstanceState)
        {
        	m_BitmapMe = savedInstanceState.getParcelable(MY_IMAGE);
        	m_BitmapOther = savedInstanceState.getParcelable(OTHER_IMAGE);
        }
        m_oAdapter = new FullMessagesListAdapter(this,wishId,OfferId,true,m_BitmapMe,m_BitmapOther);
        m_AnimationAdapter = DefaultAnimation.getDefaultListViewAnimationAdapter(m_oAdapter, m_lstMessages);
        if (m_BitmapOther == null)
        {
        	getOtherImage();
        }
        if (null == m_BitmapMe)
        {
        	getMyImage();
        }

		m_lstMessages.setAdapter(m_AnimationAdapter);
		m_AnimationAdapter.notifyDataSetChanged();

		addListeners(wishId, OfferId);
	}

	private void addListeners(final Long wishId, final Long OfferId) {
		m_editText.addTextChangedListener(new TextWatcher(){
	        @Override
			public void afterTextChanged(Editable s)
	        {
	        	m_btnSendMsg.setEnabled(s.length() != 0);
	        }

	        @Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after){}
	        @Override
			public void onTextChanged(CharSequence s, int start, int before, int count){}
	    });
		m_btnSendMsg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				m_editText.setEnabled(false);
				m_btnSendMsg.setEnabled(false);
				String text = m_editText.getText().toString();
				Message msg = new Message(OfferId, wishId, text, sMe, sOtherUser, false);
				Intent i = new Intent(WishFullMessagesActivity.this, RemoteDBService.class);
				i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.SendMessage);
				i.putExtra(RemoteDBService.PARAM_OBJECT, msg);
				startService(i);
			}
		});
	}

	private void findControllers()
	{
		m_lstMessages = (ListView)findViewById(R.id.lstAllMessagesFull);
		m_btnSendMsg = (Button)findViewById(R.id.btButtonSendMessage);
		m_editText = (EditText)findViewById(R.id.etEdittext);
	}

	private void getOtherImage() {
    	CacheManager manager = CacheManager.getManager(getApplicationContext());
    	final String sOtherURL = manager.getUserByID(sOtherUser).getImgUrl();
    	m_BitmapOther = CacheManager.getManager(getApplicationContext()).getProfileImage(sOtherURL);
    	if (m_BitmapOther != null)
    	{
    		m_oAdapter.setOtherImg(m_BitmapOther);
    		m_AnimationAdapter.notifyDataSetChanged();
    		return;
    	}

		String[] urls = {sOtherURL};
		new DownloadImageWithRunnable(new IGetFromURLS() {

			@Override
			public void onGetURLS(Bitmap bitmap) {
				if(null!=bitmap)
				{
					m_BitmapOther = bitmap;
					m_oAdapter.setOtherImg(bitmap);
					m_AnimationAdapter.notifyDataSetChanged();
					try {
						CacheManager.getManager(getApplicationContext()).addProfileImage(sOtherURL, bitmap);
					} catch (DBException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		},this).execute(urls);
	}

	private void getMyImage() {

    	CacheManager manager = CacheManager.getManager(getApplicationContext());
    	final String sMyURL = manager.getUserByID(sMe).getImgUrl();
    	m_BitmapMe = CacheManager.getManager(getApplicationContext()).getProfileImage(sMyURL);
    	if (m_BitmapMe != null)
    	{
    		m_oAdapter.setMyImg(m_BitmapMe);
    		m_AnimationAdapter.notifyDataSetChanged();
    		return;
    	}

		String[] urls = {sMyURL};
		new DownloadImageWithRunnable(new IGetFromURLS() {

			@Override
			public void onGetURLS(Bitmap bitmap) {
				if(null!=bitmap)
				{
					m_BitmapMe = bitmap;
					m_oAdapter.setMyImg(bitmap);
					m_AnimationAdapter.notifyDataSetChanged();
					try {
						CacheManager.getManager(getApplicationContext()).addProfileImage(sMyURL, bitmap);
					} catch (DBException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		},this).execute(urls);
	}


	private void setOfferInfo(Long offerId) {
		m_txtCourseTitle = (TextView)findViewById(R.id.txtMessageCourseTitle);
		IOfferItem offer = CacheManager.getManager(getApplicationContext()).getReleventOffer(offerId);
		Long catalogItemID = offer.getCatalogItemID();
		ICatalogItem catalogItem = CacheManager.getManager(getApplicationContext()).getCatalogItem(catalogItemID);
		ICourse course = CacheManager.getManager(getApplicationContext()).getCourse(catalogItem.getCourseNumber());
		String sTitle = course.getCourseNumber() + " " +  course.getCourseName() + " - " + catalogItem.getType().toName();
		m_txtCourseTitle.setText(sTitle);

		m_txtOfferInfo = (TextView)findViewById(R.id.txtMessageCourseOfferInfo);
		String sOfferInfo = "";
		if (offer.isFree())
		{
			sOfferInfo = getString(R.string.Giveaway);
		}
		else
		{
			sOfferInfo = getString(R.string.Sale) + ": " + Integer.toString(offer.getPrice()) + " " + getString(R.string.NewShekel) ;
		}
		if (offer.isExchangable())
		{
			if (!sOfferInfo.isEmpty())
			{
				sOfferInfo = "/" + sOfferInfo;
			}
			sOfferInfo = getString(R.string.Exchange) + sOfferInfo;
		}
		m_txtOfferInfo.setText(sOfferInfo);
	}


	private void handleFail(String sError) {
		Toast.makeText(this, sError, Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onResume() {
		m_WishMessegesReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.v("WishFullMessageList", "Got wish messages update");
				m_AnimationAdapter.notifyDataSetChanged();
				m_lstMessages.post(new Runnable() {
			        @Override
			        public void run() {
			        	m_lstMessages.setSelection(m_lstMessages.getCount());
			        }
			    });
			}
		};
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(RemoteDBServiceOperations.UpdateMyMessages.toString());
		intentFilter.addAction(RemoteDBServiceOperations.GetGCMMessage.toString());
		registerReceiver(m_WishMessegesReceiver, intentFilter);
		m_WishMessegesSendReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.v("WishFullMessagesActivity", "Wish message was sent");
				Boolean bError = intent.getBooleanExtra(RemoteDBService.PARAM_ERROR,false);
				if (bError)
				{
					handleFail("Failed to send message");
				}
				else
				{
					m_AnimationAdapter.notifyDataSetChanged();
					m_editText.setText("");
					m_lstMessages.post(new Runnable() {
				        @Override
				        public void run() {
				        	m_lstMessages.setSelection(m_lstMessages.getCount());
				        }
				    });
				}
				m_editText.setEnabled(true);
				m_btnSendMsg.setEnabled(false);
			}
		};
		intentFilter = new IntentFilter();
		intentFilter.addAction(RemoteDBServiceOperations.SendMessage.toString());
		registerReceiver(m_WishMessegesSendReceiver, intentFilter);
		super.onResume();
	}

	@Override
	public void onPause() {
		unregisterReceiver(m_WishMessegesReceiver);
		unregisterReceiver(m_WishMessegesSendReceiver);
		m_WishMessegesReceiver = null;
		m_WishMessegesSendReceiver = null;
		m_oAdapter = null;
		m_lstMessages = null;
		super.onPause();
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id)
        {
        	case android.R.id.home:
        		NavUtils.navigateUpFromSameTask(this);
        		return true;
        }
		return super.onOptionsItemSelected(item);
    }



	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		outState.putParcelable(MY_IMAGE, m_BitmapMe);
		outState.putParcelable(OTHER_IMAGE, m_BitmapOther);
	}
}
