package gilaad.itamar.idan.notebookswapper.wishactivity;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.common.MenuItemSelected;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;
import gilaad.itamar.idan.notebookswapper.searchnotebook.IMatchOfferListener;
import gilaad.itamar.idan.notebookswapper.searchnotebook.MatchSpecificOfferActivity;
import gilaad.itamar.idan.notebookswapper.searchnotebook.RefreshedListViewActivity;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

public class WishActivity extends RefreshedListViewActivity implements IMatchOfferListener, IOnWishMessagePickedListener {
	public static final int EDIT_REQUEST_CODE = 0;
	public static final int EDIT_RESULT_CODE = 88;
	public static final String EXTRA_WISHITEM = "wishID";
	public static final String EXTRA_EDITEDITEM = "editedItem";

	private IWishlistItem m_wishItem;
	public int m_iSelectedTab = 0;

	private static String WISH_TAB = "WishSelectedTab";
	private TabListener<WishMessagesFragment> msgsListener;
	private TabListener<WishRelatedOffersFragment> offersListener;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_main);
        m_wishItem = (IWishlistItem) getIntent().getSerializableExtra(EXTRA_WISHITEM);

        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
	    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

	    msgsListener = new gilaad.itamar.idan.notebookswapper.wishactivity.TabListener<WishMessagesFragment>(this, "messages", WishMessagesFragment.class,m_wishItem);
	    Tab tab1 = actionBar.newTab()
	    		.setText("  " + getResources().getString(R.string.wish_messages_tab_title) + "  ")
	    		.setIcon(android.R.drawable.ic_dialog_email)
	    		.setTabListener(msgsListener);
	    actionBar.addTab(tab1);
	    offersListener = new gilaad.itamar.idan.notebookswapper.wishactivity.TabListener<WishRelatedOffersFragment>(this, "offers", WishRelatedOffersFragment.class,m_wishItem);
	    Tab tab2 = actionBar.newTab()
	    		.setText(getResources().getString(R.string.wish_offers_tab_title))
	    		.setIcon(android.R.drawable.ic_menu_search)
	    		.setTabListener(offersListener);
	    actionBar.addTab(tab2);

	    if (savedInstanceState!=null)
	    {
	    	m_iSelectedTab = savedInstanceState.getInt(WISH_TAB);
	    }
	    else
	    {
	    	m_iSelectedTab = 0;
	    }
	    getActionBar().setSelectedNavigationItem(m_iSelectedTab);
	}


    @Override
    protected void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	outState.putInt(WISH_TAB, m_iSelectedTab);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.offer_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_logout) {
    		if (MenuItemSelected.commonOnOptionsItemSelected(this, item))
    		{
    			return true;
    		}
        }
        if (id == R.id.action_edit) {
        	Intent intent = new Intent(this,EditWishActivity.class);
        	intent.putExtra(WishActivity.EXTRA_WISHITEM, m_wishItem);
        	startActivityForResult(intent, EDIT_REQUEST_CODE);
        	return true;
        }
        if (id == R.id.action_delete) {
        	Intent i = new Intent(this, RemoteDBService.class);
			i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.RemoveWish);
			i.putExtra(RemoteDBService.PARAM_OBJECT, m_wishItem);
			startService(i);
        	finish();
        	return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == EDIT_REQUEST_CODE && resultCode == EDIT_RESULT_CODE)
         {
        	 if (data != null)
        	 {
        		 m_wishItem = (IWishlistItem) data.getSerializableExtra(EXTRA_EDITEDITEM);
        		 offersListener.setWishItem(m_wishItem);
        		 msgsListener.setWishItem(m_wishItem);
        	 }
         }
 }

	@Override
	public void onMatchOffer(IOfferItem offerItem) {
		Intent intent = new Intent(this,MatchSpecificOfferActivity.class);
		intent.putExtra(MatchSpecificOfferActivity.EXTRA_OFFER, offerItem);
		startActivity(intent);
	}


	@Override
	public void OnMessagePicked(Message msg) {
		Intent intent = new Intent(this,WishFullMessagesActivity.class);
		intent.putExtra(WishFullMessagesActivity.OFFERID, msg.offerId);
		intent.putExtra(WishFullMessagesActivity.WISHID, msg.wishId);
		startActivity(intent);
	}


	@Override
	public void refreshList() {
		// TODO Auto-generated method stub

	}

}