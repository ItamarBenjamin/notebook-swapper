package gilaad.itamar.idan.notebookswapper.wishactivity;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.custom_view.InformativeRowCustomView;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import android.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

public abstract class AbstractWishFragment  extends Fragment
{
	private static final String WISH = "wish";
	private IWishlistItem m_wishItem;
	private CheckBox m_CheckBuy = null;
	private CheckBox m_checkEx = null;

	public void setWishItem(IWishlistItem wishItem)
	{
		m_wishItem = wishItem;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (null != savedInstanceState)
		{
			m_wishItem = (IWishlistItem) savedInstanceState.getSerializable(WISH);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(WISH, m_wishItem);
	}

	abstract public void refreshList();

	protected void setCommonInfo(View rootView,View checkboxes)
	{
		Integer courseNumber = m_wishItem.getCourseNumber();
		ICourse course = CacheManager.getManager(getActivity()).getCourse(courseNumber);
		((InformativeRowCustomView)rootView.findViewById(R.id.irName)).setContent(Integer.toString(courseNumber) + " - " + course.getCourseName());
		((InformativeRowCustomView)rootView.findViewById(R.id.irType)).setContent(m_wishItem.getType().toName());
		m_CheckBuy = ((CheckBox)checkboxes.findViewById(R.id.checkBuy));
		m_checkEx = ((CheckBox)checkboxes.findViewById(R.id.checkExchange));
		((InformativeRowCustomView)rootView.findViewById(R.id.irOptions)).setCustomContentType(checkboxes);
	}

	@Override
	public void onResume() {
		setCheckBoxes();
		onResumeSpecific();
		super.onResume();
	}

	private void setCheckBoxes() {
		m_CheckBuy.setChecked(m_wishItem.isForPurchase());
		m_checkEx.setChecked(m_wishItem.isExchangable());
	}

	protected abstract void onResumeSpecific();

	public IWishlistItem getWishlistItem()
	{
		return m_wishItem;
	}

}
