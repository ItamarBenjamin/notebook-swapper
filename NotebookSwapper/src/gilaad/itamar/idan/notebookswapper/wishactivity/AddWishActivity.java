package gilaad.itamar.idan.notebookswapper.wishactivity;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.accounts.AccountManager;
import gilaad.itamar.idan.notebookswapper.adapters.CoursesAutoCompleteAdapter;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.NotebookType;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.WishlistItem;
import gilaad.itamar.idan.notebookswapper.searchnotebook.NotebookItem;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService.RemoteDBServiceOperations;
import gilaad.itamar.idan.notebookswapper.validation.InputValidationErrorDialogFragment;
import gilaad.itamar.idan.notebookswapper.validation.NotebookValidationException;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class AddWishActivity extends Activity
{
	private static final String CURRENT_COURSE = "CURRENT_COURSE";
	private static final String SELECTED_TYPE = "SELECTED_TYPE";
	public static final String NOTEBOOK = "NOTEBOOK";

	protected ICourse m_CurrentCourse;

	protected TextView m_tvCourse;
	protected CoursesAutoCompleteAdapter m_CoursesAdapter;
	protected CheckBox m_checkForBuy;
	protected CheckBox m_checkForExchange;
	protected EditText m_editPrice;
	protected Button m_Button;
	private BroadcastReceiver m_AddWishReciver;

	protected RadioButton m_RadioLecutres, m_RadioTutorials, m_RadioExams;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.wish_editable);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        findControllers();
        fillNotebook((NotebookItem) getIntent().getSerializableExtra(NOTEBOOK));
        initAddButton();

        if (null != savedInstanceState)
        {
        	Integer CourseNum = savedInstanceState.getInt(CURRENT_COURSE);
			if (CourseNum != null && CourseNum > 0)
			{
				setCourse(CacheManager.getManager(this).getCourse(CourseNum));
			}
			NotebookType type = (NotebookType) savedInstanceState.getSerializable(SELECTED_TYPE);
			if (type != null)
			{
				switch (type)
				{
				case EXAMS:
					m_RadioExams.setChecked(true);
					break;
				case LECTURES:
					m_RadioLecutres.setChecked(true);
					break;
				case TUTORIALS:
					m_RadioTutorials.setChecked(true);
					break;
				default:
					break;
				}
			}
        }
	}

	private void setCourse(ICourse course) {
		m_CurrentCourse = course;
		m_tvCourse.setText(course.getCourseNumber() + " " + course.getCourseName());
	}

	private void fillNotebook(NotebookItem notebook) {
		setCourse(notebook.getCourse());
		ICatalogItem catalogItem = notebook.getCatalogItem();

		switch (catalogItem.getType())
		{
			case EXAMS:
				m_RadioExams.setChecked(true);
				break;
			case LECTURES:
				m_RadioLecutres.setChecked(true);
				break;
			case TUTORIALS:
				m_RadioTutorials.setChecked(true);
				break;
			case OTHER:
				break;
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (null != m_CurrentCourse) {
			outState.putInt(CURRENT_COURSE, Integer.valueOf(m_CurrentCourse.getCourseNumber()));
		}
		if (m_RadioExams.isChecked()) {
			outState.putSerializable(SELECTED_TYPE, NotebookType.EXAMS);
		}
		if (m_RadioLecutres.isChecked()) {
			outState.putSerializable(SELECTED_TYPE, NotebookType.LECTURES);
		}
		if (m_RadioTutorials.isChecked()) {
			outState.putSerializable(SELECTED_TYPE, NotebookType.TUTORIALS);
		}
	}

	private void findControllers() {
		m_checkForBuy = (CheckBox)findViewById(R.id.checkForBuy);
		m_checkForExchange = (CheckBox)findViewById(R.id.checkExchange);
		m_editPrice = (EditText)findViewById(R.id.editPrice);
		m_tvCourse = (AutoCompleteTextView) findViewById(R.id.acCourseAddWish);
		m_tvCourse.setEnabled(false);
		m_RadioExams = ((RadioButton)findViewById(R.id.radioExams));
		m_RadioExams.setEnabled(false);
        m_RadioTutorials = ((RadioButton)findViewById(R.id.radioExc));
        m_RadioTutorials.setEnabled(false);
        m_RadioLecutres = ((RadioButton)findViewById(R.id.radioLecture));
        m_RadioLecutres.setEnabled(false);
	}

	private void initAddButton() {
		final Animation animPress = AnimationUtils.loadAnimation(this, R.anim.anim_pressing);
		m_Button = (Button)findViewById(R.id.btnPost);
        m_Button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v)
			{
				v.startAnimation(animPress);
				setProgressBarIndeterminateVisibility(true);
				try
				{
					validateInput();
					IWishlistItem data = new WishlistItem(getUid(), getCourseNumber(), getExchange(), ItemState.REGULAR, isForPurchase(), getSelectedType());
					m_AddWishReciver = new BroadcastReceiver() {
						@Override
						public void onReceive(Context context, Intent intent) {
							setProgressBarIndeterminateVisibility(false);
							boolean bError = intent.getBooleanExtra(RemoteDBService.PARAM_ERROR, false);
							if (bError)
							{
								m_Button.setEnabled(true);
								unregisterReceiver(this);
								Toast.makeText(AddWishActivity.this, "Failed to add the wish", Toast.LENGTH_SHORT).show();
							}
							else
							{
								unregisterReceiver(this);
								finish();
							}
						}
					};
					IntentFilter intentFilter = new IntentFilter();
					intentFilter.addAction(RemoteDBServiceOperations.AddWish.toString());
					registerReceiver(m_AddWishReciver, intentFilter);
					m_Button.setEnabled(false);
					Intent i = new Intent(v.getContext(), RemoteDBService.class);
					i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.AddWish);
					i.putExtra(RemoteDBService.PARAM_OBJECT, data);
					startService(i);
				}
				catch (NotebookValidationException e)
				{
					setProgressBarIndeterminateVisibility(false);
					InputValidationErrorDialogFragment dialog = new InputValidationErrorDialogFragment();
					Log.v("Validation:", e.getMessage());
			        //dialog.show(getSupportFragmentManager(), "InputValidationErrorDialogFragment");
				}
			}
		});
	}

	protected void validateInput() throws NotebookValidationException
	{
		if (getExchange() || isForPurchase())
		{
			return;
		}
		throw new NotebookValidationException("Please choose exchange or purchase");
	}

	protected String getUid() {
		return AccountManager.getCurrentAccount(getApplicationContext()).getUID();
	}

	protected int getCourseNumber() throws NotebookValidationException {
		if (null == m_CurrentCourse)
		{
			throw new NotebookValidationException("Please select a course");
		}
		return m_CurrentCourse.getCourseNumber();
	}

	protected int getMaxPrice() {
		Integer integer  = Integer.getInteger(((EditText)findViewById(R.id.editPrice)).getText().toString());
		return integer == null ? 0 : integer;
	}

	protected boolean getExchange() {
		return ((CheckBox)findViewById(R.id.checkExchange)).isChecked();
	}

	private NotebookType getSelectedType() throws NotebookValidationException {
		if (m_RadioTutorials.isChecked())
		{
			return NotebookType.TUTORIALS;
		}
		if (m_RadioLecutres.isChecked())
		{
			return NotebookType.LECTURES;
		}
		if (m_RadioExams.isChecked())
		{
			return NotebookType.EXAMS;
		}
		throw new NotebookValidationException("Please select notebook type from the check boxes");
	}

	protected Boolean isForPurchase() {
		return ((CheckBox)findViewById(R.id.checkForBuy)).isChecked();
	}

	protected Boolean isRelevant() {
		return true;
	}
}
