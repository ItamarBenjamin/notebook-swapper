package gilaad.itamar.idan.notebookswapper.wishactivity;

import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;

public interface IOnWishMessagePickedListener {
	void OnMessagePicked(Message msg);
}
