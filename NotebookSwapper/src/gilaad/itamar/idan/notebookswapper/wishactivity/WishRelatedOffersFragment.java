package gilaad.itamar.idan.notebookswapper.wishactivity;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.accounts.AccountManager;
import gilaad.itamar.idan.notebookswapper.adapters.MatchingOffersListAdapter;
import gilaad.itamar.idan.notebookswapper.animation.DefaultAnimation;
import gilaad.itamar.idan.notebookswapper.asynctasks.GetOffersByCatIDAsyncTask;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.searchnotebook.RefreshedListViewActivity;
import gilaad.itamar.idan.notebookswapper.server.gcm.GcmIntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.nhaarman.listviewanimations.swinginadapters.AnimationAdapter;

public class WishRelatedOffersFragment extends AbstractWishFragment {
	private ListView lstView;
	private TextView tvLookingForBooks;
	private AnimationAdapter m_oAdapter;
	private BroadcastReceiver m_OffersReceiver = null;
	private MatchingOffersListAdapter innerAdapter;
	private TextView m_title;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.wish_generic_fragment, container, false);
		View checkboxes = inflater.inflate(R.layout.wish_checkboxes, container, false);
		setCommonInfo(rootView,checkboxes);
		findControllers(rootView);


		m_title.setText(getResources().getString(R.string.wish_offers_tab_title));
		innerAdapter = new MatchingOffersListAdapter(getActivity(),getWishlistItem().getWishID());
		m_oAdapter = DefaultAnimation.getDefaultListViewAnimationAdapter(innerAdapter,lstView);
		lstView.setAdapter(m_oAdapter);
		boolean bShowForSale = getWishlistItem().isForPurchase();
		boolean bShowForExchange = getWishlistItem().isExchangable();
		ICatalogItem catalogItem = CacheManager.getManager(getActivity()).getCatalogItemForCourseAndType(getWishlistItem().getCourseNumber(), getWishlistItem().getType());
		String uid = AccountManager.getCurrentAccount(getActivity().getApplicationContext()).getUID();
		new GetOffersByCatIDAsyncTask(innerAdapter, catalogItem.getCatalogItemID(),bShowForSale,bShowForExchange,uid,(RefreshedListViewActivity) getActivity(),tvLookingForBooks).execute(new Void[0]);
		return rootView;
	}

	private void findControllers(View rootView) {
		lstView = (ListView) rootView.findViewById(R.id.lstGeneric);
		tvLookingForBooks = (TextView) rootView.findViewById(R.id.txtNotAvailable);
		m_title = (TextView) rootView.findViewById(R.id.txtSpecificTitle);
	}

	@Override
	protected void onResumeSpecific() {
		m_OffersReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.v("WishRelatedOffersFragment", "Got offer update");
				boolean bShowForSale = getWishlistItem().isForPurchase();
				boolean bShowForExchange = getWishlistItem().isExchangable();
				String uid = AccountManager.getCurrentAccount(getActivity().getApplicationContext()).getUID();
				ICatalogItem catalogItemForCourseAndType = CacheManager.getManager(getActivity()).getCatalogItemForCourseAndType(getWishlistItem().getCourseNumber(), getWishlistItem().getType());
				new GetOffersByCatIDAsyncTask(innerAdapter, catalogItemForCourseAndType.getCatalogItemID(),bShowForSale,bShowForExchange,uid,(RefreshedListViewActivity) getActivity(),tvLookingForBooks).execute(new Void[0]);
				m_oAdapter.notifyDataSetChanged();
			}
		};
		m_oAdapter.notifyDataSetChanged();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(GcmIntentService.OFFER_MATCH_NOTIFICATION);
		getActivity().registerReceiver(m_OffersReceiver, intentFilter);
	}

	@Override
	public void onDestroyView() {
		if (null != m_OffersReceiver)
		{
			getActivity().unregisterReceiver(m_OffersReceiver);
		}
		super.onDestroyView();
	}

	@Override
	public void refreshList() {
		m_oAdapter.notifyDataSetChanged();
	}
}
