package gilaad.itamar.idan.notebookswapper.wishactivity;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.accounts.AccountManager;
import gilaad.itamar.idan.notebookswapper.adapters.CoursesAutoCompleteAdapter;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.common.MenuItemSelected;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.NotebookType;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.WishlistItem;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService;
import gilaad.itamar.idan.notebookswapper.validation.InputValidationErrorDialogFragment;
import gilaad.itamar.idan.notebookswapper.validation.NotebookValidationException;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;

public class EditWishActivity extends Activity {
	protected ICourse m_CurrentCourse;
	private IWishlistItem m_CurrentWish;

	protected AutoCompleteTextView m_tvAutoCompleteCourse;
	protected CheckBox m_checkForBuy;
	protected CheckBox m_checkForExchange;
	protected EditText m_editPrice;
	protected Button m_Button;
	protected RadioButton m_RadioLecutres, m_RadioTutorials, m_RadioExams;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.wish_editable);
        m_CurrentWish = (IWishlistItem)getIntent().getSerializableExtra(WishActivity.EXTRA_WISHITEM);

        findControllers();
        initAddButton();
        fillInitialValues();
        setControllersInactive();
	}

	private void setControllersInactive() {
		m_tvAutoCompleteCourse.setEnabled(false);
		m_RadioLecutres.setEnabled(false);
		m_RadioTutorials.setEnabled(false);
		m_RadioExams.setEnabled(false);
	}

	private void initAddButton() {
		m_Button = (Button)findViewById(R.id.btnPost);
        m_Button.setVisibility(View.GONE);
	}

	private void findControllers() {
		m_checkForBuy = (CheckBox)findViewById(R.id.checkForBuy);
		m_checkForExchange = (CheckBox)findViewById(R.id.checkExchange);
		m_editPrice = (EditText)findViewById(R.id.editPrice);
		m_tvAutoCompleteCourse = (AutoCompleteTextView) findViewById(R.id.acCourseAddWish);
		m_RadioExams = ((RadioButton)findViewById(R.id.radioExams));
        m_RadioTutorials = ((RadioButton)findViewById(R.id.radioExc));
        m_RadioLecutres = ((RadioButton)findViewById(R.id.radioLecture));
	}

    private void fillInitialValues() {
    	m_CurrentCourse = CacheManager.getManager(this).getCourse(m_CurrentWish.getCourseNumber());
    	m_tvAutoCompleteCourse.setText(CoursesAutoCompleteAdapter.getTitleFor(m_CurrentCourse));

    	NotebookType notebookType = m_CurrentWish.getType();
		m_RadioLecutres.setChecked(notebookType == NotebookType.LECTURES);
		m_RadioTutorials.setChecked(notebookType == NotebookType.TUTORIALS);
		m_RadioExams.setChecked(notebookType == NotebookType.EXAMS);
		m_checkForBuy.setChecked(m_CurrentWish.isForPurchase());
		m_checkForExchange.setChecked(m_CurrentWish.isExchangable());
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_save) {
        	try
			{
        		IWishlistItem data = new WishlistItem(m_CurrentWish.getWishID(),getUid(), getCourseNumber(), getExchange(), m_CurrentWish.getState(), getForPurchase(), getSelectedType(),0L);
        		Intent i = new Intent(this, RemoteDBService.class);
				i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.UpdateWish);
				i.putExtra(RemoteDBService.PARAM_OBJECT, data);
				startService(i);
				Intent resultIntent = new Intent();
				resultIntent.putExtra(WishActivity.EXTRA_EDITEDITEM, data);
				setIntent(resultIntent);
				setResult(WishActivity.EDIT_RESULT_CODE,resultIntent);
        		finish();
			}
        	catch (NotebookValidationException e)
			{
				InputValidationErrorDialogFragment dialog = new InputValidationErrorDialogFragment();
				Log.v("Validation:", e.getMessage());
		        //dialog.show(getSupportFragmentManager(), "InputValidationErrorDialogFragment");
			}
        	return true;
        }
        if (id == R.id.action_delete) {
        	Intent i = new Intent(this, RemoteDBService.class);
			i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.RemoveWish);
			i.putExtra(RemoteDBService.PARAM_OBJECT, m_CurrentWish);
			startService(i);
    		finish();
        	return true;
        }
        if (id == R.id.action_logout)
        {
        	if (MenuItemSelected.commonOnOptionsItemSelected(this, item))
        	{
        		return true;
        	}
        }
        return super.onOptionsItemSelected(item);
    }

    protected String getUid() {
		return AccountManager.getCurrentAccount(getApplicationContext()).getUID();
	}

	protected int getCourseNumber() throws NotebookValidationException {
		if (null == m_CurrentCourse)
		{
			throw new NotebookValidationException("Couldn't find course");
		}
		return m_CurrentCourse.getCourseNumber();
	}

	private NotebookType getSelectedType() throws NotebookValidationException {
		return m_CurrentWish.getType();
	}

	protected boolean getForPurchase() throws NotebookValidationException {
		return m_checkForBuy.isChecked();
	}

	protected boolean getExchange() {
		return m_checkForExchange.isChecked();
	}
}
