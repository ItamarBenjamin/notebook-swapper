package gilaad.itamar.idan.notebookswapper.image;

import gilaad.itamar.idan.notebookswapper.R;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class ImageActivity extends Activity {

	private Bitmap m_Bitmap;
	public final static String EXTRA_IMAGE = "IMAGE";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image);
		ImageView ivImage = (ImageView) findViewById(R.id.ivImage);
		if (savedInstanceState != null)
		{
			m_Bitmap = savedInstanceState.getParcelable("IMAGE");
		} else
		{
			Intent intent = getIntent();
			Parcelable parcelableExtra = intent.getParcelableExtra("IMAGE");
			if (parcelableExtra != null)
			{
				m_Bitmap = (Bitmap) parcelableExtra;
			}
		}
		if (m_Bitmap != null)
		{
			ivImage.setImageBitmap(m_Bitmap);
		}
		ivImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ImageActivity.this.finish();
			}
		});
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		outState.putParcelable(EXTRA_IMAGE, m_Bitmap);
	}
}
