package gilaad.itamar.idan.notebookswapper.post;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.accounts.AccountManager;
import gilaad.itamar.idan.notebookswapper.adapters.CoursesAutoCompleteAdapter;
import gilaad.itamar.idan.notebookswapper.adapters.EditionsAutoCompleteAdapter;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.common.SnapshotTaker;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.ItemState;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.NotebookCondition;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.NotebookType;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.OfferItem;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService.RemoteDBServiceOperations;
import gilaad.itamar.idan.notebookswapper.validation.NotebookValidationException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.Toast;

public class AddOfferActivity extends Activity{

	private static final String SNAPSHOT = "SNAPSHOT";
	private static final String IMAGE_URL = "IMAGE_URL";
	private static final String IMAGE_PATH = "IMAGE_PATH";
	private static final String CURRENT_COURSE = "CURRENT_COURSE";
	private static final String SELECTED_TYPE = "SELECTED_TYPE";

	protected ICourse m_CurrentCourse;
	protected List<ICatalogItem> m_RelevantCatalogItems;
	protected boolean m_bCourseSelected;
	protected String[] m_CurrentEditionContents;


	protected CoursesAutoCompleteAdapter m_CoursesAdapter;
	protected EditionsAutoCompleteAdapter m_EditionAdapter;

	protected AutoCompleteTextView m_tvAutoCompleteCourse;
	protected Spinner m_spEdition;
	protected Button m_Button;
	protected RadioButton m_RadioLecutres, m_RadioTutorials, m_RadioExams;
	protected CheckBox m_checkForSale;
	protected CheckBox m_checkForGiveAway;
	protected EditText m_editPrice;
	protected ImageView m_imViewSnapshot;

	protected SnapshotTaker m_snap;
	protected Bitmap m_bmSnapshot;
	protected String m_imageUrl;
	protected String m_imagePath;
	private BroadcastReceiver m_OfferAddedReciever = null;
	private boolean justset = false;
	protected IOfferItem m_NewOfferItem;
	private SeekBar m_seekBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.offer_editable);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        findControllers();
        initCondBar();
        initAutoComplete();
        initCheckboxes();
        initAddButton();
        initSnapshotButton();
        if (savedInstanceState == null)
        {
	        m_bCourseSelected = false;
	        m_bmSnapshot = null;
	        m_imageUrl = null;
	        m_imagePath = null;

		} else
		{
			m_bmSnapshot = savedInstanceState.getParcelable(SNAPSHOT);
			if (m_bmSnapshot != null)
			{
				m_imViewSnapshot.setImageBitmap(m_bmSnapshot);
			}
			m_imageUrl = savedInstanceState.getString(IMAGE_URL);
			m_imagePath = savedInstanceState.getString(IMAGE_PATH);
			Integer CourseNum = savedInstanceState.getInt(CURRENT_COURSE);
			if (CourseNum != null && CourseNum > 0)
			{
				m_CurrentCourse = CacheManager.getManager(this).getCourse(CourseNum);
				onChangeCourse();
			}
			NotebookType type = (NotebookType) savedInstanceState.getSerializable(SELECTED_TYPE);
			if (type != null)
			{
				switch (type)
				{
				case EXAMS:
					m_RadioExams.setChecked(true);
					break;
				case LECTURES:
					m_RadioLecutres.setChecked(true);
					break;
				case TUTORIALS:
					m_RadioTutorials.setChecked(true);
					break;
				default:
					break;
				}
				justset = true;
			}
		}
	}

	private void initCondBar() {
	  m_seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			    @Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

			    }

			    @Override
				public void onStartTrackingTouch(SeekBar seekBar) {
			    }

			    @Override
				public void onStopTrackingTouch(SeekBar seekBar) {
			    	int iLocation = getLocation(seekBar);
			    	int iProgress = 0;
			    	switch (iLocation)
			    	{
			    		case 0 : iProgress = 0; break;
			    		case 1 : iProgress = seekBar.getMax()/2; break;
			    		case 2 : iProgress = seekBar.getMax(); break;
			    	}
			    	seekBar.setProgress(iProgress);
			    }
	  });
	}

	private int getLocation(SeekBar seekBar) {
		int iLocation = 0;
    	int progress = seekBar.getProgress();
    	if (progress>33 && progress<66)
    	{
    		iLocation = 1;
    	}
    	else if (progress > 66)
    	{
    		iLocation = 2;
    	}
		return iLocation;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable(SNAPSHOT, m_bmSnapshot);
		outState.putString(IMAGE_URL, m_imageUrl);
		outState.putString(IMAGE_PATH, m_imagePath);
		if (m_CurrentCourse != null)
		{
			outState.putInt(CURRENT_COURSE, Integer.valueOf(m_CurrentCourse.getCourseNumber()));
		}
		if (m_RadioExams.isChecked()) {
			outState.putSerializable(SELECTED_TYPE, NotebookType.EXAMS);
		}
		if (m_RadioLecutres.isChecked()) {
			outState.putSerializable(SELECTED_TYPE, NotebookType.LECTURES);
		}
		if (m_RadioTutorials.isChecked()) {
			outState.putSerializable(SELECTED_TYPE, NotebookType.TUTORIALS);
		}
	}

	private void resetEditionSpinner()
	{
		m_EditionAdapter.clear();
		m_EditionAdapter.add(getResources().getString(R.string.please_select_course));
		m_EditionAdapter.notifyDataSetChanged();
	}

	private void onChangeCourse()
	{
		m_EditionAdapter.clear();
		m_RelevantCatalogItems = CacheManager.getManager(this).getCatalogItemsForCourse(m_CurrentCourse);
		Set<String> set = new HashSet<String>();
		for (ICatalogItem $ : m_RelevantCatalogItems)
		{
			String key = $.getEdition();
			set.add(key);
		}
		m_CurrentEditionContents = set.toArray(new String[set.size()]);;
		m_EditionAdapter.addAll(m_CurrentEditionContents);
		m_EditionAdapter.notifyDataSetChanged();
		m_bCourseSelected = true;
		onChangeEdition();
	}

	private void onChangeEdition()
	{
		boolean e = false,t = false, l = false;
		if (m_bCourseSelected)
		{
			for (ICatalogItem $ : m_RelevantCatalogItems)
			{
				if ($.getEdition().equalsIgnoreCase((String) m_spEdition.getSelectedItem()))
				{
					switch ($.getType())
					{
					case EXAMS:
						e = true;
						break;
					case LECTURES:
						l = true;
						break;
					case TUTORIALS:
						t = true;
						break;
					default:
						break;
					}
				}
			}
		}
		m_RadioTutorials.setEnabled(t);
		m_RadioLecutres.setEnabled(l);
		m_RadioExams.setEnabled(e);
		if (!justset)
		{
			m_RadioExams.setChecked(false);
			m_RadioTutorials.setChecked(false);
			m_RadioLecutres.setChecked(false);
		}
		justset = false;
	}

	private void initAutoComplete() {
		m_CoursesAdapter = new CoursesAutoCompleteAdapter(this,CacheManager.getManager(this).getAllCourses());
		m_tvAutoCompleteCourse.setAdapter(m_CoursesAdapter);
		m_tvAutoCompleteCourse.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				m_tvAutoCompleteCourse.setText("");
				m_bCourseSelected = false;
				resetEditionSpinner();
			}
		});
        m_tvAutoCompleteCourse.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				m_CurrentCourse = m_CoursesAdapter.getItem(position);
				m_tvAutoCompleteCourse.setText(CoursesAutoCompleteAdapter.getTitleFor(m_CurrentCourse));
				onChangeCourse();
			}
		});

		m_EditionAdapter = new EditionsAutoCompleteAdapter(this, new ArrayList<String>());
        m_EditionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        m_spEdition.setAdapter(m_EditionAdapter);
        m_spEdition.setOnItemSelectedListener(new OnItemSelectedListener() {


			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				onChangeEdition();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
        resetEditionSpinner();
	}

	private void findControllers() {
		m_checkForSale = (CheckBox)findViewById(R.id.checkForSale);
		m_checkForGiveAway = (CheckBox)findViewById(R.id.checkGiveaway);
		m_editPrice = (EditText)findViewById(R.id.editPrice);
		m_tvAutoCompleteCourse = (AutoCompleteTextView) findViewById(R.id.acCourse);
		m_spEdition = (Spinner) findViewById(R.id.spinnerEdition);
		m_RadioExams = ((RadioButton)findViewById(R.id.radioExams));
        m_RadioTutorials = ((RadioButton)findViewById(R.id.radioExc));
        m_RadioLecutres = ((RadioButton)findViewById(R.id.radioLecture));
        m_seekBar = (SeekBar)findViewById(R.id.condBar);
	}

	private void initAddButton() {
		final Animation animPress = AnimationUtils.loadAnimation(this, R.anim.anim_pressing);
		m_Button = (Button)findViewById(R.id.btnPost);
		m_Button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				v.startAnimation(animPress);
				v.setEnabled(false);
				Context context = v.getContext();
				try {
					m_NewOfferItem = createOfferAndValidate();
				} catch (NotebookValidationException e) {
					handleError(e.getMessage(), false);
					return;
				}
				setProgressBarIndeterminateVisibility(true);
				if (null == m_imagePath || m_imageUrl != null) {
					// either we didn't take any snapshot, or we did but we already uploaded it.
					addOfferAndFinish(context);
				} else {
					// try to upload image - when this is finished, the result handler will add the offer
					setImageUrlReceiver();
					Intent i = new Intent(getApplicationContext(), RemoteDBService.class);
					i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.UploadImage);
					i.putExtra(RemoteDBService.PARAM_OBJECT, m_imagePath);
					startService(i);
				}
			}
		});
	}

	protected void setImageUrlReceiver() {

		BroadcastReceiver imageUrlReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				try
				{
					boolean bError = intent.getBooleanExtra(RemoteDBService.PARAM_ERROR, false);
					if (!bError && intent.hasExtra(RemoteDBService.PARAM_OBJECT)) {
						// everything looks OK...
						m_imageUrl = intent.getExtras().getString(RemoteDBService.PARAM_OBJECT);
						Log.v("BroadcaseReceiver", "Filter caught RemoteDBServiceOperations.UploadImage action, with url " + m_imageUrl);
						addOfferAndFinish(context);
					} else {
						setProgressBarIndeterminateVisibility(false);
						handleError( getResources().getString(R.string.error_couldnt_add_offer_now),true);
						// either way, we would like to save it to unsynced offers and try again later
						saveUnsyncedWithLocalImagePathAndFinish();
					}
				}
				finally
				{
					unregisterReceiver(this);
				}
			}

		};

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(RemoteDBServiceOperations.UploadImage.toString());
		registerReceiver(imageUrlReceiver, intentFilter);
	}

	protected void saveUnsyncedWithLocalImagePathAndFinish() {
		IOfferItem offerWithLocalImage = new OfferItem(m_NewOfferItem.getUserID(),
				m_NewOfferItem.getCatalogItemID(),
				m_NewOfferItem.isExchangable(),
				m_NewOfferItem.getState(),
				m_NewOfferItem.getPrice(),
				m_NewOfferItem.getCondition(),
				m_NewOfferItem.getAdditionalInfo());
		offerWithLocalImage.setImageUrl(m_imagePath);
		CacheManager.getManager(getApplicationContext()).addUnsyncedOffer(offerWithLocalImage);
		finish();
	}

	private IOfferItem createOfferAndValidate() throws NotebookValidationException
	{
		return new OfferItem(getUid(), getCatalogeID(), getExchange(), ItemState.REGULAR, getPrice(), getCond(), getInfo());

	}
	protected void addOfferAndFinish(Context context) {
		if (null != m_imageUrl) {
			m_NewOfferItem.setImageUrl(m_imageUrl);
		}
		m_OfferAddedReciever   = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				setProgressBarIndeterminateVisibility(false);
				boolean bError = intent.getBooleanExtra(RemoteDBService.PARAM_ERROR, false);
				if (bError) {
					handleError( getResources().getString(R.string.error_couldnt_add_offer_now),true);
					unregisterReceiver(this);
				}
				else {
					unregisterReceiver(this);
					finish();
				}
			}
		};
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(RemoteDBServiceOperations.AddOffer.toString());
		registerReceiver(m_OfferAddedReciever, intentFilter);
		Intent i = new Intent(context, RemoteDBService.class);
		i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.AddOffer);
		i.putExtra(RemoteDBService.PARAM_OBJECT, m_NewOfferItem);
		startService(i);
	}


	protected void handleError(String sError, boolean isLong) {
		Toast.makeText(this, sError, isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
		m_Button.setEnabled(true);
	}


	private void initSnapshotButton() {
		m_imViewSnapshot = (ImageView)findViewById(R.id.imgSnapshot);
		m_imViewSnapshot.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				takeSnapshot();
			}
		});
	}

	protected void takeSnapshot() {
		m_snap = new SnapshotTaker();
		m_imagePath = m_snap.takeSnapAndReturnPath(this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == SnapshotTaker.REQUEST_TAKE_SNAP && resultCode == RESULT_OK) {
			// Successfully took a new notebook photo
			m_bmSnapshot = SnapshotTaker.getSmallScaledBitmapFromPath(m_imagePath);
			m_imViewSnapshot.setImageBitmap(m_bmSnapshot);
		}
	}


	private void initCheckboxes() {
		m_checkForSale.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				m_editPrice.setEnabled(isChecked);
				if (!isChecked)
				{
					m_editPrice.setText("");
				}
				else
				{
					m_checkForGiveAway.setChecked(false);
				}
			}
		});

		m_checkForGiveAway.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked)
				{
					m_checkForSale.setChecked(false);
				}
			}
		});
	}

	protected String getUid() {
		return AccountManager.getCurrentAccount(getApplicationContext()).getUID();
	}

	protected long getCatalogeID() throws NotebookValidationException {
		if (!m_bCourseSelected)
		{
			throw new NotebookValidationException( getResources().getString(R.string.error_select_course));
		}
		NotebookType selectedType = getSelectedType();
		String sEdition = (String) m_spEdition.getSelectedItem();
		for (ICatalogItem $ : m_RelevantCatalogItems)
		{
			if ($.getEdition().equalsIgnoreCase(sEdition) && $.getType() == selectedType)
			{
				return $.getCatalogItemID();
			}
		}
		throw new NotebookValidationException( getResources().getString(R.string.error_unknown));
	}

	private NotebookType getSelectedType() throws NotebookValidationException {
		if (m_RadioTutorials.isEnabled() && m_RadioTutorials.isChecked())
		{
			return NotebookType.TUTORIALS;
		}
		if (m_RadioLecutres.isEnabled() && m_RadioLecutres.isChecked())
		{
			return NotebookType.LECTURES;
		}
		if (m_RadioExams.isEnabled() && m_RadioExams.isChecked())
		{
			return NotebookType.EXAMS;
		}
		throw new NotebookValidationException( getResources().getString(R.string.error_select_book_type));
	}

	protected String getInfo() {
		return ((EditText)findViewById(R.id.editExtraInfo)).getText().toString();
	}

	protected NotebookCondition getCond()
	{
		int iProgress = getLocation(m_seekBar);
		return NotebookCondition.fromInt(iProgress);
	}

	protected int getPrice() throws NotebookValidationException {
		if (!((CheckBox)findViewById(R.id.checkForSale)).isChecked()) {
			return 0;
		}
		String sPrice = ((EditText)findViewById(R.id.editPrice)).getText().toString();
		if (sPrice.isEmpty()) {
			throw new NotebookValidationException( getResources().getString(R.string.error_enter_price));
		}
		Integer iPrice;
		try
		{
			iPrice  = Integer.parseInt(sPrice);
			if (null == iPrice) {
				throw new Exception();
			}
		}
		catch(Throwable t)
		{
			throw new NotebookValidationException( getResources().getString(R.string.error_invalid_price));
		}
		return iPrice;
	}

	protected boolean getExchange() {
		return ((CheckBox)findViewById(R.id.checkExchange)).isChecked();
	}
}
