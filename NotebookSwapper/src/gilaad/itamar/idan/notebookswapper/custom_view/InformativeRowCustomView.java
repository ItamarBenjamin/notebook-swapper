package gilaad.itamar.idan.notebookswapper.custom_view;

import gilaad.itamar.idan.notebookswapper.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class InformativeRowCustomView extends RelativeLayout {

	private String mTitle,mContent;
	private TextView m_tvTitle,m_tvContent ;
	private View m_Sperator;

	public InformativeRowCustomView(Context context) {
        super(context);
        init();
    }

    public InformativeRowCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        readAttrs(attrs);
        init();
    }

    public InformativeRowCustomView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        readAttrs(attrs);
        init();
    }

	private void readAttrs(AttributeSet attrs)
	{
		Context context = getContext();
		TypedArray a = context.getTheme().obtainStyledAttributes(
				attrs,
				R.styleable.InformativeRowCustomView,
				0, 0);

		try {
			mTitle = a.getString(R.styleable.InformativeRowCustomView_title);
			mContent = a.getString(R.styleable.InformativeRowCustomView_content);
		} finally {
			a.recycle();
		}
	}

	private void init()
	{
		LayoutInflater.from(getContext()).inflate(R.layout.informative_row, this, true);
		m_tvTitle = (TextView)findViewById(R.id.txtTitle);
        m_tvContent = (TextView)findViewById(R.id.txtContent);
        m_Sperator = findViewById(R.id.seperator);
        m_tvTitle.setText(mTitle);
        m_tvContent.setText(mContent);

        int padding = (int)getResources().getDimension(R.dimen.default_padding);
        setPadding(padding, padding, padding, padding);
	}

	public void setContent(String content)
	{
		mContent = content;
		setVisibiltyAndText();
		invalidate();
		requestLayout();
	}

	public void setCustomContentType(View v)
	{
		v.setId(m_tvContent.getId());
		this.addView(v, m_tvContent.getLayoutParams());
		removeView(m_tvContent);
		invalidate();
		requestLayout();
	}

	private void setVisibiltyAndText() {
		if (mContent == null || mContent.isEmpty())
		{
			setVisibility(View.GONE);
		} else
		{
			setVisibility(View.VISIBLE);
			m_tvContent.setText(mContent);
		}
	}
}
