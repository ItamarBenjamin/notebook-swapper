package gilaad.itamar.idan.notebookswapper.edit;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.accounts.AccountManager;
import gilaad.itamar.idan.notebookswapper.adapters.CoursesAutoCompleteAdapter;
import gilaad.itamar.idan.notebookswapper.adapters.EditionsAutoCompleteAdapter;
import gilaad.itamar.idan.notebookswapper.asynctasks.DownloadImageTask;
import gilaad.itamar.idan.notebookswapper.cache.CacheManager;
import gilaad.itamar.idan.notebookswapper.common.MenuItemSelected;
import gilaad.itamar.idan.notebookswapper.common.SnapshotTaker;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.NotebookCondition;
import gilaad.itamar.idan.notebookswapper.dbitems.enums.NotebookType;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.OfferItem;
import gilaad.itamar.idan.notebookswapper.offer.OfferActivity;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService.RemoteDBServiceOperations;
import gilaad.itamar.idan.notebookswapper.validation.NotebookValidationException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class EditOfferActivity extends Activity
{
	private static final String SNAPSHOT = "SNAPSHOT";
	private static final String IMAGE_URL = "IMAGE_URL";
	private static final String IMAGE_PATH = "IMAGE_PATH";
	private static final String NEW_IMAGE_TAKEN = "NEW_IMAGE_TAKEN";

	private IOfferItem m_Offer;
	private ICatalogItem m_CurrentCatItem;

	private ICourse m_CurrentCourse;
	private List<ICatalogItem> m_RelevantCatalogItems;
	private String[] m_CurrentEditionContents;

	private AutoCompleteTextView m_tvAutoCompleteCourse;
	private Spinner m_spEdition;

	private EditionsAutoCompleteAdapter m_EditionAdapter;

	private RadioButton m_RadioLecutres, m_RadioTutorials, m_RadioExams;

	protected Button m_Button;
	protected CheckBox m_checkForSale;
	protected EditText m_editPrice;
	protected CheckBox m_checkForGiveAway;
	protected ImageView m_imViewSnapshot;

	protected boolean m_bNewImage;
	protected SnapshotTaker m_snap;
	protected Bitmap m_bmSnapshot;
	protected String m_imageUrl;
	private String m_imagePath;
	private BroadcastReceiver m_OfferEditedReciever;
	protected IOfferItem m_CurrentItem;
	private SeekBar m_seekBar;
	private CheckBox m_checkExchange;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.offer_editable);

        m_Offer = (IOfferItem) getIntent().getSerializableExtra(OfferActivity.EXTRA_OFFERITEM);
        CacheManager cacheManager = CacheManager.getManager(this);
		m_CurrentCatItem = cacheManager.getCatalogItem(m_Offer.getCatalogItemID());
        m_CurrentCourse = cacheManager.getCourse(m_CurrentCatItem.getCourseNumber());
        m_RelevantCatalogItems = cacheManager.getCatalogItemsForCourse(m_CurrentCourse);
		m_seekBar = (SeekBar) findViewById(R.id.condBar);
		 m_seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			    @Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

			    }

			    @Override
				public void onStartTrackingTouch(SeekBar seekBar) {
			    }

			    @Override
				public void onStopTrackingTouch(SeekBar seekBar) {
			    	int iLocation = getLocation(seekBar);
			    	int iProgress = 0;
			    	switch (iLocation)
			    	{
			    		case 0 : iProgress = 0; break;
			    		case 1 : iProgress = seekBar.getMax()/2; break;
			    		case 2 : iProgress = seekBar.getMax(); break;
			    	}
			    	seekBar.setProgress(iProgress);
			    }
	  });
        findControllers();

        if (null == savedInstanceState) {
        	m_bNewImage = false;
	        retrieveImage(cacheManager);
        } else {
        	m_imageUrl = savedInstanceState.getString(IMAGE_URL);
        	m_imagePath = savedInstanceState.getString(IMAGE_PATH);
        	m_bmSnapshot = savedInstanceState.getParcelable(SNAPSHOT);
        	m_bNewImage = savedInstanceState.getBoolean(NEW_IMAGE_TAKEN);
			if (m_bmSnapshot != null)
			{
				m_imViewSnapshot.setImageBitmap(m_bmSnapshot);
			} else if (m_imageUrl != null)
			{
				retrieveImage(cacheManager);
			}
        }

        initAddButton();
        initCheckboxes();
        initSnapshotButton();
        initEditionsSpinner();

        fillInitialValues();
        setControllersInactive();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (m_imageUrl != null) {
			outState.putString(IMAGE_URL,m_imageUrl);
		}
		if (m_bmSnapshot != null) {
			outState.putParcelable(SNAPSHOT,m_bmSnapshot);
		}
		outState.putBoolean(NEW_IMAGE_TAKEN, m_bNewImage);
		outState.putString(IMAGE_PATH, m_imagePath);
	}
	private int getLocation(SeekBar seekBar) {
		int iLocation = 0;
    	int progress = seekBar.getProgress();
    	if (progress>33 && progress<66)
    	{
    		iLocation = 1;
    	}
    	else if (progress > 66)
    	{
    		iLocation = 2;
    	}
		return iLocation;
	}

	private void retrieveImage(CacheManager cacheManager) {
		m_imageUrl = m_Offer.getImageUrl();
		if (m_imageUrl != null)
		{
		    m_bmSnapshot = cacheManager.getImage(m_imageUrl);
		    if (m_bmSnapshot != null)
		    {
		    	m_imViewSnapshot.setImageBitmap(m_bmSnapshot);
		    } else
		    {
		    	new DownloadImageTask(m_imViewSnapshot,this,false).execute(m_imageUrl);
		    }
		}
	}

	private void initEditionsSpinner()
	{
		m_EditionAdapter = new EditionsAutoCompleteAdapter(this, new ArrayList<String>());
        m_EditionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Set<String> set = new HashSet<String>();
		for (ICatalogItem $ : m_RelevantCatalogItems)
		{
			String key = $.getEdition();
			set.add(key);
		}
		m_CurrentEditionContents = set.toArray(new String[set.size()]);;
		m_EditionAdapter.addAll(m_CurrentEditionContents);
        m_spEdition.setAdapter(m_EditionAdapter);
	}

	private void setControllersInactive() {
		m_tvAutoCompleteCourse.setEnabled(false);
		m_spEdition.setEnabled(false);
		m_RadioLecutres.setEnabled(false);
		m_RadioTutorials.setEnabled(false);
		m_RadioExams.setEnabled(false);
	}

	private void initAddButton()
	{
		 m_Button.setVisibility(View.GONE);
	}

	private void initCheckboxes() {
		m_checkForSale.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				m_editPrice.setEnabled(isChecked);
				if (!isChecked)
				{
					m_editPrice.setText("");
				}
				else
				{
					m_checkForGiveAway.setChecked(false);
				}
			}
		});

		m_checkForGiveAway.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked)
				{
					m_checkForSale.setChecked(false);
				}
			}
		});
	}

	private void initSnapshotButton() {
		m_imViewSnapshot.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				takeSnapshot();
			}
		});
	}

	protected void takeSnapshot() {
		m_snap = new SnapshotTaker();
		m_imagePath = m_snap.takeSnapAndReturnPath(this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == SnapshotTaker.REQUEST_TAKE_SNAP && resultCode == RESULT_OK) {
			// Successfully took a new notebook photo
			m_bmSnapshot = SnapshotTaker.getSmallScaledBitmapFromPath(m_imagePath);
			m_imViewSnapshot.setImageBitmap(m_bmSnapshot);
			m_bNewImage = true;
		}
	}

	private void findControllers()
	{
		m_Button = (Button)findViewById(R.id.btnPost);
		m_imViewSnapshot = (ImageView)findViewById(R.id.imgSnapshot);
		m_checkForSale = (CheckBox)findViewById(R.id.checkForSale);
		m_checkForGiveAway = (CheckBox)findViewById(R.id.checkGiveaway);
		m_checkExchange = ((CheckBox)findViewById(R.id.checkExchange));
		m_editPrice = (EditText)findViewById(R.id.editPrice);
		m_tvAutoCompleteCourse = (AutoCompleteTextView) findViewById(R.id.acCourse);
		m_spEdition = (Spinner) findViewById(R.id.spinnerEdition);
		m_RadioExams = ((RadioButton)findViewById(R.id.radioExams));
        m_RadioTutorials = ((RadioButton)findViewById(R.id.radioExc));
        m_RadioLecutres = ((RadioButton)findViewById(R.id.radioLecture));
	}

    private void fillInitialValues() {

    	m_tvAutoCompleteCourse.setText(CoursesAutoCompleteAdapter.getTitleFor(m_CurrentCourse));
    	int position = 0;
    	for (; position < m_CurrentEditionContents.length ; position++) {
			if (m_CurrentEditionContents[position].equalsIgnoreCase(m_CurrentCatItem.getEdition())) {
				break;
			}
		}
    	m_spEdition.setSelection(position);

       	m_RadioExams.setChecked(m_CurrentCatItem.getType() == NotebookType.EXAMS);
		m_RadioTutorials.setChecked(m_CurrentCatItem.getType() == NotebookType.TUTORIALS);
		m_RadioLecutres.setChecked(m_CurrentCatItem.getType() == NotebookType.LECTURES);

		int iPrice = m_Offer.getPrice();
		m_checkForSale.setChecked(iPrice!=0);
		m_editPrice.setText(Integer.toString(iPrice));
		m_checkExchange.setChecked(m_Offer.isExchangable());
		m_checkForGiveAway.setChecked(m_Offer.isFree());
		String imageUrl = m_Offer.getImageUrl();
		if (null != imageUrl) {
			Bitmap newBitmap = CacheManager.getManager(getApplicationContext()).getImage(imageUrl);
			m_imViewSnapshot.setImageBitmap(newBitmap);
		}


		NotebookCondition condition = m_Offer.getCondition();
		int iMaxProgress = m_seekBar.getMax();
		switch (condition)
		{
			case LIKE_NEW:
				m_seekBar.setProgress(0);
				break;
			case TORN:
				m_seekBar.setProgress(iMaxProgress);
				break;
			case USED:
				m_seekBar.setProgress(iMaxProgress/2);
				break;
			case UNKNOWN:
			default:
				break;
		}

		((TextView)findViewById(R.id.editExtraInfo)).setText(m_Offer.getAdditionalInfo());
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId())
        {
        	case android.R.id.home:
        		NavUtils.navigateUpFromSameTask(this);
        		break;
        	case R.id.action_save:
        		try {
        			m_CurrentItem = new OfferItem(m_Offer.getOfferID(),getUid(), Long.valueOf(getCatalogeID()), getExchange(), m_Offer.getState(), getPrice(), getCond(), getInfo(),0L);
				} catch (NotebookValidationException e) {
					handleError(e.getMessage());
					break;
				}
        		setProgressBarIndeterminateVisibility(true);
        		if (!m_bNewImage) {
					// either we didn't take any snapshot, or we did but we already uploaded it.
					m_CurrentItem.setImageUrl(m_Offer.getImageUrl());
					updateOfferAndFinish();
				} else {
					// try to upload image - when this is finished, the result handler will add the offer
					setImageUrlReceiver();
					Intent i = new Intent(getApplicationContext(), RemoteDBService.class);
					i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.UploadImage);
					i.putExtra(RemoteDBService.PARAM_OBJECT, m_imagePath);
					startService(i);
				}
        	return true;
        	case R.id.action_delete:
        		//TODO ask user for confirmation
	        	Intent i = new Intent(this, RemoteDBService.class);
				i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.RemoveOffer);
				i.putExtra(RemoteDBService.PARAM_OBJECT, m_Offer);
				startService(i);
	    		finish();
	        	return true;
        	case R.id.action_logout:
        		if (MenuItemSelected.commonOnOptionsItemSelected(this, item))
        		{
        			return true;
        		}
        }
        return super.onOptionsItemSelected(item);
    }

    protected void setImageUrlReceiver() {


		BroadcastReceiver imageUrlReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				boolean bError = intent.getBooleanExtra(RemoteDBService.PARAM_ERROR, false);
				if (!bError && intent.hasExtra(RemoteDBService.PARAM_OBJECT)) {
					// everything looks OK...
					m_imageUrl = intent.getExtras().getString(RemoteDBService.PARAM_OBJECT);
					m_CurrentItem.setImageUrl(m_imageUrl);
					m_bNewImage=false;  // we no longer need to send the image
					Log.v("BroadcaseReceiver", "Filter caught RemoteDBServiceOperations.UploadImage action, with url " + m_imageUrl);
					unregisterReceiver(this);
					updateOfferAndFinish();
				} else {
					if (bError)
					{
						setProgressBarIndeterminateVisibility(false);
						handleError( getResources().getString(R.string.error_couldnt_add_offer_now),true);
					} else {
						// no extra object included
						setProgressBarIndeterminateVisibility(false);
						Log.e("BroadcaseReceiver", "Filter caught RemoteDBServiceOperations.UploadImage action, but no PARAM_OBJECT supplied");
						handleError( getResources().getString(R.string.error_couldnt_add_offer_now), true);
					}
					// either way, we would like to save it to unsynced offers and try again later
					saveUnsyncedWithLocalImagePathAndFinish();
				}
			}

		};
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(RemoteDBServiceOperations.UploadImage.toString());
		registerReceiver(imageUrlReceiver, intentFilter);
    }

    protected void saveUnsyncedWithLocalImagePathAndFinish() {
    	IOfferItem offerWithLocalImage = new OfferItem(m_CurrentItem.getUserID(),
    			m_CurrentItem.getCatalogItemID(),
    			m_CurrentItem.isExchangable(),
    			m_CurrentItem.getState(),
    			m_CurrentItem.getPrice(),
    			m_CurrentItem.getCondition(),
    			m_CurrentItem.getAdditionalInfo());
		offerWithLocalImage.setImageUrl(m_imagePath);
		CacheManager.getManager(getApplicationContext()).addUnsyncedOffer(offerWithLocalImage);
		finish();
    }

	protected void updateOfferAndFinish() {
		m_OfferEditedReciever   = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				setProgressBarIndeterminateVisibility(false);
				boolean bError = intent.getBooleanExtra(RemoteDBService.PARAM_ERROR, false);
				if (bError) {
					handleError(getResources().getString(R.string.error_couldnt_update_offer_now, true));
					unregisterReceiver(this);
				}
				else {
					unregisterReceiver(this);
					Intent resultIntent = new Intent();
					resultIntent.putExtra(OfferActivity.EXTRA_EDITEDITEM, m_CurrentItem);
					setIntent(resultIntent);
					EditOfferActivity.this.setResult(OfferActivity.EDIT_RESULT_CODE,resultIntent);
					finish();
				}
			}
		};

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(RemoteDBServiceOperations.UpdateOffer.toString());
		registerReceiver(m_OfferEditedReciever, intentFilter);

		Intent i = new Intent(this, RemoteDBService.class);
		i.putExtra(RemoteDBService.PARAM_OPERATION, RemoteDBService.RemoteDBServiceOperations.UpdateOffer);
		i.putExtra(RemoteDBService.PARAM_OBJECT, m_CurrentItem);
		startService(i);
	}

    protected void handleError(String sError) {
    	handleError(sError,false);
	}

    protected void handleError(String sError, boolean islong) {
    	Toast.makeText(this, sError, islong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
	}

	protected String getUid() {
		return AccountManager.getCurrentAccount(getApplicationContext()).getUID();
	}

	protected long getCatalogeID() throws NotebookValidationException
	{
		NotebookType selectedType = getSelectedType();
		String sEdition = (String) m_spEdition.getSelectedItem();
		for (ICatalogItem $ : m_RelevantCatalogItems)
		{
			if ($.getEdition().equalsIgnoreCase(sEdition) && $.getType() == selectedType)
			{
				return $.getCatalogItemID();
			}
		}
		throw new NotebookValidationException(getResources().getString(R.string.error_unknown));
	}

	private NotebookType getSelectedType(){
		return m_CurrentCatItem.getType();
	}

	protected String getInfo() {
		return ((EditText)findViewById(R.id.editExtraInfo)).getText().toString();
	}

	protected NotebookCondition getCond()
	{
		int iProgress = getLocation(m_seekBar);
		return NotebookCondition.fromInt(iProgress);
	}

	protected int getPrice() throws NotebookValidationException {
		if (!((CheckBox)findViewById(R.id.checkForSale)).isChecked()) {
			return 0;
		}
		String sPrice = ((EditText)findViewById(R.id.editPrice)).getText().toString();
		if (sPrice.isEmpty()) {
			throw new NotebookValidationException(getResources().getString(R.string.error_enter_price));
		}
		Integer iPrice;
		try
		{
			iPrice  = Integer.parseInt(sPrice);
			if (null == iPrice) {
				throw new Exception();
			}
		}
		catch(Throwable t)
		{
			throw new NotebookValidationException(getResources().getString(R.string.error_invalid_price));
		}
		return iPrice;
	}

	protected boolean getExchange() {
		return ((CheckBox)findViewById(R.id.checkExchange)).isChecked();
	}
}
