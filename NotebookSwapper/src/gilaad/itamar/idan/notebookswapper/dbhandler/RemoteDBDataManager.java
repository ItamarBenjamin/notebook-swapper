package gilaad.itamar.idan.notebookswapper.dbhandler;

import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IUserNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;
import gilaad.itamar.idan.notebookswapper.server.notebook.api.NotebookDatastoreApi;
import gilaad.itamar.idan.notebookswapper.server.notebook.api.NotebookQuery;
import gilaad.itamar.idan.notebookswapper.server.notebook.api.ReturnCodeNotebook;
import gilaad.itamar.idan.notebookswapper.server.notebook.api.iface.INotebookDatastoreApi;
import gilaad.itamar.idan.notebookswapper.server.notebook.api.iface.INotebookQuery;
import gilaad.itamar.idan.notebookswapper.validation.DBException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RemoteDBDataManager extends AbstractDataManager {

	private static INotebookDatastoreApi datastoreAPI = new  NotebookDatastoreApi();
	private static INotebookQuery queriesAPI = new  NotebookQuery();

	@Override
	public HashMap<Integer, ICourse> getAllCourses(long timestamp) {
		List<ICourse> list = queriesAPI.getAllCourses(timestamp);
		HashMap<Integer, ICourse> map = new HashMap<Integer, ICourse>();
		if (null != list) {
			for (ICourse $ : list)
			{
				map.put($.getCourseNumber(), $);
			}
		}
		return map;
	}

	@Override
	public List<IWishlistItem> getMyWishlist(long timestamp, String userId) {
		List<IWishlistItem> list = queriesAPI.getMyWishList(timestamp, userId);
//		HashMap<Long, IWishlistItem> map = new HashMap<Long, IWishlistItem>();
//		if (null != list)
//			for (WishlistItem $ : list)
//			{
//				map.put($.getWishID(), $);
//			}
		return list;
	}

	@Override
	public List<IOfferItem> getMyOffers(long timestamp, String userId) {
		//new debug().run();
		List<IOfferItem> list = queriesAPI.getMyOffers(timestamp, userId);
//		HashMap<Long, IOfferItem> map = new HashMap<Long, IOfferItem>();
//		if (null != list)
//			for (OfferItem $ : list)
//			{
//				map.put($.getOfferID(), $);
//			}
		return list;
	}

	@Override
	public HashMap<Long, ICatalogItem> getCatalog(long timestamp) {
		List<ICatalogItem> list = queriesAPI.getCatalog(timestamp);
		HashMap<Long, ICatalogItem> map = new HashMap<Long, ICatalogItem>();
		if (null != list) {
			for (ICatalogItem $ : list)
			{
				map.put($.getCatalogItemID(), $);
			}
		}
		return map;
	}

	@Override
	public List<IOfferItem> getOffersByCatalogID(List<Long> ids, long timestamp) {
		List<IOfferItem> list = queriesAPI.getOffersByCatalogID(ids,timestamp);
		return list == null ? new ArrayList<IOfferItem>() : list;
	}

	@Override
	public List<IWishlistItem> getWishesByCourseNumber(List<Integer> ids, long timestamp) {
		List<IWishlistItem> list = queriesAPI.getWishesByCourseNumber(ids,timestamp);
		return list == null ? new ArrayList<IWishlistItem>() : list;
	}

	@Override
	public IWishlistItem addWish(IWishlistItem data) throws DBException {
		 IWishlistItem addWishItem = datastoreAPI.addWishItem(data);
		 if (addWishItem == null)
		 {
			throw new DBException(ReturnCodeNotebook.BAD_PARAM);
		 }
		 return addWishItem;
	}

	@Override
	public void updateWish(IWishlistItem item) throws DBException {
		ReturnCodeNotebook $ = datastoreAPI.updateWishItem(item);
		validateReturnCode($);
	}

	@Override
	public void removeWish(IWishlistItem item) throws DBException {
		ReturnCodeNotebook $ = datastoreAPI.removeWishItem(item);
		validateReturnCode($);
	}

	@Override
	public IOfferItem addOffer(IOfferItem data) {
		return datastoreAPI.addOfferItem(data);
	}

	@Override
	public void updateOffer(IOfferItem data) throws DBException {
		ReturnCodeNotebook $ = datastoreAPI.updateOfferItem(data);
		validateReturnCode($);
	}

	private void validateReturnCode(ReturnCodeNotebook $) throws DBException {
		switch ($)
		{
		case SUCCESS:
			return;
		default:
			throw new DBException($);
		}
	}

	@Override
	public void removeOffer(IOfferItem item) throws DBException {
		ReturnCodeNotebook $ = datastoreAPI.removeOfferItem(item);
		validateReturnCode($);
	}

	@Override
	public void addUser(IUserNotebook user) {
		datastoreAPI.addUser(user);
	}

	@Override
	public List<Message> getMyMessages(long timestamp,List<IOfferItem> myOffers, List<IWishlistItem> myWishes) throws DBException {
		Set<Message> setMessages = new HashSet<Message>();
		ArrayList<Long> offerids = new ArrayList<Long>();
		ArrayList<Long> wishids = new ArrayList<Long>();
		for (IOfferItem $ : myOffers)
		{
			offerids.add($.getOfferID());
		}
		for (IWishlistItem $ : myWishes)
		{
			wishids.add($.getWishID());
		}
		List<Message> messagesByWish = queriesAPI.getMessagesByWish(wishids, timestamp);
		if (null == messagesByWish) {
			throw new DBException(ReturnCodeNotebook.BAD_PARAM);
		}
		setMessages.addAll(messagesByWish);

		List<Message> messagesByOffer = queriesAPI.getMessagesByOffer(offerids, timestamp);
		if (null == messagesByOffer)
		{
			throw new DBException(ReturnCodeNotebook.BAD_PARAM);
		}
		setMessages.addAll(messagesByOffer);

		return new ArrayList<Message>(setMessages);
	}

	@Override
	public Message addMessage(Message msg) throws DBException {
		Message sendMessage = queriesAPI.sendMessage(msg);
		if (sendMessage == null)
		{
			throw new DBException(ReturnCodeNotebook.BAD_PARAM);
		}
		return sendMessage;
	}

	public void markRead(List<Long> readMsgs) {
		queriesAPI.setIsReadMessages(readMsgs);
	}

	public List<IOfferItem> getAllOffers(List<Long> lstOffersIDs) {
		return queriesAPI.getOffersByIds(lstOffersIDs, 0L);
	}

	public List<IUserNotebook> getUsers(List<String> usersIds,long timestamp) {
		return queriesAPI.getUsersById(usersIds, timestamp);
	}


}
