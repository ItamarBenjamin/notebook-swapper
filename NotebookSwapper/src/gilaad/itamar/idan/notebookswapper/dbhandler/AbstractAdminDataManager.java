package gilaad.itamar.idan.notebookswapper.dbhandler;

import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.server.notebook.api.NotebookDatastoreApi;
import gilaad.itamar.idan.notebookswapper.server.notebook.api.iface.INotebookDatastoreApi;

public class AbstractAdminDataManager {
	private static INotebookDatastoreApi datastoreAPI = new  NotebookDatastoreApi();
	public void addCourse(ICourse item)
	{
		ICourse $ = datastoreAPI.addCourse(item);
	}
	
//	public abstract void updateCourse(ICourse item);
//	public abstract void removeCourse(Integer courseNumber);
	
	public void addCatalogItem(ICatalogItem item)
	{
		ICatalogItem $ = datastoreAPI.addCatalogItem(item);	
	}
//	public abstract void updateCatalogItem(ICatalogItem item);
//	public abstract void removeCatalogItem(Long id);
}
