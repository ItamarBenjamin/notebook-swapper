package gilaad.itamar.idan.notebookswapper.dbhandler;

import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICatalogItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.ICourse;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IUserNotebook;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import gilaad.itamar.idan.notebookswapper.dbitems.impl.Message;
import gilaad.itamar.idan.notebookswapper.validation.DBException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public abstract class AbstractDataManager {

	private static AbstractDataManager m_manager = null;

	/* get all courses in the Courses table
	 * key - course id
	 * value - course class
	 * onError: throws an exception
	 * */
	public abstract HashMap<Integer, ICourse> getAllCourses(long timestamp);

	/*
	 * get all wishes of the current connected user from the Wishes table
	 * key - wish ID
	 * value - wish class
	 * onError: throws an exception
	 */
	public abstract List<IWishlistItem> getMyWishlist(long timestamp, String userId);

	/*
	 * get all offers of the current connected user from the Offers table
	 * key - offer ID
	 * value - offer class
	 * onError: throws an exception
	 */
	public abstract List<IOfferItem> getMyOffers(long timestamp, String userId);


	/*
	 * get all messages of the current connected user from the Messages table
	 * key - message ID
	 * value - message class
	 * onError: throws an exception
	 */
	public abstract List<Message> getMyMessages(long timestamp, List<IOfferItem> myOffers, List<IWishlistItem> myWishes) throws DBException;

	/*
	 * get the whole catalog from the server, from the Catalog table
	 * key - catalogitemID
	 * value - the class
	 * onError: throws an exception
	 */
	public abstract HashMap<Long,ICatalogItem> getCatalog(long timestamp);

	/*
	 * searches DB for offers corresponding with pattern
	 * onError: throws an exception
	 */
	public abstract List<IOfferItem> getOffersByCatalogID(List<Long> ids, long timestamp);

	public List<IOfferItem> getOffersByCatalogItems(List<ICatalogItem> items, long timestamp) throws DBException
	{
		List<Long> ret = new ArrayList<Long>();
		for (ICatalogItem item : items) {
			ret.add(item.getCatalogItemID());
		}
		return getOffersByCatalogID(ret, timestamp);
	}

	public abstract List<IWishlistItem> getWishesByCourseNumber(List<Integer> ids, long timestamp) throws DBException;
	/*
	 * adds a new wish to the DB
	 * data - the wishlist item to add to the DB with id set to zero
	 * return - nothing, sets the id of the object it received to the new id
	 * onError: throws an exception
	 *
	 * note: the whole class doesn't have to be sent back on the network
	 * 		can only return the new id and it will be added on the client side
	 */
	public abstract IWishlistItem addWish(IWishlistItem data) throws DBException;

	/*
	 * updates a wish on the database
	 * gets the id of the record to update and the new data
	 * onError: throws an exception
	 */
	public abstract void updateWish(IWishlistItem item) throws DBException;

	/*
	 * updates a wish on the database
	 * gets the id of the record to remove
	 * onError: throws an exception
	 */
	public abstract void removeWish(IWishlistItem item) throws DBException;

	/* same like the wishes but for offers */
	public abstract IOfferItem addOffer(IOfferItem item) throws DBException;
	public abstract void updateOffer(IOfferItem item) throws DBException;
	public abstract void removeOffer(IOfferItem item) throws DBException;

	public abstract void addUser(IUserNotebook user);

	public static AbstractDataManager getManager()
	{
		return m_manager;
	}

	public static void setManager(AbstractDataManager $)
	{
		m_manager = $;
	}

	/*
	 * adds a new message to the DB
	 * data - the Message item to add to the DB with id set to zero
	 * return - nothing, sets the id of the object it received to the new id
	 * onError: throws an exception
	 *
	 * note: the whole class doesn't have to be sent back on the network
	 * 		can only return the new id and it will be added on the client side
	 */
	public abstract Message addMessage(Message msg) throws DBException;

}
