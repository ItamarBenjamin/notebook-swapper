package gilaad.itamar.idan.notebookswapper.common;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

public class SnapshotTaker {
	public static final int REQUEST_TAKE_SNAP = 1;
	private static final int SNAPSHOT_LARGE_DIMENTIONS = 1000;
	private static final int SNAPSHOT_SMALL_DIMENTIONS = 150;
	private String mPhotoPath;


	private File createImageFile() throws IOException {
		// Create an image file name
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    String imageFileName = "NbS_" + timeStamp + "_";
	    File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
	    File image = File.createTempFile(
			    imageFileName,  /* prefix */
			    ".jpg",         /* suffix */
			    storageDir      /* directory */
			);

	    // Save a file: path for use with ACTION_VIEW intents
	    mPhotoPath = image.getAbsolutePath();
	    return image;
	}

	public String takeSnapAndReturnPath(Activity activity) {

	    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	    // Ensure that there's a camera activity to handle the intent
	    if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
	        // Create the File where the photo should go
	        File photoFile = null;
	        try {
	            photoFile = createImageFile();
	        } catch (IOException ex) {
	            Log.e("Snapshot taker", "IOException caught while trying to create file for image");
	            return null;
	        }
	        // Continue only if the File was successfully created
	        if (photoFile != null) {
	            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
	            activity.startActivityForResult(takePictureIntent, REQUEST_TAKE_SNAP);
	            return mPhotoPath;
	        }
	    }
	    return null;
	}

	private void galleryAddPic(Activity activity) {
	    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
	    File f = new File(mPhotoPath);
	    Uri contentUri = Uri.fromFile(f);
	    mediaScanIntent.setData(contentUri);
	    activity.sendBroadcast(mediaScanIntent);
	}

	/**
	 * This could be later used to pass images of better quality through intents
	 * just pass the path to the file in the intent and get it as scaled bitmap later.
	 * @param path
	 * 			path to jpeg file on sdcard
	 * @return
	 * 			the bitmap that holded the scaled image.
	 */
	private static Bitmap getScaledBitmapFromPath(String path, int dimentions) {
	    // Get the dimensions of the bitmap
	    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
	    bmOptions.inJustDecodeBounds = true;
	    BitmapFactory.decodeFile(path, bmOptions);
	    int photoW = bmOptions.outWidth;
	    int photoH = bmOptions.outHeight;

	    // Determine how much to scale down the image
	    int scaleFactor = Math.min(photoW/dimentions, photoH/dimentions);

	    // Decode the image file into a Bitmap sized to fill the View
	    bmOptions.inJustDecodeBounds = false;
	    bmOptions.inSampleSize = scaleFactor;
	    bmOptions.inPurgeable = true;

	    return BitmapFactory.decodeFile(path, bmOptions);
	}

	public static Bitmap getFullScaledBitmapFromPath(String path) {
		return getScaledBitmapFromPath(path, SNAPSHOT_LARGE_DIMENTIONS);
	}

	public static Bitmap getSmallScaledBitmapFromPath(String path) {
		return getScaledBitmapFromPath(path, SNAPSHOT_SMALL_DIMENTIONS);
	}
}
