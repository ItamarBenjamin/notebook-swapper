package gilaad.itamar.idan.notebookswapper.common;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.accounts.AccountManager;
import gilaad.itamar.idan.notebookswapper.login.LoginActivity;
import gilaad.itamar.idan.notebookswapper.post.AddOfferActivity;
import gilaad.itamar.idan.notebookswapper.receiver.OurAlarmService;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.MenuItem;

public class MenuItemSelected {
	public static boolean commonOnOptionsItemSelected(Activity that, MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_post) {
			if (AccountManager.getCurrentAccount(that.getApplicationContext()).isGuest())
			{
				return true;
			}
			Intent intent = new Intent(that, AddOfferActivity.class);
			that.startActivity(intent);
			return true;
		}
		if (id == R.id.action_logout) {
			Intent intent = new Intent(that, LoginActivity.class);
			if (!AccountManager.getCurrentAccount(that.getApplicationContext()).isGuest())
			{
				SharedPreferences prefs = that.getSharedPreferences(LoginActivity.class.getPackage().getName(), Context.MODE_PRIVATE);
				prefs.edit().putBoolean(LoginActivity.PREF_CONNECTED_GOOGLE, false).commit();
				intent.setAction(LoginActivity.REVOKE_ACCESS);
				OurAlarmService.stopUpdates(that);
			}
			AccountManager.logOff(that.getApplicationContext());
			that.startActivity(intent);
			that.finish();
			return true;
		}
		return false;
	}
}
