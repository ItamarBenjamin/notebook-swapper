package gilaad.itamar.idan.notebookswapper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class GenericListViewAdapter extends BaseAdapter 
{

	private Context m_context;
	private int m_iResource;

	public GenericListViewAdapter(Context context,int resource)
	{
		m_context = context;
		m_iResource = resource;
	}
	
	@Override
	public int getCount() {
		return 10;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inf = LayoutInflater.from(m_context);
		View view;
		if (null == convertView)
		{
			view = inf.inflate(m_iResource, null);
		}
		else
		{
			view = convertView;
		}
		return view;
	}

}
