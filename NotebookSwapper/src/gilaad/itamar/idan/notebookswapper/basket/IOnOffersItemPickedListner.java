package gilaad.itamar.idan.notebookswapper.basket;

import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;

public interface IOnOffersItemPickedListner {
	void OnOffersItemPicked(IOfferItem item);
}
