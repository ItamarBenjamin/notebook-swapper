package gilaad.itamar.idan.notebookswapper.basket;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.adapters.WishlistAdapter;
import gilaad.itamar.idan.notebookswapper.animation.DefaultAnimation;
import gilaad.itamar.idan.notebookswapper.server.gcm.GcmIntentService;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService.RemoteDBServiceOperations;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.nhaarman.listviewanimations.swinginadapters.AnimationAdapter;

public class NoteBooksWishListFragment extends Fragment {


	private ListView m_ListView;
	private AnimationAdapter m_oAdapter;
	private BroadcastReceiver m_WishesReceiver;
	private BroadcastReceiver m_MatchReceiver;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onResume() {
		m_oAdapter.notifyDataSetChanged();
		super.onResume();
//		new GetMyWishlistAsyncTask(m_oAdapter).execute(new Void[1]);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_notebooks,	container, false);
		m_ListView = (ListView) rootView.findViewById(R.id.listview);
		m_oAdapter = DefaultAnimation.getDefaultListViewAnimationAdapter(new WishlistAdapter(getActivity(),m_ListView),m_ListView);
		m_ListView.setAdapter(m_oAdapter);
		m_oAdapter.notifyDataSetChanged();

		m_WishesReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.v("WishList", "Got wish update");
				m_oAdapter.notifyDataSetChanged();
			}
		};
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(RemoteDBServiceOperations.AddWish.toString());
		intentFilter.addAction(RemoteDBServiceOperations.RemoveWish.toString());
		intentFilter.addAction(RemoteDBServiceOperations.UpdateWish.toString());
		intentFilter.addAction(RemoteDBServiceOperations.UpdateMyWishes.toString());
		intentFilter.addAction(RemoteDBServiceOperations.GetGCMMessage.toString());
		getActivity().registerReceiver(m_WishesReceiver, intentFilter);

		m_MatchReceiver = new BroadcastReceiver(){

			@Override
			public void onReceive(Context context, Intent intent) {
				Log.v("WishList", "Match offer found");
				m_oAdapter.notifyDataSetChanged();
			}

		};
		intentFilter = new IntentFilter();
		intentFilter.addAction(GcmIntentService.OFFER_MATCH_NOTIFICATION);
		intentFilter.addAction(RemoteDBServiceOperations.MessagesRead.toString());
		getActivity().registerReceiver(m_MatchReceiver, intentFilter);
		return rootView;
	}

	@Override
	public void onDestroyView() {
		getActivity().unregisterReceiver(m_WishesReceiver);
		getActivity().unregisterReceiver(m_MatchReceiver);
		m_MatchReceiver = null;
		m_WishesReceiver = null;
		m_oAdapter = null;
		m_ListView = null;
		super.onDestroyView();
	}

}
