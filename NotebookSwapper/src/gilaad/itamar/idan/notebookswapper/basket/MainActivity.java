package gilaad.itamar.idan.notebookswapper.basket;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.common.MenuItemSelected;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IOfferItem;
import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;
import gilaad.itamar.idan.notebookswapper.offer.OfferActivity;
import gilaad.itamar.idan.notebookswapper.receiver.OurAlarmService;
import gilaad.itamar.idan.notebookswapper.wishactivity.WishActivity;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

public class MainActivity extends FragmentActivity implements IOnOffersItemPickedListner,IOnWishsItemPickedListener {

	private static final String MAIN_TAB = "MainActivitySelectedTab";
	private Timer m_lTime;
	private int m_iSelectedTab = 0;

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		Log.v("MainActivity", "On New Intent");
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_lTime=null;
	    final ActionBar actionBar = getActionBar();
	    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);


	    NoteBooksOffersListFragments offersFragment = new NoteBooksOffersListFragments();
	    NoteBooksWishListFragment wishFragment = new NoteBooksWishListFragment();

	    Tab tab1 = actionBar.newTab().setText(R.string.my_notesbooks);//.setIcon(android.R.drawable.btn_dialog);
		tab1.setTabListener(new FragmentTabListner(offersFragment,new Runnable() {
			@Override
			public void run() {
				m_iSelectedTab = 0;
			}
		}));
	    actionBar.addTab(tab1);

	    Tab tab2 = actionBar.newTab().setText(R.string.my_wishlist);//.setIcon(android.R.drawable.alert_dark_frame);
	    tab2.setTabListener(new FragmentTabListner(wishFragment, new Runnable() {
			@Override
			public void run() {
				m_iSelectedTab = 1;
			}
		}));
	    actionBar.addTab(tab2);
	    if (savedInstanceState!=null)
	    {
	    	m_iSelectedTab = savedInstanceState.getInt(MAIN_TAB);
	    }
	    else
	    {
	    	// start updating unsynced stuff
	        OurAlarmService.setAlarmService(this);
	    	m_iSelectedTab = 0;
	    }
	    getActionBar().setSelectedNavigationItem(m_iSelectedTab);
	}


    @Override
    protected void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	outState.putInt(MAIN_TAB, m_iSelectedTab);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
				.getActionView();
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	if (MenuItemSelected.commonOnOptionsItemSelected(this, item)) {
    		return true;
    	}
    	return super.onOptionsItemSelected(item);
    }

	@Override
	public void OnOffersItemPicked(IOfferItem item) {
		Intent intent = new Intent(this,OfferActivity.class);
		intent.putExtra(OfferActivity.EXTRA_OFFERITEM, item);
		startActivity(intent);
	}

	@Override
	public void OnWishItemPicked(IWishlistItem item) {
		Intent intent = new Intent(this,WishActivity.class);
		intent.putExtra(WishActivity.EXTRA_WISHITEM, item);
		startActivity(intent);
	}

	@Override
	public void onBackPressed() {
		if (null == m_lTime)
		{
			m_lTime = new Timer();
			m_lTime.schedule(new TimerTask() {
				@Override
				public void run() {
					m_lTime = null;
				}
			}, new Date(Calendar.getInstance().getTimeInMillis() + TimeUnit.SECONDS.toMillis(3)));
			Toast.makeText(this, "Press back again to quit the program", Toast.LENGTH_SHORT).show();
		}
		else
		{
			m_lTime.cancel();
			finish();
		}
	}
}
