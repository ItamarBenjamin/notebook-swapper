package gilaad.itamar.idan.notebookswapper.basket;

import gilaad.itamar.idan.notebookswapper.R;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Fragment;
import android.app.FragmentTransaction;

public class FragmentTabListner implements TabListener {

	private final Fragment m_fragment;
	private final Runnable m_onSelect;

	public FragmentTabListner(Fragment fragment,Runnable onSelect)
	{
		m_fragment = fragment;
		m_onSelect = onSelect;
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		ft.add(R.id.container,m_fragment, null);
		m_onSelect.run();
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		ft.remove(m_fragment);
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

}
