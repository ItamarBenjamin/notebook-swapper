package gilaad.itamar.idan.notebookswapper.basket;

import gilaad.itamar.idan.notebookswapper.dbitems.iface.IWishlistItem;

public interface IOnWishsItemPickedListener {
	void OnWishItemPicked(IWishlistItem item);
}
