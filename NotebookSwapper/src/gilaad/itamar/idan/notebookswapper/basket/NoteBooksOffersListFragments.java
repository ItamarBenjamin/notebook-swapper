package gilaad.itamar.idan.notebookswapper.basket;

import gilaad.itamar.idan.notebookswapper.R;
import gilaad.itamar.idan.notebookswapper.adapters.OffersListAdapter;
import gilaad.itamar.idan.notebookswapper.animation.DefaultAnimation;
import gilaad.itamar.idan.notebookswapper.service.RemoteDBService.RemoteDBServiceOperations;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.nhaarman.listviewanimations.itemmanipulation.OnDismissCallback;
import com.nhaarman.listviewanimations.swinginadapters.AnimationAdapter;

public class NoteBooksOffersListFragments extends Fragment implements OnDismissCallback {

	private AnimationAdapter m_oAdapter;
	private ListView mListView;
	private BroadcastReceiver m_OffersReceiver;

	enum Direction {
		LEFT, RIGHT;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onResume() {
		m_oAdapter.notifyDataSetChanged();
		super.onResume();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_notebooks,	container, false);
		mListView = (ListView) rootView.findViewById(R.id.listview);
		m_oAdapter = DefaultAnimation.getDefaultListViewAnimationAdapter(new OffersListAdapter(getActivity(),mListView), mListView);
		mListView.setAdapter(m_oAdapter);
		m_oAdapter.notifyDataSetChanged();

		m_OffersReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.v("OfferList", "Got offer update");
				m_oAdapter.notifyDataSetChanged();
			}
		};
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(RemoteDBServiceOperations.AddOffer.toString());
		intentFilter.addAction(RemoteDBServiceOperations.RemoveOffer.toString());
		intentFilter.addAction(RemoteDBServiceOperations.UpdateOffer.toString());
		intentFilter.addAction(RemoteDBServiceOperations.UpdateMyOffers.toString());
		intentFilter.addAction(RemoteDBServiceOperations.MessagesRead.toString());
		intentFilter.addAction(RemoteDBServiceOperations.GetGCMMessage.toString());
		getActivity().registerReceiver(m_OffersReceiver, intentFilter);
		return rootView;
	}


	@Override
	public void onDestroyView() {
		getActivity().unregisterReceiver(m_OffersReceiver);
		m_OffersReceiver = null;
		m_oAdapter = null;
		mListView = null;
		super.onDestroyView();
	}

	@Override
	public void onDismiss(AbsListView listView, int[] reverseSortedPositions) {
		// TODO Auto-generated method stub

	}

}
